<?php include("../includes/conn.php");
include("auto.php");
include("../includes/extraer_variables.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $ptitulo?></title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
<?php include("scripts.php");?>
</head>

<body class="twoColLiqLtHdr">

    <div id="container"> 
      <div id="header">
        <?php include("cabeza.php");?>
      <!-- end #header --></div>
      <div id="sidebar1">
        <?php include("menu.php");?>
      <!-- end #sidebar1 --></div>
      <div id="mainContent">
      <div id="submenu"><!-- DESDE AQUI SUBMENU -->
      <!-- HASTA AQUI SUBMENU --></div>
      <!-- DESDE AQUI CONTENIDO -->
      <?php           $sql_al = "SELECT * FROM com_alumnos WHERE id = ". $id ."";
          $result_al = mysql_query($sql_al);
		  $row_al = mysql_fetch_array($result_al);
    ?>
        <h1>Examenes</h1>
        <h2>Examenes tomados por usuario: <?php echo $row_al['ape1']." ".$row_al['ape2'].", ".$row_al['nombre']?></h2>
        <?php           $sql = "SELECT * FROM com_alumnos_exam WHERE alumno = ". $id ." ORDER BY fecini";
          $result = mysql_query($sql);
    ?>
    <table cellpadding="0" cellspacing="0" border="1" width="80%" align="center">
        <tr>
        <td align="center" width="30%">Capitulo / Curso</td>
        <td align="center" width="20%">Estado</td>
        <td align="center" width="30%">Inicio / Fin</td>
        <td align="center" width="20%">Ver Respuestas</td>
        </tr>
        <?php while ($row = mysql_fetch_array($result)) {
			
			$sql_pre = "SELECT * FROM com_exam_preg WHERE modulo=".$row['modulo']." ORDER BY orden1";
		   
           $result_pre = mysql_query($sql_pre1,$link);
           $NroRegistrosc=mysql_num_rows(mysql_query($sql_pre));
		   
		   $porcentaje = ($row['nota'] * 100) / $NroRegistrosc;
		   
			$sql_cap = "SELECT * FROM com_cursos_mod WHERE id = ". $row['modulo'] ."";
            $result_cap = mysql_query($sql_cap);
			$row_cap = mysql_fetch_array($result_cap);
			
			$sql_cur = "SELECT * FROM com_cursos WHERE id = ". $row_cap['curso'] ."";
            $result_cur = mysql_query($sql_cur);
			$row_cur = mysql_fetch_array($result_cur);
                        
                        
                        $sql_ven = "SELECT * FROM com_alumnos_modulo WHERE modulo = ". $row['modulo'] ." AND alumno = ".$id."";
                        $result_ven = mysql_query($sql_ven);
			$row_ven = mysql_fetch_array($result_ven);
                        
                        $nuevo_ini = strtotime($row_ven['fecin']."+ 30 days");
                        $fec_nuevoi = date("Y-m-d H:i:s",$nuevo_ini);
		  
                ?>
                <tr>
                            <td align="left"><?php echo $row_cap['titulo'];?> (<?php echo $row_cur['titulo'];?>)<br>
                                    <span<?php if ($fechoy > $fec_nuevoi) { ?> class="roja"<?php } ?>><?php echo "Ini mod: ".$row_ven['fecin']?></span><br>
                            <a href="usuarios_mod_ampliar.php?modulo=<?php echo $row['modulo'];?>&alumno=<?php echo $id;?>" onClick="return confirm('Seguro quiere ampliar el plazo de este modulo para <?php echo $row_al['ape1']." ".$row_al['ape2'].", ".$row_al['nombre']?> ?');">Ampliar plazo</a>
                        </br></td>
        
        <td align="left"><?php if ($row['estado'] == 0) {?>No Finalizado<?php } else { ?>
		Finalizado (<?php echo $porcentaje?>% - <?php if ($row['aprobado'] == 0) {?>NO<?php } ?> Aprobado)
		<?php } ?>
                
        </td>
        <td align="left"><?php echo $row['fecini'];?> <br /><?php if ($row['estado'] == 1) {
			echo $row['fecfin'];
		} ?></td>
        <td align="center"><?php if ($row['estado'] == 1) { ?><a href="usuarios_respuestas.php?id=<?php echo $row['id'];?>&alumno=<?php echo $id;?>&modulo=<?php echo $row['modulo'];?>">Ver Respuestas</a><?php } ?><br />
        <a href="usuarios_examenes_reset.php?id=<?php echo $row['id'];?>&alumno=<?php echo $id;?>&modulo=<?php echo $row['modulo'];?>"  onClick="return confirm('Se borrarán todas las preguntas del examen, y el resultado final, desea continuar?');">Reiniciar examen</a>
        </td>
         
      </tr>
                  <?php } ?>
        </table>
    <br /><br />
    	<!-- HASTA AQUI CONTENIDO --></div>
    	<br class="clearfloat" />
      <div id="footer">
        <?php include("pie.php"); ?>
      <!-- end #footer --></div>
    <!-- end #container --></div>
    </body>
</html>
