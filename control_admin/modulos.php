<?php include("../includes/conn.php");
include("auto.php");
include("../includes/extraer_variables.php");

$sql = "SELECT * FROM com_cursos WHERE id=" . $id . "";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $ptitulo ?></title>
    <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
    <?php include("scripts.php"); ?>
    <script>
        $(function () {
            $(".datepicker1").datepicker({
                dateFormat: "yy/mm/dd",
                showOn: "button",
                buttonImage: "images/calendar.gif",
                buttonImageOnly: true,
                buttonText: "Select date"
            });
        });
    </script>
</head>
<body class="twoColLiqLtHdr">
<div id="container">
    <div id="header">
        <?php include("cabeza.php"); ?>
        <!-- end #header --></div>
    <div id="sidebar1">
        <?php include("menu.php"); ?>
        <!-- end #sidebar1 --></div>
    <div id="mainContent">
        <div id="submenu"><!-- DESDE AQUI SUBMENU -->
            <!-- HASTA AQUI SUBMENU --></div>
        <!-- DESDE AQUI CONTENIDO -->
        <h1>Modulos: <?php echo $row['titulo']; ?></h1>
        <div class="box">
            <h2>Agregar Modulo al curso </h2>
            <form method="POST" action="modulos_add.php?curso=<?php echo $id; ?>">
                <div class="row">
                    <div class="col-4">
                        <label><span>Nombre: </span>
                            <input class="form-control" type="text" name="titulo" size="20"></label>
                    </div>
                    <div class="col-4"><label><span>Título en diploma: </span>
                            <input class="form-control" type="text" name="titulo_diploma" size="20"></label></div>
                    <div class="col-4"><label><span>Créditos: </span>
                            <input class="form-control" type="text" name="creditos" size="10"></label></div>
                </div>
                <div class="row">
                    <div class="col-4"><label><span>No. Acreditación: </span>
                            <input class="form-control" type="text" name="no_acred" size="20"></label></div>
                    <div class="col-4"><label><span>Periodo: </span>
                            <input class="form-control" type="text" name="periodo" size="20"></label></div>
                    <div class="col-4"><label><span>Acreditado desde: </span>
                            <input class="form-control" type="text" name="acred_desde" class="datepicker1" size="20"
                                   readonly="readonly"></label></div>
                </div>
                <div class="row">
                    <div class="col-4"><label><span>Acreditado hasta: </span>
                            <input class="form-control" type="text" name="acred_hasta" class="datepicker1" size="20"
                                   readonly="readonly"></label></div>
                    <div class="col-4"><label></label><span>Solicitada acreditación?: </span>
                        <input type="checkbox" name="solicitada" value="1"></div>
                    <div class="col-4"><label></label> <span>Acreditado?: </span>
                        <input type="checkbox" name="acreditado" value="1">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6"><label><span>Horas: </span>
                            <input class="form-control" type="text" name="horas" size="10" class="numerico"></label>
                    </div>
                    <div class="col-6"><label><span>Color: </span>
                            <input class="form-control" type="text" name="color" size="10"></label>
                    </div>
                </div>
                <label><span>Descripción: </span><textarea class="form-control" name="descripcion" rows="4"
                                                           cols="40"></textarea></label>
                <div class="row">
                    <div class="col-6"><label><span>Subtítulo: </span>
                            <input class="form-control" type="text" name="subtitulo" size="20"></label></div>
                    <div class="col-6"><label><span>Video (código Vimeo): </span>
                            <input class="form-control" type="text" name="video" size="20"></label></div>

                </div>


                <label><span>Introduccion: </span>&nbsp;</label>
                <textarea class="form-control" id="editor1" name="intro" rows="6" cols="80"></textarea>
                <script type="text/javascript">


                    var editor = CKEDITOR.replace('editor1',

                        {

                            toolbar:

                                [

                                    ['Source', '-', 'Preview', '-', 'Templates'],
                                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print', 'SpellChecker', 'Scayt'],
                                    ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],

                                    '/',
                                    ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
                                    ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
                                    ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                                    ['BidiLtr', 'BidiRtl'],
                                    ['Link', 'Unlink', 'Anchor'],
                                    ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
                                    '/',
                                    ['Styles', 'Format', 'Font', 'FontSize'],
                                    ['TextColor', 'BGColor'],
                                    ['Maximize', 'ShowBlocks', '-', 'About']

                                ],
                            stylesCombo_stylesSet: 'my_styles:<?php echo $baseURLcontrol;?>js/styles.js',
                            contentsCss: '<?php echo $baseURLcontrol;?>css/losstilos.css',


                        });
                    //editor.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );

                    // Just call CKFinder.SetupCKEditor and pass the CKEditor instance as the first argument.
                    // The second parameter (optional), is the path for the CKFinder installation (default = "/ckfinder/").
                    editor.config.templates_files = ['<?php echo $baseURLcontrol;?>js/mytemplates.js'];
                    CKFinder.setupCKEditor(editor, '<?php echo $baseURL;?>plugins/ckfinder/');

                    // It is also possible to pass an object with selected CKFinder properties as a second argument.
                    // CKFinder.SetupCKEditor( editor, { BasePath : '../../', RememberLastFolder : false } ) ;
                </script>


                <div class="fmr_sub">Examen del modulo</div>
                <label><span>Mostrar Preg / pag: </span>
                    <input class="form-control" type="text" name="preg_pag" size="10" class="numerico"></label>

                <div class="row">
                    <div class="col-6"><label><span>Cant. preguntas minima para aprobar: </span>
                            <input class="form-control" type="text" name="preg_aprob" size="10"
                                   class="numerico"></label></div>
                    <div class="col-6"><label><span>Porcentaje aprobacion: </span>
                            <input class="form-control" type="text" name="porc_aprob" size="10"
                                   class="numerico" placeholder="%"></label></div>

                </div>

                <?php if ($row['examen'] == 1) { ?>
                    <label><span>Examen unico del curso: </span><input type="checkbox" name="ex_unico"
                                                                       value="1"/></label>
                <?php } ?>

                <div class="text-center"><input class="btn btn-primary" type="submit" value="Agregar" name="B1"/></div>
            </form>
        </div>
        <h2>Contenidos del Curso</h2>
        <?php
        $sql_1 = "SELECT * FROM com_cursos_mod WHERE curso = " . $id . " ORDER BY orden";
        $result_1 = mysql_query($sql_1);
        ?>
        <table class="table table-responsive table-striped">

            <?php while ($row_1 = mysql_fetch_array($result_1)) {
                //$descr = strip_tags($row['fra']);
                ?>
                <tr id="table6-row-<?php echo $row_1['id'] ?>">
                    <td class="dragHandle">&nbsp;</td>
                    <td align="center"><?php echo $row_1['titulo'] ?></td>
                    <td>
                        <div class="row">

                            <div class="col-3">
                                <a href="capitulos.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>">Capitulos</a>
                            </div>
                            <div class="col-3">
                                <a href="modulos_down.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>">Descargas</a>
                            </div>
                            <div class="col-3">
                                <?php if ($row['examen'] == 0 or ($row['examen'] == 1 && $row_1['examen_unico'] == 1)) { ?>
                                    <a href="modulos_acred.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>">Acreditaciones</a>
                                <?php } ?>
                            </div>
                            <div class="col-3">
                                <a href="modulos_encuesta.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>">Encuesta</a>
                            </div>
                        </div>
                    </td>

                    <td align="center">
                        <a href="modulos_up.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>"><img border="0"
                                                                                                           alt="Imagen"
                                                                                                           title="Imagen"
                                                                                                           src="body/jpg.png"></a>
                        <a href="modulos_mod.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>"><img border="0"
                                                                                                            alt="Modificar"
                                                                                                            title="Modificar"
                                                                                                            src="body/modif.gif"></a>
                        <a href="modulos_elim.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>"
                           onClick="return confirm('Seguro de eliminar este modulo?');"><img border="0" alt="Eliminar"
                                                                                             title="Eliminar"
                                                                                             src="body/elim.gif"></a>
                        <?php if ($row_1['estado'] == 0) { ?>
                            <a href="modulos_estado.php?st=1&id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>"><img
                                        border="0" src="body/suspender.gif" title="Click para Activar"></a>&nbsp;
                        <?php } else { ?>
                            <a href="modulos_estado.php?st=0&id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>"><img
                                        border="0" src="body/activar.gif" title="Click para Suspender"></a>&nbsp;
                        <?php } ?>

                    </td>


                </tr>
            <?php } ?>
        </table>
        <div id="AjaxResult"></div>
        <br/><br/>
        <script type="text/javascript">
            <?php include_once('script_ordenar_mod.php');?>
        </script>
        <br/><br/>
        <!-- HASTA AQUI CONTENIDO --></div>
    <br class="clearfloat"/>
    <div id="footer">
        <?php include("pie.php"); ?>
        <!-- end #footer --></div>
    <!-- end #container --></div>
</body>
</html>
