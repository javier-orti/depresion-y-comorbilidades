<?php include("../includes/conn.php");
include("auto.php");

include("../includes/extraer_variables.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $ptitulo ?></title>
    <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
    <?php include("scripts.php"); ?>
</head>

<body class="twoColLiqLtHdr">

<div id="container">
    <div id="header">
        <?php include("cabeza.php"); ?>
        <!-- end #header --></div>
    <div id="sidebar1">
        <?php include("menu.php"); ?>
        <!-- end #sidebar1 --></div>
    <div id="mainContent">
        <div id="submenu"><!-- DESDE AQUI SUBMENU -->
            <!-- HASTA AQUI SUBMENU --></div>
        <!-- DESDE AQUI CONTENIDO -->
        <?php $RegistrosAMostrar = 30;
        foreach ($_GET as $key => $value) {
            if ($key != 'pag') {
                $listvar .= $key . "=" . $value . "&";
            }
            if ($key != 'orden' && $key != 'tiporden' && $key != 'pag') {
                $listvaro .= $key . "=" . $value . "&";
            }
        }
        if (empty($_GET['orden'])) {
            $orden = 'ape1';
        } else {
            if (empty($_GET['tiporden'])) {
                $orden = $_GET['orden'];
            } else {
                $orden = $_GET['orden'] . " DESC";
            }
        }

        //estos valores los recibo por GET
        if (isset($pag)) {
            $RegistrosAEmpezar = ($pag - 1) * $RegistrosAMostrar;
            $PagAct = $pag;
            //echo "aqui vemos";
            //caso contrario los iniciamos
        } else {
            $RegistrosAEmpezar = 0;
            $PagAct = 1;
        }


        $sql_men3 = "SELECT * FROM com_alumnos WHERE id >= 0";
        if (!empty($email)) {
            $sql_men3 .= " AND email = '" . $email . "'";
        }
        if (!empty($nombre)) {
            $sql_men3 .= " AND nombre LIKE '%" . $nombre . "%'";
        }
        if (!empty($apellido)) {
            $sql_men3 .= " AND ape1 LIKE '%" . $apellido . "%'";
        }


        $sql_men31 = $sql_men3 . " ORDER BY " . $orden . " LIMIT $RegistrosAEmpezar, $RegistrosAMostrar";

        $result_men3 = mysql_query($sql_men31, $link);
        $NroRegistros = mysql_num_rows(mysql_query($sql_men3));
        $NroRegistrosc = mysql_num_rows(mysql_query($sql_men31));


        $cantproductos = $RegistrosAEmpezar + $RegistrosAMostrar;
        if ($cantproductos > $NroRegistros) {
            $cantproductos = $NroRegistros;
        }

        $PagAnt = $PagAct - 1;
        $PagSig = $PagAct + 1;
        $PagUlt = $NroRegistros / $RegistrosAMostrar;

        //verificamos residuo para ver si llevará decimales
        $Res = $NroRegistros % $RegistrosAMostrar;

        if ($Res > 0) $PagUlt = floor($PagUlt) + 1;

        echo "<div class=\"paginas\">Alumnos " . ($RegistrosAEmpezar + 1) . "-" . $cantproductos . " de " . $NroRegistros . "</div>";
        echo "<div class=\"paginas\">Pagina " . $PagAct . " de " . $PagUlt . "</div>";


        echo "<div style='padding:10px;'>";
        echo "<div style='position:relative; width:140px; float:left; padding:0px; text-align:right;'>";
        if ($PagAct > 1) {

            echo "<a href='usuarios.php?pag=1&" . $listvar . "'>Primera</a>&nbsp;&nbsp;&nbsp;<a href='usuarios.php?pag=" . $PagAnt . "&" . $listvar . "'>Anterior</a>";
        }
        echo "</div>";
        //echo "<strong>Pagina ".$PagAct."/".$PagUlt."</strong>";
        echo "<div style='position:relative; width:180px; float:right; padding:0px; text-align:left;'>";

        if ($PagAct < $PagUlt) {

            echo "<a href='usuarios.php?pag=" . $PagSig . "&" . $listvar . "'>Siguiente</a>&nbsp;&nbsp;&nbsp;<a href='usuarios.php?pag=" . $PagUlt . "&" . $listvar . "'>Ultima</a>";
        }
        //echo "<a onclick=Pagina('$PagUlt')>Ultimo</a>";
        echo "</div><br class='clearfloat' /></div>";


        ?>
        <h6>Buscar Usuarios</h6>
        <div class="container">
            <form action="usuarios.php" method="get">
                <div class="row">
                    <div class="col-3"><input class="form-control" type="text" name="email" placeholder="E-mail"/></div>
                    <div class="col-3"><input class="form-control" type="text" name="apellido" placeholder="Apellido"/></div>
                    <div class="col-3"><input class="form-control" type="text" name="nombre"  placeholder="nombre"/></div>
                    <div class="col-3"><button class="btn btn-dark" type="submit">Buscar</button></div>
                </div>
            </form>
        </div>
        <h2>Usuarios ingresados</h2>
        <table class="table table-striped table-responsive">
            <thead>
            <tr>
                <th>Usuario <a href="usuarios.php?<?php echo $listvaro ?>&orden=ape1"><img src="body/orden_up.png"></a>
                    <a href="usuarios.php?<?php echo $listvaro ?>&orden=ape1&tiporden=desc"><img
                                src="body/orden_down.png"></a></th>
                <th>Codigo</th>
                <th>Email <a href="usuarios.php?<?php echo $listvaro ?>&orden=email"><img src="body/orden_up.png"></a>
                    <a href="usuarios.php?<?php echo $listvaro ?>&orden=email&tiporden=desc"><img
                                src="body/orden_down.png"></a></th>
                <th>Fecha reg <a href="usuarios.php?<?php echo $listvaro ?>&orden=fecreg"><img src="body/orden_up.png"></a>
                    <a href="usuarios.php?<?php echo $listvaro ?>&orden=fecreg&tiporden=desc"><img
                                src="body/orden_down.png"></a></th>
                <th>Especialidad</th>
                <th>Provincia</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <?php while ($row_men3 = mysql_fetch_array($result_men3)) {

                $perfil = "";
                $sql_perfil = "SELECT codigo, perfil FROM com_perfiles WHERE codigo = '" . $row_men3['perfil'] . "'";

                $result_perfil = mysql_query($sql_perfil, $link) or die("el error es porque user1: " . mysql_error());

                if ($row_perfil = mysql_fetch_array($result_perfil)) {
                    $perfil = $row_perfil['perfil'];
                }

                if ($row_men3['perfil'] == 'ME' && $row_men3['especialidad'] != 0) {

                    $sql_perfil1 = "SELECT especialidad FROM com_especialidades WHERE id = " . $row_men3['especialidad'] . "";

                    $result_perfil1 = mysql_query($sql_perfil1, $link) or die("el error es porque user2: " . mysql_error());

                    if ($row_perfil1 = mysql_fetch_array($result_perfil1)) {
                        $perfil = $row_perfil1['especialidad'];
                    }

                }

                $provincia = "";

                if (!empty($row_men3['provincia'])) {

                    $sql_pro = "SELECT provincia FROM com_provincias WHERE codigo = '" . $row_men3['provincia'] . "' AND pais = '" . $row_men3['pais'] . "'";

                    $result_pro = mysql_query($sql_pro, $link) or die("el error es porque user3: " . mysql_error());

                    if ($row_pro = mysql_fetch_array($result_pro)) {
                        $provincia = $row_pro['provincia'];
                    }

                } else {

                    $sql_pro = "SELECT pais FROM com_paises WHERE codigo = '" . $row_men3['pais'] . "'";

                    $result_pro = mysql_query($sql_pro, $link) or die("el error es porque user4: " . mysql_error());

                    if ($row_pro = mysql_fetch_array($result_perfil1)) {
                        $provincia = $row_pro['pais'];
                    }

                }


                ?>
                <tr>
                    <td align="left"><?php echo $row_men3['ape1'] . " " . $row_men3['ape2'] . ", " . $row_men3['nombre']; ?></td>
                    <td align="left"><?php echo $row_men3['codusuario']; ?>
                    </td>
                    <td align="left"><?php echo $row_men3['email']; ?>
                    </td>
                    <td align="left"><?php $fecha = strtotime($row_men3['fecreg']);
                        echo date('d-m-Y h:i:s', $fecha); ?>
                    </td>
                    <td align="left"><?php echo $perfil; ?>
                    </td>
                    <td align="left"><?php echo $provincia; ?>
                    </td>
                    <td align="center"><a href="usuarios_examenes.php?id=<?php echo $row_men3['id']; ?>">Ver
                            Examenes</a>
                    </td>

                </tr>
            <?php } ?>
        </table>
        <br/><br/>
        <!-- HASTA AQUI CONTENIDO --></div>
    <br class="clearfloat"/>
    <div id="footer">
        <?php include("pie.php"); ?>
        <!-- end #footer --></div>
    <!-- end #container --></div>
</body>
</html>
