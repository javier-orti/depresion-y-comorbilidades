<?php include("../includes/conn.php");
include("auto.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.7.2.custom.css"/>

    <title><?php echo $ptitulo ?></title>
    <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
    <?php include("scripts.php"); ?>
    <script type="text/javascript">
        jQuery(function ($) {
            $.datepicker.regional['es'] = {
                closeText: 'Cerrar',
                prevText: '&#x3c;Ant',
                nextText: 'Sig&#x3e;',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                    'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
                dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Juv', 'Vie', 'S&aacute;b'],
                dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S&aacute;'],
                weekHeader: 'Sm',
                dateFormat: 'yy/mm/dd',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['es']);
        });
    </script>
</head>

<body class="twoColLiqLtHdr">

<div id="container">
    <div id="header">
        <?php include("cabeza.php"); ?>
        <!-- end #header --></div>
    <div id="sidebar1">
        <?php include("menu.php"); ?>
        <!-- end #sidebar1 --></div>
    <div id="mainContent">
        <div id="submenu"><!-- DESDE AQUI SUBMENU -->
            <!-- HASTA AQUI SUBMENU --></div>
        <!-- DESDE AQUI CONTENIDO -->
        <h1>Buscar Examenes Finalizados</h1>
        <div class="box">
            <h2>Buscar</h2>
            <form method="GET" action="resultados_bus1.php">
                <div class="row">
                    <div class="col-6"><label><span>Curso: </span>
                            <select class="form-select" name="curso" id="frm_curso">
                                <option value="">Todos</option>
                                <?php $sql = "SELECT * FROM com_cursos ORDER BY id";
                                $result = mysql_query($sql);
                                while ($row = mysql_fetch_array($result)) {
                                    ?>
                                    <option value="<?php echo $row['id'] ?>"><?php echo $row['titulo'] ?></option>
                                <?php } ?>
                            </select></label></div>
                    <div class="col-6"><label><span>Modulo: </span>
                            <select class="form-select" name="modulo" id="frm_modulo">
                                <option value="">Todos</option>
                            </select></label></div>
                </div>
                <div class="row">
                    <div class="col-6"><label><span>Finalizado desde: </span>
                            <input type="text" name="datepicker" class="datepicker form-control"
                                   readonly="readonly"/></label>
                    </div>
                    <div class="col-6"><label><span> hasta: </span><input type="text" name="datepicker1"
                                                                          class="datepicker form-control"
                                                                          readonly="readonly"/></label></div>
                </div>
                <div class="row">
                    <div class="col-6"><label><span>Resultado: </span>
                            <select class="form-select" name="aprobado">
                                <option value="">Todos</option>
                                <option value="0">Reprobado</option>
                                <option value="1">Aprobado</option>
                            </select></label></div>
                    <div class="col-6"><div><input type="submit" class="btn btn-dark mt-4" value="Buscar" name="B1"/></div></div>
                </div>



            </form>
        </div>

        <br/><br/>
        <!-- HASTA AQUI CONTENIDO --></div>
    <br class="clearfloat"/>
    <div id="footer">
        <?php include("pie.php"); ?>
        <!-- end #footer --></div>
    <!-- end #container --></div>
</body>
</html>
