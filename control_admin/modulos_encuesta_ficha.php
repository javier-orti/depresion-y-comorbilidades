<?php include("../includes/conn.php");
include("auto.php");
include("../includes/extraer_variables.php");

$sql = "SELECT * FROM com_cursos WHERE id=".$id."";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $ptitulo?></title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
<?php include("scripts.php");?>
<script>
  $(function() {
    $(".datepicker1").datepicker({
	     dateFormat:"yy/mm/dd",
		 showOn: "button",
      buttonImage: "images/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Select date"
		});
  });
  </script>
</head>

<body class="twoColLiqLtHdr">

    <div id="container"> 
      <div id="header">
        <?php include("cabeza.php");?>
      <!-- end #header --></div>
      <div id="sidebar1">
        <?php include("menu.php");?>
      <!-- end #sidebar1 --></div>
      <div id="mainContent">
      <div id="submenu"><!-- DESDE AQUI SUBMENU -->
      <!-- HASTA AQUI SUBMENU --></div>
      <!-- DESDE AQUI CONTENIDO -->
      
        <h2>Encuesta del modulo</h2>
        <div class="der"><a href="modulos_encuesta_resp.php?modulo=<?php echo $modulo;?>&ref=<?php echo $ref;?>">Volver</a></div>
        <?php
         $sql_p1 = "SELECT * FROM com_encuesta WHERE id=".$id."";		   
                 $result_p1 = mysql_query($sql_p1);
               $row_p1 = mysql_fetch_array($result_p1); 
		  
		  $sql_user = "SELECT * FROM com_alumnos WHERE id = ".$row_p1['alumno'];
                $result_user = mysql_query($sql_user);
               $row_user = mysql_fetch_array($result_user); 

    ?>
    <div class="bloque espaciado">
           
           <div>
           	<div>Usuario: <?php echo $row_user['ape1'].", ".$row_user['nombre'];?></div>
           	<div>Email: <?php echo $row_user['email'];?></div>
           	<div>Fecha: <?php echo $row_p1['fecha'];?></div>
           </div>
            
             <table width="100%" cellpadding="2" cellspacing="0" border="1">
              <tr>
                <td width="75%">Pregunta</td>
                <td width="25%"><strong>Respuesta</strong></td>
              </tr>
              <tr>
                <td class="tith"><span class="labelp2">¿Crees que el curso ha sido útil para ampliar tus conocimientos en epilepsia?</span></td>
                <td><?php echo $row_p1['p1'];?></td>
              </tr>
              
              <tr>
                <td class="tith"><span class="labelp2">¿Crees que los casos clínicos presentados son de interés para la práctica clínica?</span></td>
                <td><?php echo $row_p1['p2'];?></td>
              </tr>
              
              <tr>
                <td class="tith"><span class="labelp2">¿Crees que el formato interactivo del curso y la presencia de videos del autor son de utilidad para mejorar la comprensión de los casos clínicos?</span></td>
                <td><?php echo $row_p1['p3'];?></td>
              </tr>
              
              <tr>
                <td class="tith"><span class="labelp2">¿Crees que las preguntas del examen era suficientemente claras?</span></td>
                <td><?php echo $row_p1['p4'];?></td>
              </tr>
              
              
              <tr>
                <td class="tith"><span class="labelp2">¿Recomendarías este curso a otros profesionales sanitarios?</span></td>
                <td><?php echo $row_p1['p5'];?></td>
              </tr>
              
              <tr>
                <td class="tith" colspan="2"><span class="labelp2">¿Qué consideras que podríamos mejorar en este curso?</span></td>
              </tr>
              
              <tr>
                <td class="tith" colspan="2"><?php echo $row_p1['p6'];?></td>
              </tr>
              
             </table>
           </div>
        
 
 
    <br /><br />
    	<!-- HASTA AQUI CONTENIDO --></div>
    	<br class="clearfloat" />
      <div id="footer">
        <?php include("pie.php"); ?>
      <!-- end #footer --></div>
    <!-- end #container --></div>
    </body>
</html>
