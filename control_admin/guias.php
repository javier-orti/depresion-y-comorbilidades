<? include_once("../includes/conn.php");
include("auto.php");
include_once("../includes/extraer_variables.php");

$sql = "SELECT * FROM com_cursos WHERE id=" . $id . "";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= $ptitulo ?></title>
    <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
    <? include("scripts.php"); ?>

    <link href="<?php echo $baseURL; ?>plugins/uploadify/uploadify.css" type="text/css" rel="stylesheet"/>
    <script type="text/javascript" src="<?php echo $baseURL; ?>plugins/uploadify/swfobject.js"></script>
    <script type="text/javascript"
            src="<?php echo $baseURL; ?>plugins/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseURLcontrol; ?>js/funciones.js"></script>

    <script type="text/javascript">


        function startUpload(id, conditional) {
            if (conditional.value.length != 0) {


                $('#' + id).uploadifySettings('scriptData', {
                    'titulo': remplazos($('#titulo').val()),
                    'url': remplazos($('#url').val()),
                    'categoria': remplazos($('#categoria').val()),
                    'subcategoria': remplazos($('#subcategoria').val()),
                    'curso': <?=$id?>
                });
                $('#' + id).uploadifyUpload();
            } else
                alert("Debe ingresar un nombre");
        }


        $(document).ready(function () {


            $("#fileUploadname3").uploadify({
                'uploader': '<?php echo $baseURL;?>plugins/uploadify/uploadify.swf',
                'script': '<?php echo $baseURL;?>guias/uploadify.php',
                'cancelImg': '<?php echo $baseURL;?>plugins/uploadify/cancel.png',
                'folder': '<?php echo $baseURL;?>guias',
                'multi': false,
                'buttonText': 'Seleccionar Imagen',
                'fileExt': '*.jpg;*.gif;*.png;*.JPG;*.GIF;*.PNG',
                'fileDesc': 'Web Image Files (.GIF; .PNG; .JPG)',
                'method': 'post',
                'displayData': 'percentage',

                onAllComplete: function (event, queueID, fileObj, response, data) {
                    //alert(data);
                    $.post("guias_listado.php?id=<?=$id?>", function (data1) {
                        /// Ponemos la respuesta de nuestro script en el párrafo recargado
                        $("#imagenes").html(data1);
                    });
                    $('#titulo').attr('value', '');
                    $('#url').attr('value', '');
                    $('#subcategoria').val('');

                    $(".check").removeAttr("checked");


                }
            });


        });

    </script>

</head>

<body class="twoColLiqLtHdr">

<div id="container">
    <div id="header">
        <? include("cabeza.php"); ?>
        <!-- end #header --></div>
    <div id="sidebar1">
        <? include("menu.php"); ?>
        <!-- end #sidebar1 --></div>
    <div id="mainContent">
        <div id="submenu"><!-- DESDE AQUI SUBMENU -->
            <!-- HASTA AQUI SUBMENU --></div>
        <!-- DESDE AQUI CONTENIDO -->
        <h1>Guías prácticas en DM</h1>
        <div class="box container">
            <h2>Agregar Guia </h2>
            <div class="row">
                <div class="col-3">
                    <label><span>Nombre: </span>
                        <input class="form-control" type="text" name="titulo" id="titulo" size="20"></label>
                </div>
                <div class="col-3">
                    <label><span>URL: </span>
                        <input class="form-control" type="text" name="url" id="url" size="20"></label>
                </div>
                <div class="col-3">
                    <label><span>Categoria: </span>
                        <select class="form-control" name="categoria" id="categoria">
                            <option value="guias1">Algoritmos Terapéuticos</option>
                            <option value="guias2">Guías Terapéuticas</option>
                            <option value="guias3">Recursos Online para profesionales</option>
                            <option value="guias4">Guías para pacientes</option>
                            <option value="guias5">Estrategia en diábetes del Sistema Nacional de Salud</option>
                        </select></label>
                </div>
                <div class="col-3">
                    <label><span>Categoria: </span>
                        <select class="form-control" name="categoria" id="categoria">
                            <option value="guias1">Algoritmos Terapéuticos</option>
                            <option value="guias2">Guías Terapéuticas</option>
                            <option value="guias3">Recursos Online para profesionales</option>
                            <option value="guias4">Guías para pacientes</option>
                            <option value="guias5">Estrategia en diábetes del Sistema Nacional de Salud</option>
                        </select></label>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <label><span>Sub Categoria: </span>
                        <select class="form-control" name="subcategoria" id="subcategoria">
                            <option value="">N/A</option>
                            <option value="Ancianos">Ancianos</option>
                            <option value="Generales">Generales</option>
                            <option value="Insuficiencia Renal">Insuficiencia Renal</option>
                        </select></label>
                </div>
                <div class="col-6">
                    <div class="botonup">
                        <div id="fileUploadname3">You have a problem with your javascript</div>
                    </div>
                    <div class="botonup"><a
                                href="javascript:startUpload('fileUploadname3', document.getElementById('titulo'))">
                            <button class="btn btn-dark">Subir</button>
                        </a> | <a
                                href="javascript:$('#fileUploadname3').fileUploadClearQueue()">
                            <button class="btn btn-success">Limpiar</button>
                        </a></div>
                </div>
            </div>
        </div>
        <div id="imagenes">
            <h2>Material de Vista</h2>
            <?php include('guias_listado.php'); ?>


            <br/><br/>
            <!-- HASTA AQUI CONTENIDO --></div>
        <br class="clearfloat"/>
        <div id="footer">
            <? include("pie.php"); ?>
            <!-- end #footer --></div>
        <!-- end #container --></div>
</body>
</html>
