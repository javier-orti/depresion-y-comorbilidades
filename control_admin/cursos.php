<?php include("../includes/conn.php");
include("auto.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $ptitulo ?></title>
    <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
    <?php include("scripts.php"); ?>
</head>

<body class="twoColLiqLtHdr">

<div id="container">
    <div id="header">
        <?php include("cabeza.php"); ?>
        <!-- end #header --></div>
    <div id="sidebar1">
        <?php include("menu.php"); ?>
        <!-- end #sidebar1 --></div>
    <div id="mainContent">
        <div id="submenu"><!-- DESDE AQUI SUBMENU -->
            <!-- HASTA AQUI SUBMENU --></div>
        <!-- DESDE AQUI CONTENIDO -->
        <h1 style="float: right;"> Cursos</h1>
        <h2>Cursos Registradas</h2>

        <?php
        $sql = "SELECT * FROM com_cursos ORDER BY id";
        $result = mysql_query($sql);
        ?>
        <table class=" table table-responsive table-course table-striped">
            <thead>
            <tr>
                <th class="text-center">Curso</th>
                <th class="text-center">Contenidos</th>
                <th class="text-center">Opciones</th>
            </tr>
            </thead>
            <tbody>
            <?php while ($row = mysql_fetch_array($result)) {


                ?>
                <!-- <tr>
                <td colspan="3"><?php echo $row['titulo']; ?>: http://formacion.zoomdiabetes65.es/action_alta.php?id=<?php echo $row['id']; ?><br />
CON = <?php echo $row['con']; ?><br />
ZON = <?php echo $row['zon']; ?>
</td>
                </tr>-->
                <tr>
                    <td class="text-center"><?php echo $row['titulo']; ?></td>
                    <td class="text-center">
                        <div class="row">
                            <div class="col-6">
                                <ul>
                                    <li><a href="contenidos.php?id=<?php echo $row['id']; ?>">Contenidos</a></li>
                                    <li><a href="comite.php?id=<?php echo $row['id']; ?>">Comite Editorial</a></li>
                                    <li><a href="modulos.php?id=<?php echo $row['id']; ?>">Modulos</a></li>
                                </ul>
                            </div>
                            <div class="col-6">
                                <ul>
                                    <li><a href="material.php?id=<?php echo $row['id']; ?>">Recursos</a></li>
                                    <li><a href="guias.php?id=<?php echo $row['id']; ?>">Guias</a></li>
                                    <li><a href="prelanding.php?id=<?php echo $row['id']; ?>">Prelanding</a></li>
                                </ul>
                            </div>
                        </div>

                    </td>
                    <td align="center"><a href="cursos_mod.php?id=<?php echo $row['id']; ?>"><img border="0"
                                                                                                  alt="Modificar"
                                                                                                  src="body/modif.gif"/></a>&nbsp;<a
                                href="cursos_elim.php?id=<?php echo $row['id']; ?>"
                                onClick="return confirm('Esta seguro de borrar este curso?');"><img border="0"
                                                                                                    alt="Eliminar"
                                                                                                    src="body/elim.gif"></a>&nbsp;
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <br/><br/>
        <!-- HASTA AQUI CONTENIDO --></div>
    <br class="clearfloat"/>
    <div id="footer">
        <?php include("pie.php"); ?>
        <!-- end #footer --></div>
    <!-- end #container --></div>
</body>
</html>
