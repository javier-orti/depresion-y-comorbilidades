<?php include("../includes/conn.php");
include("../includes/extraer_variables.php");
include("auto.php");


$sql = "SELECT * FROM com_cursos_mod WHERE id=" . $id . "";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);

$sql0 = "SELECT * FROM com_cursos WHERE id=" . $row['curso'] . "";
$result0 = mysql_query($sql0);
$row0 = mysql_fetch_array($result0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= $ptitulo ?></title>
    <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
    <?php include("scripts.php"); ?>
    <script>
        $(function () {
            $(".datepicker1").datepicker({
                dateFormat: "yy/mm/dd",
                showOn: "button",
                buttonImage: "images/calendar.gif",
                buttonImageOnly: true,
                buttonText: "Select date"
            });
        });
    </script>
</head>

<body class="twoColLiqLtHdr">

<div id="container">
    <div id="header">
        <?php include("cabeza.php"); ?>
        <!-- end #header --></div>
    <div id="sidebar1">
        <?php include("menu.php"); ?>
        <!-- end #sidebar1 --></div>
    <div id="mainContent">
        <div id="submenu"><!-- DESDE AQUI SUBMENU -->
            <!-- HASTA AQUI SUBMENU --></div>
        <!-- DESDE AQUI CONTENIDO -->
        <h1>Acreditaciones del Modulo: <?php echo $row['titulo']; ?> del curso: <?php echo $row0['titulo']; ?></h1>
        <div class="box">
            <h2>Agregar período de acreditacion al Modulo </h2>
            <form method="POST" action="modulos_acred_add.php?modulo=<?php echo $id; ?>">

                <div class="row">
                    <div class="col-4"><label><span>Creditos: </span>
                            <input class="form-control" type="text" name="creditos" size="10"></label></div>
                    <div class="col-4"><label><span>No. Acreditacion: </span>
                            <input class="form-control" type="text" name="no_acred" size="20"></label></div>
                    <div class="col-4"><label><span>Periodo: </span>
                            <input class="form-control" type="text" name="periodo" size="20"></label></div>
                </div>
                <div class="row">
                    <div class="col-4"> <label><span>Horas: </span>
                            <input  type="text" name="horas" size="10" class="numerico form-control"></label></div>
                    <div class="col-4"><label><span>Acreditado desde: </span>
                            <input  type="text" name="acred_desde" class="datepicker1 form-control" size="20" readonly="readonly"></label></div>
                    <div class="col-4"><label><span>Acreditado hasta: </span>
                            <input type="text" name="acred_hasta" class="datepicker1 form-control" size="20" readonly="readonly"></label></div>
                </div>
                <label><span>Acreditado?: </span>
                    <input type="checkbox" name="acreditado" value="1"></label>

                <div><input class="btn btn-primary" type="submit" value="Enviar" name="B1"/></div>
            </form>
        </div>
        <h2>Acreditaciones del Modulo</h2>
        <?php
        $sql_1 = "SELECT * FROM com_acreditaciones WHERE modulo = " . $id . " ORDER BY acred_desde";
        $result_1 = mysql_query($sql_1);
        ?>
        <table class="table">
            <tr class="nodrop nodrag">
                <td width="45%" align="center">Desde</td>
                <td width="15%" align="center">Hasta</td>
                <td width="25%" align="center">Acciones</td>

            </tr>
            <?php $conty = 1;
            while ($row_1 = mysql_fetch_array($result_1)) {
                //$descr = strip_tags($row['fra']);
                ?>
                <tr>
                    <td align="center"><?php echo $row_1['acred_desde'] ?></td>
                    <td align="center"><?php echo $row_1['acred_hasta'] ?></td>

                    <td align="center">
                        <a href="modulos_acred_mod.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>&curso=<?php echo $ref; ?>"><img
                                    border="0" alt="Modificar" title="Modificar" src="body/modif.gif"></a>
                        <a href="modulos_acred_elim.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>&curso=<?php echo $ref; ?>"
                           onClick="return confirm('Seguro de eliminar esta acreditacion?');"><img border="0"
                                                                                                   alt="Eliminar"
                                                                                                   title="Eliminar"
                                                                                                   src="body/elim.gif"></a>


                    </td>


                </tr>
            <?php } ?>
        </table>
        <div id="AjaxResult"></div>
        <br/><br/>


        <br/><br/>
        <!-- HASTA AQUI CONTENIDO --></div>
    <br class="clearfloat"/>
    <div id="footer">
        <?php include("pie.php"); ?>
        <!-- end #footer --></div>
    <!-- end #container --></div>
</body>
</html>
