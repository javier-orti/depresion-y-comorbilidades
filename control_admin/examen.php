<?php include("../includes/conn.php");
include("auto.php");
include("../includes/extraer_variables.php");

$sql = "SELECT * FROM com_cursos WHERE id=".$ref."";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);

$sql_mod = "SELECT * FROM com_cursos_mod WHERE id=".$id."";
$result_mod = mysql_query($sql_mod);
$row_mod = mysql_fetch_array($result_mod);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$ptitulo?></title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
<?php include("scripts.php");?>
</head>

<body class="twoColLiqLtHdr">

    <div id="container"> 
      <div id="header">
        <?php include("cabeza.php");?>
      <!-- end #header --></div>
      <div id="sidebar1">
        <?php include("menu.php");?>
      <!-- end #sidebar1 --></div>
      <div id="mainContent">
      <div id="submenu"><!-- DESDE AQUI SUBMENU -->
      <!-- HASTA AQUI SUBMENU --></div>
      <!-- DESDE AQUI CONTENIDO -->
        <h1>Examen del Modulo: <?php echo $row_mod['titulo'];?></h1>
        <div class="box">
        <h2>Agregar Pregunta al examen</h2>
        <form method="POST" action="examen_add.php?capitulo=<?php echo $id;?>&modulo=<?php echo $ref;?>" id="form">
        <label><span>Numero: </span><input type="text" name="numero"></label>
        <label><span>Pagina: </span>
            <select name="pagina">
              <?php $sql_pag = "SELECT * FROM com_capitulo_contenidos WHERE capitulo = ".$id." ORDER BY orden";
           //echo $sql_ex;
           $result_pag = mysql_query($sql_pag,$link) or die ("Error en el examen");
           while($row_pag = mysql_fetch_array($result_pag)) {?>
              <option value="<?php echo $row_pag['id'];?>"><?php echo $row_pag['subtitulo']." ".$row_pag['titulo'];?></option>
          <?php } ?>
            </select>
      </label>
        <label><span>Cod. Video Vimeo: </span><input type="text" name="video"></label>
        <label><span>Pregunta: </span>&nbsp;</label>
         <textarea id="editor1" name="pregunta" rows="6" cols="60"></textarea>
         <script type="text/javascript">


	var editor = CKEDITOR.replace( 'editor1',

    {

        toolbar :

        [

            ['Source','-','Preview','-','Templates'],
    ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
   
    '/',
    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ['BidiLtr', 'BidiRtl' ],
    ['Link','Unlink','Anchor'],
    ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
    '/',
    ['Styles','Format','Font','FontSize'],
    ['TextColor','BGColor'],
    ['Maximize', 'ShowBlocks','-','About']

        ], 
		stylesCombo_stylesSet: 'my_styles:<?php echo $baseURLcontrol;?>js/styles.js',
     contentsCss : '<?php echo $baseURLcontrol;?>css/losstilos.css',
		

    });
	//editor.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );

	// Just call CKFinder.SetupCKEditor and pass the CKEditor instance as the first argument.
	// The second parameter (optional), is the path for the CKFinder installation (default = "/ckfinder/").
	editor.config.templates_files = [ '<?php echo $baseURLcontrol;?>js/mytemplates.js' ];
	CKFinder.setupCKEditor( editor, '<?php echo $baseURL;?>plugins/ckfinder/' ) ;

	// It is also possible to pass an object with selected CKFinder properties as a second argument.
	// CKFinder.SetupCKEditor( editor, { BasePath : '../../', RememberLastFolder : false } ) ;
</script>
 <label><span>Explicacion de la respuesta: </span>&nbsp;</label>
         <textarea name="exp_resp" rows="6" cols="60"></textarea>
         <input type="hidden" name="cant" id="cant" />
         
         <div class="fmr_sub">Respuestas</div>
         
        <div id="div_1">
<label>
<span class="small">Respuesta</span>
</label>
<input type="text" name="respuesta1" style="width:200px;"/> Correcta?<input type="checkbox" name="correcta1" style="width:40px;" value="1"/><input class="bt_plus" id="1" type="button" value="+"/><input type="hidden" name="laid1" value=""/><div class="error_form">
<br class="clearfloat" />
</div>
</div>

        
      
        
       
       <div class="spacer"><input type="submit" value="Enviar" name="B1" /></div>
        </form>
        </div>
        <h2>Preguntas del Examen (<a href="examen_resultados.php?id=<?php echo $id?>&ref=<?php echo $ref;?>">Ver preguntas y respuestas</a>)</h2>
        <?php
          $sql_1 = "SELECT * FROM com_exam_preg WHERE capitulo = ". $id ." ORDER BY pagina, orden1";
          $result_1 = mysql_query($sql_1);
    ?>
    <table cellpadding="0" cellspacing="0" border="1" width="95%" align="center" id="table-6">
        <tr class="nodrop nodrag">
        <td width="15%" align="center">Orden</td>
        <td width="45%" align="center">Pregunta</td>
        <td width="15%" align="center">Resp. (correc)</td>
        <td width="25%" align="center">Acciones</td>
      
        </tr>
        <?php $conty = 1;
		while ($row_1 = mysql_fetch_array($result_1)) { 
		
		$sql_2 = "SELECT * FROM com_exam_resp WHERE pregunta = ". $row_1['id'] ." ORDER BY id";
        $result_2 = mysql_query($sql_2);
		$cant_res = mysql_num_rows($result_2);
		
		
		$sql_3 = "SELECT * FROM com_exam_resp WHERE pregunta = ". $row_1['id'] ." AND correcta = 1 ORDER BY id";
        $result_3 = mysql_query($sql_3);
		$cant_cor = mysql_num_rows($result_3);

    $sql_4 = "SELECT * FROM com_capitulo_contenidos WHERE id = ". $row_1['pagina'] ." LIMIT 1";
        $result_4 = mysql_query($sql_4);
        $row_4 = mysql_fetch_array($result_4);
		
		?>
        <tr id="table6-row-<?=$row_1['id']?>">
          <td class="dragHandle"><?php echo $conty;?></td>
          <td><?php echo "<strong>". $row_4['titulo'].":</strong> ".$row_1['num']."<br>".strip_tags($row_1['pregunta'])?></td>
          <td align="center"><?php echo $cant_res . " (".$cant_cor.")"?></td>
         
          <td align="center">
              <a href="examen_mod.php?id=<?php echo $row_1['id'];?>&ref=<?php echo $id?>"><img border="0" alt="Modificar" title="Modificar" src="body/modif.gif"></a>
              <a href="examen_elim.php?id=<?php echo $row_1['id'];?>&ref=<?php echo $id?>" onClick="return confirm('Seguro de eliminar esta pregunta?');"><img border="0" alt="Eliminar" title="Eliminar" src="body/elim.gif"></a>
               
              
          </td>
        
       
        
      </tr>
      <?php $conty = $conty + 1;
	  } ?>
        </table>
        <div id="AjaxResult"></div>
    <br /><br />
    <script type="text/javascript"> 
 <?php include_once('script_ordenar_preg.php');?>
 </script>
 
 
    <br /><br />
    	<!-- HASTA AQUI CONTENIDO --></div>
    	<br class="clearfloat" />
      <div id="footer">
        <?php include("pie.php"); ?>
      <!-- end #footer --></div>
    <!-- end #container --></div>
    </body>
</html>
