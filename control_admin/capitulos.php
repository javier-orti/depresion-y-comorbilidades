<?php include("../includes/conn.php");
include("auto.php");
include("../includes/extraer_variables.php");

$sql = "SELECT * FROM com_cursos_mod WHERE id=" . $id . "";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);

$sql0 = "SELECT * FROM com_cursos WHERE id=" . $row['curso'] . "";
$result0 = mysql_query($sql0);
$row0 = mysql_fetch_array($result0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $ptitulo ?></title>
    <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
    <?php include("scripts.php"); ?>
</head>

<body class="twoColLiqLtHdr">

<div id="container">
    <div id="header">
        <?php include("cabeza.php"); ?>
        <!-- end #header --></div>
    <div id="sidebar1">
        <?php include("menu.php"); ?>
        <!-- end #sidebar1 --></div>
    <div id="mainContent">
        <div id="submenu"><!-- DESDE AQUI SUBMENU -->
            <!-- HASTA AQUI SUBMENU --></div>
        <!-- DESDE AQUI CONTENIDO -->
        <h1>Capitulos del Modulo: <?php echo $row['titulo']; ?> del curso: <?php echo $row0['titulo']; ?></h1>
        <div class="box">
            <h2>Agregar Capitulo al Modulo </h2>
            <form method="POST" action="capitulos_add.php?modulo=<?php echo $id; ?>">
                <div class="row">
                    <div class="col-4"><label><span>Caso: </span>
                            <input class="form-control" type="text" name="caso" size="20"></label></div>
                    <div class="col-4"><label><span>Titulo Español: </span>
                            <input class="form-control" type="text" name="titulo" size="20"></label></div>
                    <div class="col-4"><label><span>Titulo Ingles: </span>
                            <input class="form-control" type="text" name="titulo_eng" size="20"></label></div>
                </div>

                <div class="row">
                    <div class="col-4"><label><span>Video de intro: </span>
                            <input class="form-control" type="text" name="video" size="20"></label></div>
                    <div class="col-4"><label><span>Revista: </span>
                            <input class="form-control" type="text" name="revista" size="20"></label></div>
                    <div class="col-4"><label><span>Tema: </span>
                            <input class="form-control" type="text" name="tema" size="20"></label></div>
                </div>
                <div class="row">
                    <div class="col-6"><label><span>Autor: </span>
                            <input class="form-control" type="text" name="autor" size="20"></label></div>
                    <div class="col-6"><label><span>Video: </span>
                            <input class="form-control" type="text" name="video" size="20"></label></div>
                </div>
                <label><span>Pestaña de descargas en menu: </span>
                    <input type="checkbox" name="sub_menu" value="1"></label>
                <div class="row">
                    <div class="col-12">
                        <label><span>Rese&ntilde;a del Autor: </span><textarea class="form-control" name="resena_autor"
                                                                               rows="4"
                                                                               cols="40"></textarea></label>
                    </div>
                </div>
                <div class="fmr_sub">Primera Pagina</div>
                <br>
                <div class="row">
                    <div class="col-4"><label><span>Titulo Pagina: </span>
                            <input type="text" class="form-control" name="titulo_con" size="20"></label></div>
                    <div class="col-4"><label><span>Sub-Titulo Pagina: </span>
                            <input type="text" class="form-control" name="subtitulo_con" size="20"></label></div>
                    <div class="col-4"><label><span>Autor: </span>
                            <input class="form-control" type="text" name="autor_con" size="20"></label></div>
                </div>

                <div class="row">
                    <label id="ver_contenido"><span>Tipo Contenido: </span>
                        <div class="col-6">
                            <input type="radio" name="tipo" value="cont">Contenido
                    </label>
                </div>
                <div class="col-6">
                    <label id="ver_video">
                        <input type="radio" name="tipo" value="video">Diapositivas</label>
                </div>

        </div>


        <div id="pag_cont" class="oculto">
            <label><span>Contenido: </span>&nbsp;</label>
            <textarea id="editor1" name="contenido" rows="10" cols="80"></textarea>
            <script type="text/javascript">


                var editor = CKEDITOR.replace('editor1',

                    {

                        toolbar:

                            [

                                ['Source', '-', 'Preview', '-', 'Templates'],
                                ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print', 'SpellChecker', 'Scayt'],
                                ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],

                                '/',
                                ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
                                ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
                                ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                                ['BidiLtr', 'BidiRtl'],
                                ['Link', 'Unlink', 'Anchor'],
                                ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
                                '/',
                                ['Styles', 'Format', 'Font', 'FontSize'],
                                ['TextColor', 'BGColor'],
                                ['Maximize', 'ShowBlocks', '-', 'About']

                            ],
                        stylesCombo_stylesSet: 'my_styles:<?php echo $baseURLcontrol;?>js/styles.js',
                        contentsCss: '<?php echo $baseURLcontrol;?>css/losstilos.css',


                    });
                editor.setData('<?php echo eregi_replace("[\n|\r|\n\r]", ' ', utf8_encode($row['contenido']));  ?>');

                // Just call CKFinder.SetupCKEditor and pass the CKEditor instance as the first argument.
                // The second parameter (optional), is the path for the CKFinder installation (default = "/ckfinder/").
                editor.config.templates_files = ['<?php echo $baseURLcontrol;?>js/mytemplates.js'];
                CKFinder.setupCKEditor(editor, '<?php echo $baseURL;?>plugins/ckfinder/');

                // It is also possible to pass an object with selected CKFinder properties as a second argument.
                // CKFinder.SetupCKEditor( editor, { BasePath : '../../', RememberLastFolder : false } ) ;
            </script>
        </div>
        <div id="pag_video" class="oculto">
        </div>
        <div class="text-center"><input class="btn btn-primary" type="submit" value="Enviar" name="B1"/></div>
        </form>
    </div>
    <h2>Capitulos del Modulo (<a href="capitulos_indexar_todos.php?modulo=<?php echo $id ?>">indexar todos los
            capitulos al buscador</a>)</h2>
    <?php
    $sql_1 = "SELECT * FROM com_cursos_mod_cap WHERE modulo = " . $id . " ORDER BY orden";
    $result_1 = mysql_query($sql_1);
    ?>
    <table class="table table-striped">
        <tr class="nodrop nodrag">
            <td width="15%" align="center">Orden</td>
            <td width="45%" align="center">Titulo</td>
            <td width="15%" align="center">Capitulos</td>
            <td width="25%" align="center">Acciones</td>

        </tr>
        <?php $conty = 1;
        while ($row_1 = mysql_fetch_array($result_1)) {
            //$descr = strip_tags($row['fra']);
            $sql_2 = "SELECT * FROM com_capitulo_contenidos WHERE capitulo = " . $row_1['id'] . " AND tipo = 'video' ORDER BY orden";
            $result_2 = mysql_query($sql_2);
            $codigover = '';
            if ($row_2 = mysql_fetch_array($result_2)) {
                $codigover = "cod: " . $row_2['id'];
            }
            ?>
            <tr id="table6-row-<?php echo $row_1['id'] ?>">
                <td class="dragHandle">&nbsp;</td>
                <td align="center">Capitulo <strong><?php echo $conty ?></strong>: <strong><?php //$codigover;
                        echo strlen(urls_amigables($row_1['titulo'])) ?></strong> <?php echo $row_1['caso'] . "<br>" . $row_1['titulo'] ?>
                </td>
                <td>
                    <ul>
                        <li><a href="paginas.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>">Paginas</a></li>
                        <li><a href="capitulos_cap_down.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>">Descargas</a></li>
                        <li><a href="examen.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>">Examen</a></li>
                    </ul>
                </td>
                <td align="center">
                    <a href="capitulos_up.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>"><img border="0"
                                                                                                         alt="Imagen"
                                                                                                         title="Imagen"
                                                                                                         src="body/jpg.png"></a>
                    <a href="capitulos_mod.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>&curso=<?php echo $ref; ?>"><img
                                border="0" alt="Modificar" title="Modificar" src="body/modif.gif"></a>
                    <a href="capitulos_elim.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>&curso=<?php echo $ref; ?>"
                       onClick="return confirm('Seguro de eliminar este modulo?');"><img border="0" alt="Eliminar"
                                                                                         title="Eliminar"
                                                                                         src="body/elim.gif"></a>
                    <?php if ($row_1['estado'] == 0) { ?>
                        <a href="capitulos_estado.php?st=1&id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>&curso=<?php echo $ref; ?>"><img
                                    border="0" src="body/suspender.gif" title="Click para Activar"></a>&nbsp;
                    <?php } else { ?>
                        <a href="capitulos_estado.php?st=0&id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>&curso=<?php echo $ref; ?>"><img
                                    border="0" src="body/activar.gif" title="Click para Suspender"></a>&nbsp;
                        <a href="capitulos_down.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>&curso=<?php echo $ref; ?>"><img
                                    src="body/pdf_icon.png"/></a>&nbsp;
                        <a href="capitulos_ppt.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>&curso=<?php echo $ref; ?>"><img
                                    src="body/ppt.png" title="Subir PPT" alt="Subir PPT"/></a>&nbsp;
                        <a href="capitulos_zip.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>&curso=<?php echo $ref; ?>"><img
                                    src="body/zip.png"/></a>&nbsp;
                                                            &nbsp;<a
                                href="capitulos_print.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>&curso=<?php echo $ref; ?>"
                                target="_blank"><img src="body/print.png"/></a>
                        &nbsp;<a
                                href="capitulos_indexar.php?id=<?php echo $row_1['id']; ?>&ref=<?php echo $id ?>&curso=<?php echo $ref; ?>"><img
                                    src="body/indexar.jpg" title="Indexar al buscador"
                                    alt="Indexar al buscador"/></a>
                        <?php
                        $conty = $conty + 1;
                    } ?>
                </td>
            </tr>
        <?php } ?>
    </table>
    <div id="AjaxResult"></div>
    <br/><br/>
    <script type="text/javascript">
        <?php include_once('script_ordenar_cap.php');?>
    </script>


    <br/><br/>
    <!-- HASTA AQUI CONTENIDO --></div>
<br class="clearfloat"/>
<div id="footer">
    <?php include("pie.php"); ?>
    <!-- end #footer --></div>
<!-- end #container --></div>
</body>
</html>
