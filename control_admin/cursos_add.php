<?php include("../includes/conn.php");
include("auto.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $ptitulo ?></title>
    <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
    <?php include("scripts.php"); ?>
</head>

<body class="twoColLiqLtHdr">

<div id="container">
    <div id="header">
        <?php include("cabeza.php"); ?>
        <!-- end #header --></div>
    <div id="sidebar1">
        <?php include("menu.php"); ?>
        <!-- end #sidebar1 --></div>

    <div id="mainContent">
        <div id="submenu"><!-- DESDE AQUI SUBMENU -->
            <!-- HASTA AQUI SUBMENU --></div>
        <!-- DESDE AQUI CONTENIDO -->

        <div class="box">
            <h2 style="float:right;"> CURSOS</h2>
            <h2>Agregar Curso </h2>

            <form method="POST" action="cursos_add1.php">
                <label><span>Nombre: </span>
                    <input class="form-control" type="text" name="titulo" size="20"></label>
                <label><span>CON: </span>
                    <input class="form-control" type="text" name="con" size="20"></label>
                <label><span>ZON: </span>
                    <input class="form-control" type="text" name="zon" size="20"></label>
                <div>
                    <label><span>Examen unico del curso: </span><input type="checkbox" name="ex_unico" value="1"/></label>
                </div>
                <label><span>Introduccion: </span>&nbsp;</label>
                <textarea class="form-control" id="editor1" name="bienvenida" rows="5" cols="80"></textarea>
                <br>

                <div><input class="btn btn-primary" type="submit" value="AGREGAR" name="B1"/></div>

                <script type="text/javascript">

                    var editor = CKEDITOR.replace('editor1',

                        {

                            toolbar:

                                [

                                    ['Source', '-', 'Preview', '-', 'Templates'],
                                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print', 'SpellChecker', 'Scayt'],
                                    ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],

                                    '/',
                                    ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
                                    ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
                                    ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                                    ['BidiLtr', 'BidiRtl'],
                                    ['Link', 'Unlink', 'Anchor'],
                                    ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
                                    '/',
                                    ['Styles', 'Format', 'Font', 'FontSize'],
                                    ['TextColor', 'BGColor'],
                                    ['Maximize', 'ShowBlocks', '-', 'About']

                                ],
                            stylesCombo_stylesSet: 'my_styles:<?php echo $baseURLcontrol;?>js/styles.js',
                            contentsCss: '<?php echo $baseURLcontrol;?>css/losstilos.css',


                        });
                    //editor.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );

                    // Just call CKFinder.SetupCKEditor and pass the CKEditor instance as the first argument.
                    // The second parameter (optional), is the path for the CKFinder installation (default = "/ckfinder/").
                    editor.config.templates_files = ['<?php echo $baseURLcontrol;?>js/mytemplates.js'];
                    CKFinder.setupCKEditor(editor, '<?php echo $baseURL;?>plugins/ckfinder/');

                    // It is also possible to pass an object with selected CKFinder properties as a second argument.
                    // CKFinder.SetupCKEditor( editor, { BasePath : '../../', RememberLastFolder : false } ) ;
                </script>

            </form>
        </div>

        <br/><br/>
        <!-- HASTA AQUI CONTENIDO --></div>
    <br class="clearfloat"/>
    <div id="footer">
        <?php include("pie.php"); ?>
        <!-- end #footer --></div>
    <!-- end #container --></div>
</body>
</html>
