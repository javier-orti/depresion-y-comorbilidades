<?php

error_reporting(E_ALL);
ini_set('display_errors', '0'); 
$page = 'registro';
$tipologin = 'off';
include('header.php'); ?>
<div class="main">
    <div class="container">
        <div class="registro">
            <a href="login.php">
                < Volver</a>
                    <br><br>
                    <h2 class="color2">CREAR CUENTA</h2>
                    <div>

                        <br>
                        

                    <p><b>Área de acceso restringido</b>: esta área contiene información dirigida exclusivamente a profesionales sanitarios facultados para prescribir o dispensar medicamentos en España (requiere una formación especializada para su correcta interpretación).</p>

                    <p class="color1"><b>CLAVES DE ACCESO A LOS SERVICIOS RESTRINGIDOS</b></p>

                     <?php   if ($err=='6') { ?>
          <div class="alert alert-danger">El email que está intentado registrar ya está registrado en la base de datos, use su correo y contraseña para acceder o recupere su contraseña si la ha olvidado.</div>
          <?php } ?>

                    <form action="action_registro.php?action=registro" method="post" id="registro" >
                      <div class="row row-cols-1 row-cols-lg-3">
                                <div class="col">
                                    <p>Nombre *</p>
                                    <input type="text" name="usu_nombre" class="form-control" required>
                                </div>
                                <div class="col">
                                    <p>Primer apellido *</p>
                                    <input type="text" name="usu_ape1" class="form-control" required>
                                </div>
                                <div class="col">
                                    <p>Segundo apellido *</p>
                                    <input type="text" name="usu_ape2" class="form-control" required>
                                </div>
                                <div class="col">
                                    <p>Correo electrónico *</p>
                                    <input type="email" name="usu_email" class="form-control" required>
                                </div>
                                <div class="col">
                                    <p>Contraseña *</p>
                                    <input type="password" name="usu_password" id="usu_password" class="form-control" required>
                                </div>
                                <div class="col">
                                    <p>Confirmar contraseña *</p>
                                    <input type="password" name="usu_password2" id="usu_password2" class="form-control" required>
                                </div>
                                <div class="col">
                                    <p>Profesión *</p>
                                    <select name="usu_codperfil" id="usu_codperfil" class="form-control">
                                        <option value="0">Selecciona una opción</option>
                                        <option value="ME">Médico</option>
                                        <option value="FA">Farmacéutico</option>
                                        <option value="AX">Auxiliar de Farmacia</option>
                                        <option value="OT">Otros</option>
                                        <option value="RS">Residente</option>
                                        <option value="EN">Enfermero</option>
                                    </select>
                                </div>
                                <div class="col">
                                    <p>Especialidad *</p>
                                    <select class="form-control" name="usu_codespecialidad" id="usu_codespecialidad" data-validation="required" disabled="disabled" autocomplete="off">
                                        <option value="">Especialidad</option>
                                        <option value="4">Alergia</option>
                                        <option value="5">Analistas</option>
                                        <option value="6">Anatomia patológica</option>
                                        <option value="3">Andrólogo</option>
                                        <option value="8">Anestesistas</option>
                                        <option value="46">Angiología</option>
                                        <option value="1">Aparato circulatorio</option>
                                        <option value="2">Aparato digestivo/proctología</option>
                                        <option value="7">Aparato respiratorio</option>
                                        <option value="9">Bacteriología</option>
                                        <option value="10">Cardiología</option>
                                        <option value="11">Cirugía</option>
                                        <option value="45">Cirugía vascular</option>
                                        <option value="12">Citología</option>
                                        <option value="13">Dermatología</option>
                                        <option value="14">Diabetología</option>
                                        <option value="47">Drogodependencia</option>
                                        <option value="16">Endocrinología</option>
                                        <option value="15">Enfermedades infecciosas</option>
                                        <option value="56">Enfermería</option>
                                        <option value="49">Epidemiología</option>
                                        <option value="18">Farmacéutico</option>
                                        <option value="17">Farmacología</option>
                                        <option value="19">Geriatría</option>
                                        <option value="20">Hematología / hemoterapia</option>
                                        <option value="21">Medicina de empresa</option>
                                        <option value="22">Medicina familiar</option>
                                        <option value="23">Medicina general</option>
                                        <option value="42">Medicina hospitalaria</option>
                                        <option value="24">Medicina interna</option>
                                        <option value="50">Medicina Preventiva</option>
                                        <option value="57">Medicina Tropical</option>
                                        <option value="52">Micología</option>
                                        <option value="25">Nefrología</option>
                                        <option value="41">Neumología</option>
                                        <option value="60">Neurofisiología</option>
                                        <option value="59">Neuro-Pediatría</option>
                                        <option value="26">Neuro-psiquiatría</option>
                                        <option value="44">Neurocirugía</option>
                                        <option value="27">Neurología</option>
                                        <option value="28">Odontología/estomatología</option>
                                        <option value="29">Oftalmología </option>
                                        <option value="30">Oncología</option>
                                        <option value="31">Otorrinolaringología</option>
                                        <option value="58">Otra</option>
                                        <option value="32">Pediatría</option>
                                        <option value="55">Psicología</option>
                                        <option value="33">Psiquiatría</option>
                                        <option value="34">Radiología</option>
                                        <option value="51">Radioterapia</option>
                                        <option value="36">Rehabilitación</option>
                                        <option value="35">Reumatología</option>
                                        <option value="43">Sin especialidad</option>
                                        <option value="37">Tocología ginecología</option>
                                        <option value="38">Traumatología</option>
                                        <option value="53">UCI - UVI</option>
                                        <option value="48">Unidades de tabaquismo</option>
                                        <option value="39">Urgencias</option>
                                        <option value="40">Urología</option>
                                        <option value="54">Vacunas</option>
                                        
                                    </select>
                                </div>
                                <div class="col">
                                    <p>Nº de colegiado *</p>
                                    <input type="text" name="usu_num" class="form-control" required>
                                </div>
                                <div class="col">
                                    <p>Centro de trabajo *</p>
                                    <input type="text" name="usu_centro" class="form-control" required>
                                </div>
                                <div class="col">
                                    <p>País *</p>
                                    <select class="form-control" name="usu_codpais" id="usu_codpais">
                                        <option value="">Seleccionar pais</option>
                                        <?php
                                        $pais = new Pais();
                                        $pais->getAll('todos');
                                        foreach ($pais->row as $Elem) {
                                        ?>
                                            <option value="<?php echo $Elem['codigo']; ?>"><?php echo $Elem['pais']; ?></option>

                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col">

                                    <div class="form-group" id="dv_provincia">
                                        <p class="verde">PROVINCIA *</p>
                                        <select class="form-control" name="usu_codprovestado" value="" id="usu_codprovestado" autocomplete="off" disabled>
                                            <option value="">Seleccionar Provincia*</option>
                                        </select>
                                    </div>
                                </div>




                            </div>
                            <br>

                      <div class="col">

                        <br>
                        
                        <div class="check">
                            

                          <input type="checkbox" name="acepto" id="acepto" class="css-checkbox" required>

                          <label for="acepto" class="css-label">He leído y acepto el <a href="legal.php" target="_blank" title="Aviso Legal">aviso legal</a> y la <a href="privacidad.php" target="_blank" title="Política de privacidad" class="info-privacy">política de privacidad</a>.</label>

                        </div>

                        <div class="clearfix"></div></br>

                        <div class="check">

                          <input type="checkbox" name="mailing" id="mailing" class="css-checkbox" value="S">

                          <label for="mailing" class="css-label">Deseo recibir <a href="privacidad.php" target="_blank" title="">comunicaciones comerciales del organizador

del curso THE BUTTERFLIES HEALTHCARE</a>.</label>

                        </div>

                        <div class="clearfix"></div></br>

                        <div class="check">

                          <input type="checkbox" name="mailing" id="mailing" class="css-checkbox" value="S">

                          <label for="mailing1" class="css-label">Acepto recibir <a href="privacidad.php" target="_blank" title="">comunicaciones comerciales del patrocinador del curso ESTEVE</a>.</label>

                        </div>

                      </div>
					  
					  <div class="form-group">
              		<div class="col-sm-2">
              		</div>
              		<div class="col-sm-12">
              		<!--	<div class="g-recaptcha" data-sitekey="6Ld_QHEaAAAAAFZvr42ova5CQXSVVV6viDiMikY-"></div>-->
              		</div>
                  <div class="clearfix"></div>
              	</div>

                      <div class="clearfix"></div></br>

                      <div class="col text-right">

                        <button type="submit" class="btn-login">CREAR CUENTA</button>




                      </div>

                      

                    </div>

                    



                    </form>

                    </div>
                    <br>

                    <br><br>
        </div>
    </div>

</div>
<?php include('footer.php'); ?>