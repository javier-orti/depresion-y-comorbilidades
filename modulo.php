<?php
ini_set('display_errors', '0');
$page = 'modulo';
include('header.php');
$mod = new Modulo();
$mod->getOne($id);
$mod->alumno = $authj->rowff['id'];

$tipoAcceso = 1;

$cap = new Capitulo();

if (!empty($orden)) {
    $cap->orden = $orden;
}

if (!empty($tiporden)) {
    $cap->tiporden = $tiporden;
}

if (!empty($pagi)) {
    $cap->pag = $pagi;
}

$cap->modulo = $id;

$cap->alumno = $authj->rowff['id'];
$cap->modulo = $id;
$cap->getAll();



if (empty($capitulo)) {
    $capitulo = $cap->row[0]['id'];
}


$pag0 = new Pagina();
$pag0->alumno = $authj->rowff['id'];

$cap1 = new Capitulo();
$cap1->getOne($capitulo);

$cap1->alumno = $authj->rowff['id'];
$cap1->registrarAcceso();

//echo "capitulo: ".$capitulo;

$pag = new Pagina();
$pag->alumno = $authj->rowff['id'];
$pag->capitulo = $capitulo;
$pag->getAll($capitulo);




if (empty($pagina)) {
    $pagina = $pag->row[0]['id'];
}

//echo "pagina";
$pag2 = new Pagina();
$pag2->alumno = $authj->rowff['id'];
$pag2->capitulo = $capitulo;
$pag2->getOne($pagina);


$exam = new Examen();
$exam->modulo = $cap->row[0]['modulo'];
$exam->alumno = $authj->rowff['id'];
if ($exam->checkPlazo() == 1) {
    $plazo_vencido = 1;
} else {
    $plazo_vencido = 0;
}
$estado_exam = $exam->getEstado();



//print_r($pag2->row);


/*$diap = new Diapositiva();
    $diap->getAll($pag->row[0]['id']);*/



$scripts = "none";
?>

<div class="modulo">


    <div class="titulo-modulo color2 text-center">
        <?php echo $mod->row[0]['titulo'] ?> | <?php echo $mod->row[0]['titulo_diploma'] ?>
    </div>
    <div class="row row-cols-1 row-cols-lg-2">
        <div class="col-12 col-lg-3 menubar">
            <div class="temario">
                <div class="temas">

                    <?php
                    $contador = 0;
                    $contaArray = 0;

                    $arrayPag = array();
                    $arrayCap = array();
                    $arrayTip = array();
                    foreach ($cap->row as $Elem) {
                        $pag0->capitulo = $Elem['id'];
                        $pag0->getAll($Elem['id']);
                        $cot_exa = 0;
                        $cot_real = 0; ?>
                        <a class="collapsetema" data-toggle="collapse" data-target="#collapse<?php echo $contador; ?>" aria-expanded="false" aria-controls="collapseExample">
                            <div class="tema-temario">
                                <p><?php echo $Elem['caso'] ?> | <?php echo $Elem['titulo'] ?></p><i class="chevron fa fa-chevron-down"></i>

                            </div>

                        </a>
                        <div class="collapse<?php if ($Elem['id'] == $capitulo) {
                                                echo " show";
                                            } ?>" id="collapse<?php echo $contador; ?>">
                            <?php
                            foreach ($pag0->row as $Elem2) {
                                $cot_exa++;

                                $verificar = Pagina::verificarAcceso($authj->rowff['id'], $Elem2['id'], 1);
                                //echo "aqui ".$verificar;
                                if ($verificar == 1) {
                                    $claseextra = " visto";
                                } else {
                                    $claseextra = "";
                                }

                                $verificar2 = Pagina::verificarAcceso($authj->rowff['id'], $Elem2['id'], 2);
                                //echo "aqui ".$verificar;
                                if ($verificar2 == 1) {
                                    $claseextra2 = " visto";
                                } else {
                                    $claseextra2 = "";
                                }
                                $arrayCont[$contaArray] = "";
                                $arrayPag[$contaArray] = $Elem2['id'];
                                $arrayCap[$contaArray] = $Elem['id'];
                                $arrayTip[$contaArray] = "cont";

                                if ($capitulo == $Elem['id'] && $pagina == $Elem2['id']) {
                                    $actual = $contaArray;
                                }

                                $contaArray++;

                            ?>
                                <!--

                    <span class="list-group-item list-group-item-action<?php echo $claseextra;
                                                                        if ($Elem2['id'] == $pag2->row[0]['id']) { ?> selected<?php } ?>"><span class="ocult"><?php echo $Elem2['subtitulo'] ?>| <?php echo $Elem2['titulo'] ?></span></span>

                    -->

                                <a href="modulo.php?id=<?php echo $mod->row[0]['id'] ?>&capitulo=<?php echo $Elem['id'] ?>&pagina=<?php echo $Elem2['id'] ?>" class="list-group-item list-group-item-action<?php echo $claseextra;
                                                                                                                                                                                                        if ($Elem2['id'] == $pag2->row[0]['id']) { ?> selected<?php } ?>"><span class="ocult">APARTADO<br><small><?php echo $Elem2['titulo'] ?></small></span></a>
                                <?php if (Diapositiva::contarDiapo($Elem2['id']) > 0) { ?>
                                    <a href="modulo-caso.php?id=<?php echo $mod->row[0]['id'] ?>&capitulo=<?php echo $Elem['id'] ?>&pagina=<?php echo $Elem2['id'] ?>" class="list-group-item list-group-item-action<?php echo $claseextra2; ?>"><span class="ocult">CASO CLÍNICO<br><small><?php echo $Elem2['titulo'] ?></small></span></a>
                                <?php } ?>
                                <?php if ($Elem2['examen'] == 1) { ?>
                                    <a class="exam-menu" href="examen.php"><i class="fa fa-graduation-cap"></i> Examen del bloque </a>
                                <?php } ?>
                            <?php


                                //echo $mod->row[0]['id']." - ".$Elem2['id']."<br>";
                                $examen = Examen::estadoExamenPag($mod->row[0]['id'], $Elem2['id'], $authj->rowff['id']);
                                if ($examen == 1) {
                                    $cot_real++;
                                }
                                $pag2->registrarAcceso($tipoAcceso);
                            }
                            $arrayCont[$contaArray] = "";
                            $arrayPag[$contaArray] = $Elem2['id'];
                            $arrayCap[$contaArray] = $Elem['id'];
                            $arrayTip[$contaArray] = "exam";
                            $contaArray++;
                            $estExamCap = Examen::estadoExamenCap($mod->row[0]['id'], $Elem['id'], $authj->rowff['id']);
                            ?>
                            <a href="cexamen.php?id=<?php echo $mod->row[0]['id'] ?>&capitulo=<?php echo $Elem['id'] ?>&pagina=<?php echo $Elem2['id'] ?>" class="exam-menu<?php if ($estExamCap == 1) {  ?> visto<?php } ?>"><i class="fa fa-graduation-cap"></i>&nbsp;<b>EXAMEN <?php echo $Elem['caso'] ?></b></a>

                        </div>
                    <?php
                        // echo $Elem['id'] ."==". $cap1->row[0]['id']." contador".$contador;
                        if ($Elem['id'] == $cap1->row[0]['id']) {
                            $contador_ant = $contador - 1;
                        }
                        if ($Elem['id'] == $cap1->row[0]['id']) {
                            $contador_sig = $contador + 1;
                        }
                        $contador++;
                        $contador_ult = $contador - 1;
                    }
                    $cap1->registrarAcceso();
                    ?>
                    <!-- <a href="modulovideo.php" class="list-group-item list-group-item-action"><b></b><br><span class="ocult"></span></a>
                    <a href="modulo.php" class="list-group-item list-group-item-action "><b>1.3</b><br><span class="ocult">Epilepsias focales</span></a>
                    <a href="modulo.php" class="list-group-item list-group-item-action "><b>1.4</b><br><span class="ocult">Síndromes epilépticos</span></a>
                    <a href="modulo.php" class="list-group-item list-group-item-action "><b>1.5</b><br><span class="ocult">Diagnóstico diferencial: episodios no epilépticos</span></a>
                    <a href="modulo.php" class="list-group-item list-group-item-action "><b>1.6</b><br><span class="ocult">Status epilepticus</span></a>
                    
                    -->

                </div>

            </div>

        </div>
        <!-- Page Content -->
        <div class="col-12 col-lg-7 repro">


            <div class="text-center">
                <?php if ($plazo_vencido == 1 && $estado_exam < 3) { ?>
                    <p><b>El plazo para terminar el examen desde el momento en que inició el módulo ha finalizado.</b></p>

                <?php } else if ($estado_exam >= 3) { ?>
                    <p><b>Ya has finalizado el examen de este módulo.</b></p>
                <?php } else { ?>
                    <p><b style="font-size:16px;">Dispone de <?php echo $exam->duracion; ?> días para finalizar el examen del módulo</b></small></p>
                <?php } ?>
            </div>

            <div class="tema">

                <?php if ($pag->row[0]['contenido']) {  ?>
                    <div class="textointro">
                        <?php echo $pag->row[0]['contenido']; ?>
                        <!--
                        <h4 class="color2">Introducción al curso</h4>
                        <p class="color1">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, se  im ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.<br><br>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.<br><br>
                            Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.<br><br>
                        </p>  
                        -->
                    </div>
                <?php  } else { ?>
                    <div class="video embed-responsive embed-responsive-16by9 text-center">
                        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/<?php echo $pag2->row[0]['video']; ?>" width="896" height="504" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                <?php  }  ?>

                <div class="row dentro">

                    <div class="col-7 titulotema text-left">
                        <!--<h2 class="color2"><?php echo $cap1->row[0]['caso'] ?> | <span class="color3"><small><?php echo $pag2->row[0]['subtitulo'] . " - " . $pag2->row[0]['titulo'] ?></small></span></h2>
                    -->
                    </div>

                    <div class="col-5 siguiente color2 text-right">
                        <?php if ($actual != 0) {
                            if ($arrayTip[$actual - 1] != "exam") { ?>

                                <!--
                            <a class="btn-siguiente" href="modulo.php?id=<?php echo $id ?>&capitulo=<?php echo $arrayCap[$actual - 1]; ?>&pagina=<?php echo $arrayPag[$actual - 1] ?>"><i class="fa fa-step-backward"></i></a>
                                    -->
                        <?php
                            }
                        }  ?>
                        <?php
                        //echo $actual ." - ".$contaArray;
                        if ($actual <= ($contaArray)) {

                            // echo $arrayTip[$actual+1];
                        ?>
                            <?php if (Diapositiva::contarDiapo($pagina) > 0) { ?>
                                <a href="modulo-caso.php?id=<?php echo $id ?>&capitulo=<?php echo $capitulo; ?>&pagina=<?php echo $pagina ?>" class="btn-siguiente<?php if ($cap->row[$contador_ult]['id'] == $cap1->row[0]['id']) { ?> disabled0<?php } ?>"><i class="fa fa-step-forward"></i>&nbsp;&nbsp;&nbsp;SIGUIENTE BLOQUE</a>
                            <?php } else { ?>

                                <?php
                                //echo $actual ." - ".$contaArray;
                                if ($actual <= ($contaArray)) {

                                    // echo $arrayTip[$actual+1];
                                    if ($arrayTip[$actual + 1] == "exam") {
                                ?>
                                        <br><br>
                                        <a href="cexamen.php?id=<?php echo $id ?>&capitulo=<?php echo $capitulo; ?>&pagina=<?php echo $pagina ?>" class="btn-siguiente<?php if ($cap->row[$contador_ult]['id'] == $cap1->row[0]['id']) { ?> disabled0<?php } ?>"><i class="fa fa-step-forward"></i>&nbsp;&nbsp;&nbsp;EXAMEN</a>

                                    <?php  } else { ?>
                                        <br><br>
                                        <a href="modulo.php?id=<?php echo $id ?>&capitulo=<?php echo $arrayCap[$actual + 1]; ?>&pagina=<?php echo $arrayPag[$actual + 1] ?>" class="btn-siguiente<?php if ($cap->row[$contador_ult]['id'] == $cap1->row[0]['id']) { ?> disabled0<?php } ?>"><i class="fa fa-step-forward"></i>&nbsp;&nbsp;&nbsp;SIGUIENTE BLOQUE</a>

                                <?php
                                    }
                                }  ?>
                            <?php }  ?>
                        <?php

                        }  ?>
                    </div>
                </div>

                <br>


            </div>




        </div>
        <div class="col-12 col-lg-2 text-center botons">

            <p class="small">Descarga TeorÍa</p>
            <a href="pdf/teoria_<?php echo $pagina ?>.pdf" class="btn-siguiente"><i class="fa fa-download"></i>&nbsp;&nbsp; DESCARGAR</a>
            <hr>
                <p class="small">Realiza la encuesta de satisfacción del Módulo</p>
                <a href="encuesta.php?modulo=<?php echo $id?>" class="btn-siguiente"><i class="fa fa-list"></i>&nbsp;&nbsp; ENCUESTA</a>
                <hr>
            <!--
                <p class="small">Realiza la encuesta de satisfacción del Módulo</p>
                <a href="encuesta.php?modulo=<?php echo $id ?>" class="btn-siguiente"><i class="fa fa-list"></i>&nbsp;&nbsp; ENCUESTA</a>
                <hr>
                -->
        </div>
        <!-- /#page-content-wrapper -->

    </div>

</div>
<div class="clearfix"></div>
<?php include('footer.php'); ?>