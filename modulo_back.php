<?php 
$page = 'modulo';
include('header.php'); ?>
    
    <div class="modulo">
        <div class="d-flex" id="wrapper">

            <!-- Sidebar -->
            <div class="border-right" id="sidebar-wrapper">
            <div class="sidebar-heading">DEPRESIÓN Y CALIDAD DE VIDA | <b>M1</b></div>
            <div class="list-group list-group-flush">
                <a href="modulo.php" class="list-group-item list-group-item-action visto">TEORÍA<br>Depresión y problemas del sueño</a>
                <a href="modulo.php" class="list-group-item list-group-item-action selected">CASOS CLÍNICOS<br>Depresión y problemas del sueño</a>
                <a href="modulo.php" class="list-group-item list-group-item-action "><b>EXAMEN M1</b><br>Depresión y problemas del sueño</a>
                <a href="modulo.php" class="list-group-item list-group-item-action ">TEORÍA<br>Depresión y temas sexuales</a>
                <a href="modulo.php" class="list-group-item list-group-item-action ">CASOS CLÍNICOS<br>Depresión y temas sexuales</a>
                <a href="modulo.php" class="list-group-item list-group-item-action "><b>EXAMEN M1</b><br>Depresión y temas sexuales</a>
                
            </div>
            </div>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">

            <nav class="navbar navbar-expand-lg navbar-light nopad">
                <div class="container">
                    <button class="btn btn-secondary" id="menu-toggle"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp; Temario</button>
                </div>
                
                
            </nav>

            <div class="container-fluid">
                <div class="container">
                <div class="tema">
                    <div class="row">
                    <div class="col-1 anterior color2">
                        <a href="modulo.php" class="disabled"><i class="fa fa-chevron-left fa-2x"></i></a>
                    </div>
                    <div class="col-10 titulotema text-center">
                    <h1 class="color2">TEORÍA  | <span class="color3"><small>Depresión y problemas del sueño</small></span></h1>
                    </div>

                    <div class="col-1 siguiente color2">
                        <a href="modulo.php"><i class="fa fa-chevron-right fa-2x"></i></a>
                    </div>
                    </div>

                    <hr>
                    <div class="video embed-responsive embed-responsive-16by9 text-center">
                    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/317239882" width="896" height="504" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                    <br>
                    <div class="descargas">
                    <div class="row">
                        <div class="col-12 material text-right">
                            <div class="container">
                                <a href="#" class="btn-reg" ><i class="fa fa-download"></i>&nbsp;&nbsp;Descargar PPT</a>
                            </div>
                        
                        </div>
                    </div>
                    </div>
                    <br>
                    

                </div>
                </div>


            </div>
            </div>
            <!-- /#page-content-wrapper -->

            </div>
        
    </div>
    <div class="clearfix"></div>
<?php include('footer.php'); ?>
    