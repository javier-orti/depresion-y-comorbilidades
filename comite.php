<?php 
$page = 'comite';
include('header.php'); ?>
    <div class="degradado">
        <div class="container">
        <div class="titulo-modulo color2"><h1>COMITÉ CIENTÍFICO</h1></div>
          
            

        </div>
    </div>
    <div class="comite">

    
        <div class="main">
            <div class="container">
                <h5>Coordinador</h5>
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
                    <div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/Celso-Arango.jpg" alt="">
                                </div>
                               
                            </div>
                            <div class="infoponente">
                           
                                
                            <h5 class="color3 font-weight-bold bee">Dr. Celso Arango</h5>
                            <p class="color1 mb-0 small"> Director Instituto de Psiquiatría y Salud Mental.<br>
Jefe Servicio Psiquiatría del Niño y el Adolescente<br>
Hospital General Universitario Gregorio Marañón<br>
Universidad Complutense, Madrid. CIBERSAM<br>
Presidente de la Sociedad Española de Psiquiatría (SEP)</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
</div>

<h5> Autores Módulo 1</h5>
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4">
                    <div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/javier-garcia-campayo.jpg" alt="">
                                </div>
                               
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dr. Javier García Campayo</h5>
                            <p class="color1 mb-0 small">Catedrático Acreditado de Psiquiatría.</p>
                            <p class="color1 mb-0 small">Director Master de Mindfulness. Universidad de Zaragoza</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/menchon.jpg" alt="">
                                </div>
                                
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dr. José Manuel Menchón Magriñá</h5>
                            <p class="color1 mb-0 small">Jefe de Servicio de Psiquiatría. Hospital Universitari Bellvitge, Barcelona.</p>
                            <p class="color1 mb-0 small">Profesor titular de Psiquiatría de la Universidad de Barcelona.</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/grande.jpg" alt="">
                                </div>
                                
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dra. Iria Grande Fullana</h5>
                            <p class="color1 mb-0 small">Profesora asociada de la Universidad de Barcelona.</p>
                            <p class="color1 mb-0 small">Psiquiatra. Unidad de Trastornos Bipolares y Depresivos</p>
                            <p class="color1 mb-0 small">Hospital Clínic. IDIBAPS. CIBERSAM. Barcelona.</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/lara-grau.jpg" alt="">
                                </div>
                                
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dra. Lara Grau López</h5>
                            <p class="color1 mb-0 small">Psiquiatra.</p>
                            <p class="color1 mb-0 small">Jefa de la Sección de Adicciones y Patología Dual.</p>
                            <p class="color1 mb-0 small">Hospital Universitario Vall d’Hebron. Barcelona.</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    
                 
                    
                   
                </div>
				
				<h5> Autores Módulo 2</h5>
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4">
                    <div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/Mod2_Aguera.jpg" alt="">
                                </div>
                               
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dr. Luis Fernando Agüera</h5>
                            <p class="color1 mb-0 small">Servicio de Psiquiatría. Hospital Universitario 12 de Octubre</p>
                            <p class="color1 mb-0 small">Profesor Asociado de Psiquiatría. Universidad Complutense</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
					
					
					<div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/Mod2_Pagonabarraga.jpg" alt="">
                                </div>
                               
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dr. Javier Pagonabarraga</h5>
                            <p class="color1 mb-0 small">Unidad de Trastornos del Movimiento.</p>
                            <p class="color1 mb-0 small">Hospital de la Santa Creu i Sant Pau. Barcelona</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
					
					
					<div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/Mod2_Mico.jpg" alt="">
                                </div>
                               
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dr. Juan Antonio Micó</h5>
                            <p class="color1 mb-0 small">Departamento de Neurociencias (Farmacología y Psiquiatría)</p>
                            <p class="color1 mb-0 small">Facultad de Medicina, Universidad de Cádiz</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
					
					
					<div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/Mod2_Olivares.jpg" alt="">
                                </div>
                               
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dr. José Manuel Olivares</h5>
                            <p class="color1 mb-0 small">Jefe del Servicio de Psiquiatría. Hospital Álvaro Cunqueiro. EOXI Vigo</p>
                            <p class="color1 mb-0 small">Responsable del Grupo de Investigación en Neurociencia Traslacional. IISGS. CIBERSAM</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                 
                    
                   
                    
                 
                    
                   
                </div>


                <h5> Autores Módulo 3</h5>
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4">
                    <div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/Mod3_Olivera.jpg" alt="">
                                </div>
                               
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dr. Javier Olivera Pueyo</h5>
                            <p class="color1 mb-0 small">Servicio de Psiquiatría. Hospital Universitario San Jorge. Huesca</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
					
					
					<div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/Mod3_Diaz.jpg" alt="">
                                </div>
                               
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dra. Marina Díaz Marsá</h5>
                            <p class="color1 mb-0 small">Servicio de Psiquiatría. Hospital Clínico San Carlos. Universidad Complutense. Madrid</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
					
					
					<div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/Mod3_Romero.jpg" alt="">
                                </div>
                               
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dra. Soledad Romero Cela</h5>
                            <p class="color1 mb-0 small">Servicio de Psiquiatría y Psicología Infantil y Juvenil.</p>
                            <p class="color1 mb-0 small">Instituto de Neurociencias. Hospital Clínic Barcelona. CIBERSAM</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
					
					
					<div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/Mod3_Montejo.jpg" alt="">
                                </div>
                               
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dr. Ángel Luis Montejo González</h5>
                            <p class="color1 mb-0 small">Profesor Titular de Psiquiatría de la Universidad de Salamanca</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                 
                    
                   
                    
                 
                    
                   
                </div>


                <h5> Autores Módulo 4</h5>
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4">
                    <div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/Mod4_Perez.jpg" alt="">
                                </div>
                               
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dr. Víctor Pérez Sola</h5>
                            <p class="color1 mb-0 small">Servicio de Psiquiatría. Hospital del Mar. Barcelona.</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
					
					
					<div class="col comiteinfo">
                        <div class="card">
                            <div class="row">
                                <div class="col-12 foto">
                                    <img src="comite/Mod4_Moreno.jpg" alt="">
                                </div>
                               
                            </div>
                            <div class="infoponente">
                                
                            <h5 class="color3 font-weight-bold bee">Dra. Carmen Moreno Ruiz</h5>
                            <p class="color1 mb-0 small">Servicio de Psiquiatría del Niño y el Adolescente. Hospital General Universitario Gregorio Marañón. Madrid.</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
					
					
					
					
				              
                    
                   
                    
                 
                    
                   
                </div>
				
				
				
            </div>
            
        </div>
    </div>
<?php include('footer.php'); ?>
    