<?php
$page = 'login';
$tipologin = 'off';
include('header.php'); ?>
<div class="main">
    <div class="container">
        <div class="login">
            <h2 class="color2">INICIAR SESIÓN</h2>
            <div class="card">

                <?php if ($act == 'OK') { ?>
                    <div class="alert alert-success">Se ha enviado un email a su correo electrónico, por favor siga las instrucciones para recuperar su clave.</div>
                <?php } else  if ($err == '1') { ?>
                    <div class="alert alert-danger">El email introducido no está registrado en nuestra base de datos.</div>
                <?php } ?>



                <p>Introduzca su correo electrónico para recuperar su clave de acceso.</p>
                <br>
                <?php
                $db = Db::getInstance();
                $sql = "SELECT * FROM com_passrecover WHERE clave = :clave AND usuario = :usuario LIMIT 1";
                $bind = array(
                    ':clave' => $unique,
                    ':usuario' => $id
                );

                $cont = $db->run($sql, $bind);


                if ($cont > 0) {
                    $db1 = Db::getInstance();
                    $rowff1 = $db1->fetchAll($sql, $bind);
                    if ($rowff1[0]['estado'] == 0) {
                ?>
                        <form action="recover_pass1.php" method="post">
                            <input type="hidden" name="unique" value="<?php echo $unique; ?>">
                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                            <p><b>Nuevo Password</b></p>
                            <input type="password" class="form-control" name="usu_newpwd" value="">
                            <br>

                            <p><b>Repita Nuevo Password</b></p>
                            <input type="password" class="form-control" name="usu_password2" value="">
                            <br>

                            <div class="text-center">
                                <button type="submit" class="btn-login">ENVIAR</button>
                            </div>
                        </form>

                    <?php } else { ?>
                        <div class="alert alert-danger" La url de recuperación de contraseña ya fue utilizada</div> <?php }  ?> <?php } else { ?> <div class="alert alert-danger" La url de recuperación de contraseña es inválida</div> <?php }  ?> </div> <br>
                            <div class="text-center">
                                <a href="login.php" class="btn-reg">LOGIN</a> - <a href="registro.php" class="btn-reg">CREAR UNA CUENTA</a>
                            </div>
                            <br><br>
                        </div>
            </div>

        </div>
        <?php include('footer.php'); ?>