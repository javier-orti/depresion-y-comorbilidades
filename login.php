<?php 
$page = 'login';
$tipologin = 'off';
include('header.php'); ?>
    <div class="main">
        <div class="container">
            <div class="login">
                <h2 class="color2">INICIAR SESIÓN</h2>
                <div class="card">
                   

                    <?php if ($act=='OK' or $act=='rOK' or $res=='OK') { ?>
                            <div class="alert alert-success">Gracias por registrarse, debe confirmar su registro. <br>Revise la bandeja de entrada de su correo electrónico  y haga clic en el enlace que aparece en el email que le hemos enviado.<br>En caso de que no aparezca en su bandeja principal , revise en la carpeta de "Correo no deseado".<br><br></div>
                            <?php } else if ($reg=='OK') { ?>
                            <div class="alert alert-success">Gracias por registrarse, a partir de ahora puede acceder a nuestro servicio introduciendo sus datos de acceso.</div>
                            <?php } else if ($recpass=='OK') { ?>
                            <div class="alert alert-success">Se ha guardado su nueva contraseña, puede acceder a su cuenta.</div>
                            <?php } else  if ($err=='2' or $err=='1') { ?>
                            <div class="alert alert-danger">El correo y la contraseña no coinciden.</div>
                            <?php } else if ($err=='3') { ?>
                            <div class="alert alert-success">Su cuenta ha sido activada previamente, ya puede acceder con su email y contraseña.</div>
                            <?php } else  if ($err=='4') { ?>
                            <div class="alert alert-danger">Debe activar su cuenta antes de acceder usando el email que recibió en su correo electrónico en el momento del registro.</div>
                            <?php } else  if ($err=='6') { ?>
                            <div class="alert alert-danger">El email que está intentado registrar ya está registrado en la base de datos, use su correo y contraseña para acceder o recupere su contraseña si la ha olvidado.</div>
                            <?php } else  if ($err=='7') { ?>
                            <div class="alert alert-danger">Su identidad no ha sido verificada aún.<br> Revise la bandeja de entrada de su correo electrónico y haga clic en el enlace que aparece en el email que le hemos enviado.<br>
                                En caso de que no aparezca en su bandeja principal, revise en la carpeta de "Correo no deseado".<br>
                                Si no recibió el correo de confirmación <a href="confirmacion_email.php?id=<?php echo $id;?>&uniqueid=<?php echo $uniqueid;?>">haga click aqui para volver a enviarlo</a>. <br><br></div>
                            <?php } else  if ($err=='8') { ?>
                            <div class="alert alert-danger">No se ha podido enviar el email. <br><br></div>

                            <?php } ?>
                    <br>
                    <form action="action_login.php" method="post">
                        <p><b>Correo electrónico</b></p>
                        <input type="email" name="usu_email" class="form-control">
                        <br>
                        <p><b>Contraseña</b></p>
                        <input type="password" name="usu_password" class="form-control">
                        <p><a href="forgot.php">¿Olvidó su contraseña?</a></p>
                        <br>
                        <div class="text-center">
                            <button type="submit" class="btn-login">INICIAR SESIÓN</button>
                        </div>
                    </form>
                    
                </div>
                <br>
                <div class="text-center">
                    <a href="registro.php" class="btn-reg">CREAR UNA CUENTA</a>
                </div>
                <br><br>
            </div>
        </div>
        
    </div>
<?php include('footer.php'); ?>
    