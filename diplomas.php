<?php
/*
error_reporting(E_ALL);
ini_set('display_errors', '0'); */
$page = "diplomas";
include('header.php'); 

$mod = new Modulo();
$mod->getAll();

$exam = new Examen();

$scripts = "";
?>
    <div class="modulo">
      <div class="container">
        <div class="text-center">
          <div class="titulo-modulo color2 text-center"><h1>DIPLOMAS</h1></div>
        </div>
      </div>
      <div class="modulos">
        <div class="contenedor">
          <?php foreach ($mod->row as $Elem) {
          if ($Elem['estado'] == 1 or ($Elem['test'] == 1 and $authj->rowff['test'] == 1)) {
            $exam->modulo = $Elem['id'];
            $exam->alumno = $authj->rowff['id'];
            $estado_exam[$Elem['id']] = $exam->getEstado(1);
            if ($exam->checkPlazo() == 1) {
                $plazo_vencido = 1;
              } else {
                $plazo_vencido = 0;
              } ?>
          <div class="encuesta card mb-4">
            <div class="enchead">
              <p class="text-center"><strong><?php echo $Elem['titulo'].": ".$Elem['titulo_diploma'];?></strong></p>
            </div>
            <div class="encbody">
              <p>&nbsp;</p>
              <?php if ($Elem['test'] == 0) { ?>
                <!--
              <p class="alert alert-success">Disponible desde el <?php echo $Elem['periodo'];?></p>-->
              <?php } ?>
            </div>

  <!-- si no ha aprobado -->
  <?php if ($estado_exam[$Elem['id']] == 0) { ?>
    <p class="text-danger"><b>No has iniciado el examen del <?php echo $Elem['titulo']?>.</b></p>
    <?php } ?>
          <?php if ($estado_exam[$Elem['id']] == 1 or $estado_exam[$Elem['id']] == 2 or ($estado_exam[$Elem['id']] == 4 and $exam->aprobado == 0)) { ?>
            <hr>
            <div class="encbody encajado toleft">
              <?php if ($estado_exam[$Elem['id']] == 1) { ?>
              <p class="text-danger"><b>No has finalizado el examen del <?php echo $Elem['titulo']?>.</b></p>
            <?php } else if ($estado_exam[$Elem['id']] == 2) { ?>
              <p class="text-danger"><b>Lamentablemente no has superado el examen del <?php echo $Elem['titulo']?>.</b></p>
              <?php if ($plazo_vencido==1) { ?>
              <p class="text-danger"><b>El plazo para iniciar el segundo intento ha vencido.</b></p>
              <?php } else { ?>
              <p class="text-primary"><b>Dispones de un segundo intento.</b></p>
              <?php } ?>
            <?php } else if ($estado_exam[$Elem['id']] == 4) { ?>
              <p class="text-danger"><b>Lamentablemente no has superado el examen del <?php echo $Elem['titulo']?>.</b></p>
              <p class="text-danger"><b>Lo sentimos, has agotado todos los intentos.</b></p>
              <?php } ?>
            </div>
            
            <?php  if ($estado_exam[$Elem['id']] == 2) { ?>
                <?php if ($plazo_vencido==0) { ?>
                    <div class="encbody encajado toright">
                      <a href="examen_reiniciar.php?modulo=<?php echo $Elem['id']?>&origen=diploma" class="btn-siguiente"><span >REPETIR EXAMEN</span></a>

                      <br><br>
                    </div>
                <?php } ?>
            <?php } ?>



          <?php } else if ($estado_exam[$Elem['id']] == 3) { ?>
  <!-- si ha aprobado -->
            <hr>
            <div class="encbody encajado toleft row ml-1 mr-1">
              <div class="col-sm-12 col-md-8">
                <p class="text-success"><b>Has finalizado el examen del <?php echo $Elem['titulo']?>, debes contestar la encuesta para ver los resultados</b></p>
              </div>
              <div class="col-sm-12 col-md-4 text-right">
                <a href="encuesta.php?modulo=<?php echo $Elem['id'];?>" class="btn-siguiente">HACER ENCUESTA</a>
              </div>
              <div class="clearfix"></div>
            </div>
            
          <?php } else if ($estado_exam[$Elem['id']] == 4 && $exam->aprobado == 1) {

            ?>
  <!-- si ha aprobado -->
            <hr>
            <div class="encbody encajado toleft row ml-1 mr-1">
              <div class="col-sm-12 col-md-8">
                <p class="text-success"><b>¡Enhorabuena, has superado el examen!</b></p>
              </div>
              <div class="col-sm-12 col-md-4 text-right">
                  <?php if ($Elem['acreditado'] == 1) { ?>
                    <a href="descargar_diploma.php?exam=<?php echo $exam->id;?>&id=<?php echo $Elem['id']?>" class="btn-login">Descargar Diploma</a>
                  <?php } else { ?>
                  Examen pendiente de acreditación.<br>Te avisaremos cuando esté disponible la descarga del diploma.
                  <?php } ?>
              </div>
              <div class="clearfix"></div>
            </div>
            
          <?php }
          if ($estado_exam[$Elem['id']] == 4) {
              $exam->getPreg(1);?>
            <div class="encbody encajado toleft">
              <div class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="panel-title">
                      <a href="#resultados<?php echo $Elem['id'];?>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample" class="alert alert-secondary" style="display:block;">
                        <p class="mb-0"><b>VER RESULTADOS DEL EXAMEN</b>&nbsp;&nbsp;<span class="granate"><i class="fa fa-chevron-right"></i></span></p>
                      </a>
                    </div>
                  </div>
                  <div id="resultados<?php echo $Elem['id'];?>" class="collapse">
                    <div class="panel-body" style="font-size:0.7em">
                       
                    
                              
                                    

                                    <?php
                                    $lasresp = array();
                                    //print_r($exam->preg);
                                    $numPreg = 1;
                                    foreach ($exam->preg as $Elem) {
                                        $lasresp[] = "resp_" . $Elem['id']; ?>
                                        <div class="pregunta">
                                            <div class="enunciado">
                                                <h4 class="color3"><b><?php echo $numPreg.".- ".strip_tags($Elem['pregunta']) ?>
                                                <?php if ($Elem['alum_correc'] == 1) { 
                                                                           echo "<i class=\"fa fa-check-circle\" style=\"font-size:20px; color: #32a909\" aria-hidden=\"true\"></i>";
                                                                       } else { 
                                                                        echo "<i class=\"fa fa-times-circle\" style=\"font-size:20px; color: #ff0000\" aria-hidden=\"true\"></i>";
                                                                    } ?>
                                            </b></h4>                                                
                                            </div>
                                            <div style="color:#FF0000" id="error_resp_<?php echo $Elem['id']; ?>" class="error_resp_<?php echo $Elem['id']; ?> rojo"></div>

                                            <?php
                                            //echo "respuestas: ".$Elem['alumn_resp']." - ".$Elem['alum_correc'];

                                            $numresp = 1;
                                            foreach ($Elem['respuestas'] as $key => $value) {
                                                $respuestas = explode(". ", $value);

                                            ?>
                                                <div class="respuesta" <?php if ($Elem['alum_correc'] == 0 and $key == $Elem['alumn_resp']) { ?>style="background:#f7b0b0"<?php } ?>>
                                                    <input type="radio" id="resp_<?php echo $Elem['id'] . "_" . $key; ?>" name="resp_<?php echo $Elem['id']; ?>" value="<?php echo $key ?>" class="css-checkbox" <?php if ($Elem['resp_corr'][$key] == 1) { ?> checked<?php } ?> disabled>
                                                    <label for="resp_<?php echo $Elem['id'] . "_" . $key; ?>" class="mb-0 css-label<?php if ($key == $Elem['alumn_resp'] and $Elem['alum_correc'] == 0) {  ?> rojo <?php } ?>"><?php echo $value; ?><?php if ($key == $Elem['alumn_resp']) {  ?> <strong>(Tu respuesta)</strong> <?php } ?><?php if ($Elem['resp_corr'][$key] == 1) { ?> <strong>(Respuesta Correcta)</strong><?php } ?></label>
                                                </div>
                                            <?php $numresp++;
                                            } ?>

                                        </div>

                                    <?php $numPreg++; } ?>

                                    


                                
                    </div>
                  </div>
                </div>
                  <div class="clearfix"></div>

              </div>

            </div>
            
            <?php } ?>




          <div class="clearfix"></div>
        </div>
      <?php }
      } ?>
      </div>
    </div>
<?php include('footer.php');?>
