<?php 
$page = 'modulo';
include('header.php'); ?>
    
    <div class="modulo">
        <div class="d-flex" id="wrapper">

            <!-- Sidebar -->
            <div class="border-right" id="sidebar-wrapper">
            <div class="sidebar-heading">DEPRESIÓN Y CALIDAD DE VIDA | <b>M1</b></div>
            <div class="list-group list-group-flush">
                <a href="modulo.php" class="list-group-item list-group-item-action visto">TEORÍA<br>Depresión y problemas del sueño</a>
                <a href="modulo.php" class="list-group-item list-group-item-action visto">CASOS CLÍNICOS<br>Depresión y problemas del sueño</a>
                <a href="modulo.php" class="list-group-item list-group-item-action visto"><b>EXAMEN M1</b><br>Depresión y problemas del sueño</a>
                <a href="modulo.php" class="list-group-item list-group-item-action visto">TEORÍA<br>Depresión y temas sexuales</a>
                <a href="modulo.php" class="list-group-item list-group-item-action visto">CASOS CLÍNICOS<br>Depresión y temas sexuales</a>
                <a href="modulo.php" class="list-group-item list-group-item-action visto"><b>EXAMEN M1</b><br>Depresión y temas sexuales</a>
                
            </div>
            </div>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">

            <nav class="navbar navbar-expand-lg navbar-light nopad">
                <div class="container">
                    <button class="btn btn-secondary" id="menu-toggle"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp; Temario</button>
                </div>
                
                
            </nav>

            <div class="container-fluid">
                <div class="container">
                    <div class="tema text-center color1">
                        <img src="img/enhora.png" width="100">
                        <br><br>
                        <h2><b>¡ENHORABUENA!</b></h2>
                        <br>
                        <p>Has finalizado con éxito el <b>módulo 1</b> del temario <b>“Depresión y calidad de vida”</b>.</p>
                        <p>Ya puedes seguir con tu formación empezando el <b>MÓDULO 2</b> del temario <b>“Depresión en poblaciones especiales”</b>.</p>
                        

                    </div>
                </div>


            </div>
            </div>
            <!-- /#page-content-wrapper -->

            </div>
        
    </div>
    <div class="clearfix"></div>
<?php include('footer.php'); ?>
    