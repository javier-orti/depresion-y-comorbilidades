<?php
//ini_set('display_errors', '1');
$page = 'modulo';
include('header.php');

$id = $modulo;
$mod = new Modulo();
$mod->getOne($id);

$cap = new Capitulo();

if (!empty($orden)) {
    $cap->orden = $orden;
}

if (!empty($tiporden)) {
    $cap->tiporden = $tiporden;
}

if (!empty($pagi)) {
    $cap->pag = $pagi;
}

$cap->modulo = $id;

$cap->alumno = $authj->rowff['id'];
$cap->modulo = $id;
$cap->getAll();



if (empty($capitulo)) {
    $capitulo = $cap->row[0]['id'];
}


$pag0 = new Pagina();
$pag0->alumno = $authj->rowff['id'];

$cap1 = new Capitulo();
$cap1->getOne($capitulo);

$cap1->alumno = $authj->rowff['id'];
$cap1->registrarAcceso();

//echo "capitulo: ".$capitulo;

$pag = new Pagina();
$pag->alumno = $authj->rowff['id'];
$pag->capitulo = $capitulo;
$pag->getAll($capitulo);




if (empty($pagina)) {
    $pagina = $pag->row[0]['id'];
}

//echo "pagina";
$pag2 = new Pagina();
$pag2->alumno = $authj->rowff['id'];
$pag2->capitulo = $capitulo;
$pag2->getOne($pagina);

//print_r($pag2->row);


/*$diap = new Diapositiva();
    $diap->getAll($pag->row[0]['id']);*/

$exam = new Examen();
$exam->modulo = $id;
$exam->capitulo = $capitulo;
$exam->pagina = $pagina;
$exam->alumno = $authj->rowff['id'];

$cap2 = new Capitulo();
$cap2->getOne($exam->capitulo);

// revisamos si vencio el plazo

if ($exam->checkPlazo() == 1) {
    $plazo_vencido = 1;
} else {
    $plazo_vencido = 0;
    //verificamos en que estado está el examen
    $estado_exam = $exam->getEstado();

    //echo  $estado_exam." - ".$exam->id;

    if ($estado_exam == 1) {
        $exam->getPreg();
    } else if ($estado_exam == 2) {
        // mostrar pantalla de reiniciar examen
    } else if ($estado_exam == 3) {
        // mostrar encuesta
    } else if ($estado_exam == 4) {
        // mostrar resultados
    }
}

$encuesta = Modulo::getEncuestaUser($modulo, $authj->rowff['id']);

$scripts = "none";
?>

<div class="modulo">


    <div class="titulo-modulo color2 text-center">
        <?php echo $mod->row[0]['titulo'] ?> | <?php echo $mod->row[0]['titulo_diploma'] ?>
    </div>
    <div class="row row-cols-1 row-cols-lg-2">
        <div class="col-12 col-lg-3 menubar">
            <div class="temario">
                <div class="temas">

                    <?php
                    $contador = 0;
                    foreach ($cap->row as $Elem) {
                        $pag0->capitulo = $Elem['id'];
                        $pag0->getAll($Elem['id']);
                        $cot_exa = 0;
                        $cot_real = 0; ?>
                        <a class="collapsetema<?php if ($Elem['id'] == $capitulo) { echo " opened"; } ?>" data-toggle="collapse" data-target="#collapse<?php echo $contador; ?>" aria-expanded="false" aria-controls="collapseExample">
                            <div class="tema-temario">
                                <p><?php echo $Elem['caso'] ?> | <?php echo $Elem['titulo'] ?></p><i class="chevron fa fa-chevron-down"></i>

                            </div>

                        </a>
                        <div class="collapse<?php if ($Elem['id'] == $capitulo) { echo " show"; } ?>" id="collapse<?php echo $contador; ?>">
                            <?php
                            foreach ($pag0->row as $Elem2) {
                                $cot_exa++;

                                $verificar = Pagina::verificarAcceso($authj->rowff['id'], $Elem2['id'],1);
                                //echo "aqui ".$verificar;
                                if ($verificar==1) {
                                    $claseextra = " visto";
                                } else {
                                    $claseextra = "";
                                    if ($capitulo == $Elem['id']) {
                                        $accedeExamen = 0;
                                    }
                                }
            
                                $verificar2 = Pagina::verificarAcceso($authj->rowff['id'], $Elem2['id'],2);
                                //echo "aqui ".$verificar;
                                if ($verificar2==1) {
                                    $claseextra2 = " visto";
                                } else {
                                    $claseextra2 = "";
                                    if ($capitulo == $Elem['id']) {
                                        $accedeExamen = 0;
                                    }
                                }

                                $arrayCont[$contaArray] = "";
                                $arrayPag[$contaArray] = $Elem2['id'];
                                $arrayCap[$contaArray] = $Elem['id'];
                                $arrayTip[$contaArray] = "cont";
    
                            
                            
                            $contaArray++;

                            ?>

<a href="modulo.php?id=<?php echo $mod->row[0]['id'] ?>&capitulo=<?php echo $Elem['id'] ?>&pagina=<?php echo $Elem2['id'] ?>" class="list-group-item list-group-item-action<?php echo $claseextra;?>"><span class="ocult">APARTADO<br><small><?php echo $Elem2['titulo'] ?></small></span></a>
                                <a href="modulo-caso.php?id=<?php echo $mod->row[0]['id'] ?>&capitulo=<?php echo $Elem['id'] ?>&pagina=<?php echo $Elem2['id'] ?>" class="list-group-item list-group-item-action<?php echo $claseextra2;?>"><span class="ocult"><?php echo $Elem2['subtitulo'] ?>| <?php echo $Elem2['titulo'] ?></span></a>
                                <?php if ($Elem2['examen'] == 1) { ?>
                                    <a class="exam-menu" href="examen.php"><i class="fa fa-graduation-cap"></i> Examen del bloque </a>
                                <?php } ?>
                            <?php


                                //echo $mod->row[0]['id']." - ".$Elem2['id']."<br>";
                                $examen = Examen::estadoExamenPag($mod->row[0]['id'], $Elem2['id'], $authj->rowff['id']);
                                if ($examen == 1) {
                                    $cot_real++;
                                }
                                $pag2->registrarAcceso();
                            } 
                            
                            $arrayCont[$contaArray] = "";
                            $arrayPag[$contaArray] = $Elem2['id'];
                            $arrayCap[$contaArray] = $Elem['id'];
                            $arrayTip[$contaArray] = "exam";

                            if ($capitulo == $Elem['id'] && $pagina == $Elem2['id']) {
                                $actual = $contaArray;
                            }
                    $contaArray++;
                    
                    ?>
                            <a href="cexamen.php?id=<?php echo $mod->row[0]['id'] ?>&capitulo=<?php echo $Elem['id'] ?>&pagina=<?php echo $Elem2['id'] ?>" class="exam-menu"><i class="fa fa-graduation-cap"></i>&nbsp;<b>EXAMEN <?php echo $Elem['caso'] ?></b></a>

                        </div>
                    <?php
                        // echo $Elem['id'] ."==". $cap1->row[0]['id']." contador".$contador;
                        if ($Elem['id'] == $cap1->row[0]['id']) {
                            $contador_ant = $contador - 1;
                        }
                        if ($Elem['id'] == $cap1->row[0]['id']) {
                            $contador_sig = $contador + 1;
                        }
                        $contador++;
                        $contador_ult = $contador - 1;
                    }
                    $cap1->registrarAcceso();
                    ?>
                    <!-- <a href="modulovideo.php" class="list-group-item list-group-item-action"><b></b><br><span class="ocult"></span></a>
                    <a href="modulo.php" class="list-group-item list-group-item-action "><b>1.3</b><br><span class="ocult">Epilepsias focales</span></a>
                    <a href="modulo.php" class="list-group-item list-group-item-action "><b>1.4</b><br><span class="ocult">Síndromes epilépticos</span></a>
                    <a href="modulo.php" class="list-group-item list-group-item-action "><b>1.5</b><br><span class="ocult">Diagnóstico diferencial: episodios no epilépticos</span></a>
                    <a href="modulo.php" class="list-group-item list-group-item-action "><b>1.6</b><br><span class="ocult">Status epilepticus</span></a>
                    
                    -->

                </div>

            </div>

        </div>
        <!-- Page Content -->
        <div class="col-12 col-lg-6 repro">





            <div class="tema">

                <!-- aqui empieza el examen -->
                <div class="encbody encajado toleft ml-1 mr-1">
                <?php if ($encuesta == 0) { ?>
              <form action="enviar_encuesta.php" method="post" class="preguntas">
                <input type="hidden" name="modulo" value="<?php echo $modulo; ?>">
                <p class="text-primary text-center"><b>Valora del 1 al 5, siendo 5 la calificación más alta y 1 la más baja.</b></p>
                <br>
                <p class="alert alert-success"><b>¿Crees que el curso ha sido útil para ampliar tus conocimientos?</b></p>
                <input type="radio" class="css-checkbox" name="ep1" value="1" id="ep11" required><label for="ep11" class="css-label css-label2">1</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep1" value="2" id="ep12"><label class="css-label css-label2" for="ep12">2</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep1" value="3" id="ep13"><label class="css-label css-label2" for="ep13">3</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep1" value="4" id="ep14"><label class="css-label css-label2" for="ep14">4</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep1" value="5" id="ep15"><label class="css-label css-label2" for="ep15">5</label>
                <br>
                <p class="alert alert-success"><b>¿Crees que los contenidos presentados en los vídeos son de interés para la práctica clínica?</b></p>
                <input type="radio" class="css-checkbox" name="ep2" value="1" id="ep21" required><label for="ep21" class="css-label css-label2">1</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep2" value="2" id="ep22"><label class="css-label css-label2" for="ep22">2</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep2" value="3" id="ep23"><label class="css-label css-label2" for="ep23">3</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep2" value="4" id="ep24"><label class="css-label css-label2" for="ep24">4</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep2" value="5" id="ep25"><label class="css-label css-label2" for="ep25">5</label>
                <br>
                <p class="alert alert-success"><b>¿Crees que el formato interactivo del curso es de utilidad para mejorar la comprensión de los contenidos?</b></p>
                <input type="radio" class="css-checkbox" name="ep3" value="1" id="ep31" required><label for="ep31" class="css-label css-label2">1</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep3" value="2" id="ep32"><label class="css-label css-label2" for="ep32">2</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep3" value="3" id="ep33"><label class="css-label css-label2" for="ep33">3</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep3" value="4" id="ep34"><label class="css-label css-label2" for="ep34">4</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep3" value="5" id="ep35"><label class="css-label css-label2" for="ep35">5</label>
                <br>
                <p class="alert alert-success"><b>¿Crees que las preguntas del examen eran suficientemente claras?</b></p>
                <input type="radio" class="css-checkbox" name="ep4" value="1" id="ep41" required><label for="ep41" class="css-label css-label2">1</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep4" value="2" id="ep42"><label class="css-label css-label2" for="ep42">2</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep4" value="3" id="ep43"><label class="css-label css-label2" for="ep43">3</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep4" value="4" id="ep44"><label class="css-label css-label2" for="ep44">4</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep4" value="5" id="ep45"><label class="css-label css-label2" for="ep45">5</label>
                <br>
                <p class="alert alert-success"><b>¿Recomendarías este curso a otros profesionales sanitarios?</b></p>
                <input type="radio" class="css-checkbox" name="ep5" value="1" id="ep51" required><label for="ep51" class="css-label css-label2">1</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep5" value="2" id="ep52"><label class="css-label css-label2" for="ep52">2</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep5" value="3" id="ep53"><label class="css-label css-label2" for="ep53">3</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep5" value="4" id="ep54"><label class="css-label css-label2" for="ep54">4</label>&nbsp;&nbsp;&nbsp;|
                <input class="css-checkbox" type="radio" name="ep5" value="5" id="ep55"><label class="css-label css-label2" for="ep55">5</label>
                <br>
                <p class="alert alert-success"><b>¿Qué consideras que podríamos mejorar en este curso? </b></p>
                <textarea class="form-control" rows="4" name="ep6" value="" placeholder="Escribe aquí tus sugerencias"></textarea>
                <br>
                <div class="toright">
                  <button type="submit" class="btn-acceder2" name="button">ENVIAR ENCUESTA</button>

                </div>

              </form>
              <?php } else  { ?>

              <p>Ya has contestado la encuesta de este modulo</p>
              <?php } ?>
            </div>

                <!-- termina el examen -->

              

                <br>


            </div>




        </div>
        <div class="col-12 col-lg-3 text-center botons">
        <!--
                <p class="small">Descarga el contenido del curso</p>
                <a href="uploads/modulos/<?php echo $id?>.zip" class="btn btn-primary"><i class="fa fa-download"></i>&nbsp;&nbsp; DESCARGAR</a>
                <hr>
                <p class="small">Realiza la encuesta de satisfacción del Módulo</p>
                <a href="encuesta.php" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp;&nbsp; ENCUESTA</a>
                <hr>
                -->
            </div>
        <!-- /#page-content-wrapper -->

    </div>

</div>
<div class="clearfix"></div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Examen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="alert alert-success">Preguntas guardadas correctamente</div>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php'); ?>