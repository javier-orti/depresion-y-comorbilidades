<?php
$page = 'intro';
$tipologin = 'off';
include('header.php'); ?>
<div class="degradado">
    <div class="container">
        <div class="titulo-modulo color2">
            <h1>INTRODUCCIÓN</h1>
        </div>

        <p><strong>DE&CO: Formación en Depresión y Comorbilidades</strong>, es un programa formativo en el que se valora la depresión como una entidad que puede asociarse con otras patologías, como otros trastornos de la salud mental o enfermedades neurodegenerativas, así como su relación con el dolor y su abordaje en poblaciones especiales.
        </p>
        <p>
            El programa formativo se presenta en formato exclusivamente on-line y está estructurado en 4 módulos, cada uno de ellos con unidades didácticas desarrolladas por expertos de referencia en cada temática.
        </p>
        <p>
            La formación impartida ofrece una visión global de la depresión y permite actualizar conocimientos para reforzar la atención y el manejo de los pacientes en la práctica clínica.
        </p>
        <p>
            El programa cuenta con el aval científico de la Sociedad Española de Psiquiatría y de la Sociedad Española de Psiquiatría Biológica.
        </p>
        <ul>
            <li> Módulo 1 | Depresión y enfermedades psiquiátricas (1,2 créditos) | Del 29/03/2021 al 28/03/2022)</li>
            <li>Módulo 2 | Depresión y enfermedades neurodegenerativas (1,2 créditos) | Del 07/06/2021 al 06/06/2022</li>
            <li>Módulo 3 | Depresión en poblaciones especiales y en pacientes con disfunción sexual (1,1 créditos) | Del 27/09/2021 al 26/09/2022</li>
            <li>Módulo 4 | Salud Mental en tiempos de COVID (Solicitada acreditación) | Del 31/01/2022 al 30/01/2023</li>
        </ul>


    </div>
</div>
<div class="main">

    <div class="container">


        <div class="video embed-responsive embed-responsive-16by9">

            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/514306832" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

        </div>
        <?php if ($authj->logueado != 1) { ?>
            <div class="acceder">
                <a href="login.php" class="btn-acceder">ACCEDER<br>AL CURSO</a>
            </div>
        <?php } ?>
    </div>

    <small>Por motivos de la Alerta Sanitaria por el COVID-19 la grabación de los contenidos formativos por parte de los autores se ha realizado a través de sistemas de streaming, anteponiendo la seguridad de los profesionales sanitarios que participan en este curso a la calidad de imagen, sin afectar a los objetivos didácticos del mismo.<br>
        La estructura del curso puede haber sufrido modificaciones debido a la Alerta Sanitaria, sin afectar a los contenidos formativos.
    </small>

</div>
<?php include('footer.php'); ?>