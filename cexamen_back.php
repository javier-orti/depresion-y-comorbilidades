<?php
//ini_set('display_errors', '1');
$page = 'modulo';
include('header.php');
$mod = new Modulo();
$mod->getOne($id);
$mod->alumno = $authj->rowff['id'];

$cap = new Capitulo();

if (!empty($orden)) {
    $cap->orden = $orden;
}

if (!empty($tiporden)) {
    $cap->tiporden = $tiporden;
}

if (!empty($pagi)) {
    $cap->pag = $pagi;
}

$cap->modulo = $id;

$cap->alumno = $authj->rowff['id'];
$cap->modulo = $id;
$cap->getAll();



if (empty($capitulo)) {
    $capitulo = $cap->row[0]['id'];
}


$pag0 = new Pagina();
$pag0->alumno = $authj->rowff['id'];

$cap1 = new Capitulo();
$cap1->getOne($capitulo);

$cap1->alumno = $authj->rowff['id'];
$cap1->registrarAcceso();

//echo "capitulo: ".$capitulo;

$pag = new Pagina();
$pag->alumno = $authj->rowff['id'];
$pag->capitulo = $capitulo;
$pag->getAll($capitulo);




if (empty($pagina)) {
    $pagina = $pag->row[0]['id'];
}

//echo "pagina";
$pag2 = new Pagina();
$pag2->alumno = $authj->rowff['id'];
$pag2->capitulo = $capitulo;
$pag2->getOne($pagina);

//print_r($pag2->row);


/*$diap = new Diapositiva();
    $diap->getAll($pag->row[0]['id']);*/

$exam = new Examen();
$exam->modulo = $id;
$exam->capitulo = $capitulo;
$exam->pagina = $pagina;
$exam->alumno = $authj->rowff['id'];

$cap2 = new Capitulo();
$cap2->getOne($exam->capitulo);

// revisamos si vencio el plazo

if ($exam->checkPlazo() == 1) {
    $plazo_vencido = 1;
	$estado_exam = $exam->getEstado();
} else {
    $plazo_vencido = 0;
    //verificamos en que estado está el examen
    $estado_exam = $exam->getEstado();

    

    if ($estado_exam == 1) {
        $exam->getPreg();
    } else if ($estado_exam == 2) {
        // mostrar pantalla de reiniciar examen
    } else if ($estado_exam == 3) {
        // mostrar encuesta
    } else if ($estado_exam == 4) {
        // mostrar resultados
    }
}

//echo $plazo_vencido;

$scripts = "none";
?>

<div class="modulo">


    <div class="titulo-modulo color2 text-center">
        <?php echo $mod->row[0]['titulo'] ?> | <?php echo $mod->row[0]['titulo_diploma'] ?>
    </div>
    <div class="row row-cols-1 row-cols-lg-2">
        <div class="col-12 col-lg-3 menubar">
            <div class="temario">
                <div class="temas">

                    <?php
                    $contador = 0;
                    foreach ($cap->row as $Elem) {
                        $pag0->capitulo = $Elem['id'];
                        $pag0->getAll($Elem['id']);
                        $cot_exa = 0;
                        $cot_real = 0; ?>
                        <a class="collapsetema<?php if ($Elem['id'] == $capitulo) { echo " opened"; } ?>" data-toggle="collapse" data-target="#collapse<?php echo $contador; ?>" aria-expanded="false" aria-controls="collapseExample">
                            <div class="tema-temario">
                                <p><?php echo $Elem['caso'] ?> | <?php echo $Elem['titulo'] ?></p><i class="chevron fa fa-chevron-down"></i>

                            </div>

                        </a>
                        <div class="collapse<?php if ($Elem['id'] == $capitulo) { echo " show"; } ?>" id="collapse<?php echo $contador; ?>">
                            <?php
                            if ($capitulo == $Elem['id']) {
                                $accedeExamen = 1;
                            }
                            foreach ($pag0->row as $Elem2) {
                                $cot_exa++;


                                $verificar = Pagina::verificarAcceso($authj->rowff['id'], $Elem2['id'],1);
                                //echo "aqui ".$verificar;
                                if ($verificar==1) {
                                    $claseextra = " visto";
                                } else {
                                    $claseextra = "";
                                    if ($capitulo == $Elem['id']) {
                                        $accedeExamen = 0;
                                    }
                                }
            
                                $verificar2 = Pagina::verificarAcceso($authj->rowff['id'], $Elem2['id'],2);
                                //echo "aqui ".$verificar;
                                if ($verificar2==1) {
                                    $claseextra2 = " visto";
                                } else {
                                    $claseextra2 = "";
                                    if ($capitulo == $Elem['id']) {
                                        $accedeExamen = 0;
                                    }
                                }

                                /*

                                $verificar = Pagina::verificarAcceso($authj->rowff['id'], $Elem2['id']);
                                //echo "aqui ".$verificar;
                                if ($verificar == 1) {
                                    $claseextra = " visto";
                                } else {
                                    $claseextra = "";
                                    if ($capitulo == $Elem['id']) {
                                        $accedeExamen = 0;
                                    }
                                    
                                }
                                */

                                $arrayCont[$contaArray] = "";
                                $arrayPag[$contaArray] = $Elem2['id'];
                                $arrayCap[$contaArray] = $Elem['id'];
                                $arrayTip[$contaArray] = "cont";
    
                            
                            
                            $contaArray++;

                            ?>

                                <!--
                                <span class="list-group-item list-group-item-action<?php echo $claseextra;
                                                                                    if ($Elem2['id'] == $pag2->row[0]['id']) { ?> selected<?php } ?>"><span class="ocult"><?php echo $Elem2['subtitulo'] ?>| <?php echo $Elem2['titulo'] ?></span></span>
                                                                                    -->

                                <a href="modulo.php?id=<?php echo $mod->row[0]['id'] ?>&capitulo=<?php echo $Elem['id'] ?>&pagina=<?php echo $Elem2['id'] ?>" class="list-group-item list-group-item-action<?php echo $claseextra;?>"><span class="ocult">APARTADO<br><small><?php echo $Elem2['titulo'] ?></small></span></a>
                                <a href="modulo-caso.php?id=<?php echo $mod->row[0]['id'] ?>&capitulo=<?php echo $Elem['id'] ?>&pagina=<?php echo $Elem2['id'] ?>" class="list-group-item list-group-item-action<?php echo $claseextra2;?>"><span class="ocult"><?php echo $Elem2['subtitulo'] ?>| <?php echo $Elem2['titulo'] ?></span></a>
                                <?php if ($Elem2['examen'] == 1) { ?>
                                    <a class="exam-menu" href="examen.php"><i class="fa fa-graduation-cap"></i> Examen del bloque </a>
                                <?php } ?>
                            <?php


                                //echo $mod->row[0]['id']." - ".$Elem2['id']."<br>";
                                $examen = Examen::estadoExamenPag($mod->row[0]['id'], $Elem2['id'], $authj->rowff['id']);
                                if ($examen == 1) {
                                    $cot_real++;
                                }
                                $pag2->registrarAcceso();
                            } 
                            
                            $arrayCont[$contaArray] = "";
                            $arrayPag[$contaArray] = $Elem2['id'];
                            $arrayCap[$contaArray] = $Elem['id'];
                            $arrayTip[$contaArray] = "exam";


                            $estExamCap = Examen::estadoExamenCap($mod->row[0]['id'], $Elem['id'], $authj->rowff['id']);

                            if ($capitulo == $Elem['id'] && $pagina == $Elem2['id']) {
                                $actual = $contaArray;
                                if ($estExamCap == 1) {
                                    $examen_hecho = 1;

                                } 
                            }
                    $contaArray++;

                    
                    
                    ?>
                            <a href="cexamen.php?id=<?php echo $mod->row[0]['id'] ?>&capitulo=<?php echo $Elem['id'] ?>&pagina=<?php echo $Elem2['id'] ?>" class="exam-menu<?php if ($estExamCap == 1) {  ?> visto<?php } ?>"><i class="fa fa-graduation-cap"></i>&nbsp;<b>EXAMEN <?php echo $Elem['caso'] ?></b></a>

                        </div>
                    <?php
                        // echo $Elem['id'] ."==". $cap1->row[0]['id']." contador".$contador;
                        if ($Elem['id'] == $cap1->row[0]['id']) {
                            $contador_ant = $contador - 1;
                        }
                        if ($Elem['id'] == $cap1->row[0]['id']) {
                            $contador_sig = $contador + 1;
                        }
                        $contador++;
                        $contador_ult = $contador - 1;
                    }
                    $cap1->registrarAcceso();
                    ?>
                    <!-- <a href="modulovideo.php" class="list-group-item list-group-item-action"><b></b><br><span class="ocult"></span></a>
                    <a href="modulo.php" class="list-group-item list-group-item-action "><b>1.3</b><br><span class="ocult">Epilepsias focales</span></a>
                    <a href="modulo.php" class="list-group-item list-group-item-action "><b>1.4</b><br><span class="ocult">Síndromes epilépticos</span></a>
                    <a href="modulo.php" class="list-group-item list-group-item-action "><b>1.5</b><br><span class="ocult">Diagnóstico diferencial: episodios no epilépticos</span></a>
                    <a href="modulo.php" class="list-group-item list-group-item-action "><b>1.6</b><br><span class="ocult">Status epilepticus</span></a>
                    
                    -->

                </div>

            </div>

        </div>
        <!-- Page Content -->
        <div class="col-12 col-lg-6 repro">





            <div class="tema">

                <!-- aqui empieza el examen -->
                <div class="examen">
                <?php if ($plazo_vencido == 1) { ?>
                            <p class="granate"><b>El plazo para terminar el examen desde el momento en que inició el módulo ha finalizado.</b></p>

                        <?php } ?>

                    
                    <?php
                    if ($estado_exam == 1) { ?>
                        <?php if ($plazo_vencido == 1) { ?>
                           

                        <?php } else if ($accedeExamen == 1)  { ?>


                            <p class="text-center"><b style="font-size:16px;">Dispone de <?php echo $exam->duracion; ?> días para finalizar el examen del módulo</b><br><small>Una vez finalizada la totalidad de los exámenes se indicará el porcentaje de aciertos.<br>Para aprobar se deberá obtener al menos un <b>70% de respuestas correctas</b>.<br>
                                En caso de no obtener la puntuación necesaria se podrá <b>repetir el examen</b> una segunda vez.<br>Está opción estará habilitada hasta la finalización de los 30 días.</small>
                            </p>
                   

                            <hr>
                            <?php
                            if ($estado_exam == 1) { 
                                if ($examen_hecho != 1) { 
                                ?>
                              <form method="post" id="form_exam" action="examen_guardar.php"> 
                              
                                    <input type="hidden" name="modulo" value="<?php echo $id; ?>">
                                    <input type="hidden" name="capitulo" value="<?php echo $capitulo; ?>">
                                    <input type="hidden" name="pagina" value="<?php echo $pagina; ?>">
                                    <input type="hidden" name="id" value="<?php echo $exam->id; ?>">

                                    <?php
                                    $lasresp = array();
                                    //print_r($exam->preg);
                                    $numPreg = 1;
                                    foreach ($exam->preg as $Elem) {
                                        $lasresp[] = "resp_" . $Elem['id']; ?>
                                        <div class="pregunta">
                                            <div class="enunciado">
                                                <h5 class="color3"><b><?php echo $numPreg.".- ".strip_tags($Elem['pregunta']) ?></b></h5>                                                
                                            </div>
                                            <div style="color:#FF0000" id="error_resp_<?php echo $Elem['id']; ?>" class="error_resp_<?php echo $Elem['id']; ?> rojo"></div>

                                            <?php

                                            $numresp = 1;
                                            foreach ($Elem['respuestas'] as $key => $value) {
                                                $respuestas = explode(". ", $value);

                                            ?>
                                                <div class="respuesta">
                                                    <input type="radio" id="resp_<?php echo $Elem['id'] . "_" . $key; ?>" name="resp_<?php echo $Elem['id']; ?>" value="<?php echo $key ?>" class="css-checkbox" <?php if ($key == $Elem['alumn_resp']) { ?> checked<?php } ?>>
                                                    <label for="resp_<?php echo $Elem['id'] . "_" . $key; ?>" class="mb-0 css-label"><?php echo $value; ?></label>
                                                </div>
                                            <?php $numresp++;
                                            } ?>

                                        </div>

                                    <?php $numPreg++;
                                } ?>

                                    <br>
                                    <div class="envioform text-right">
                                    <div id="errores" class="errores rojo"></div>
                                        <button class="btn-reg btn-clr2" type="submit">ENVIAR EXAMEN <i class="fa fa-send"></i> </button>

                                    </div>
                                </form>
                            <?php } else  { ?>
                                <p>Ya has enviado el examen de esta Unidad. Tus respuestas serán guardadas para la evaluación final</p>

                                <?php
                            }
                        }  ?>
                        <?php } else {?>
                            <p>Debes visualizar todos los contenidos del bloque para iniciar el examen</p>
                        <?php } ?>
					<?php } else if ($estado_exam == 2 && $plazo_vencido == 1) {  ?>
                        <div class="video">
                            <div class="clearfix"></div>
                            <br>
                            <p class="granate"><b>Has <span class="roja">SUSPENDIDO</span> el primer intento de realizar el examen.<br>No puedes reiniciar el examen porque el plazo ha vencido</b></p>

                            
                        </div>
						

                    <?php } else if ($estado_exam == 2) {  ?>
                        <div class="video">
                            <div class="clearfix"></div>
                            <br>
                            <p class="granate"><b>Has <span class="roja">SUSPENDIDO</span> el primer intento de realizar el examen.<br>Puedes reiniciar el examen e iniciar un segundo intento. <br><a href="examen_reiniciar.php?modulo=<?php echo $id; ?>&capitulo=<?php echo $capitulo; ?>&pagina=<?php echo $pagina; ?>&id=<?php echo $exam->id; ?>" class="btn btn-success" style="cursor: pointer;">REINICIAR</a></b></p>

                            
                        </div>
                    <?php } else if ($estado_exam == 3) { 
                        // aqui si aun no ha contestado la encuesta
                        ?>
                        <div class="video">
                            <div class="clearfix"></div>
                            <br>
                            <!--<p class="granate"><b>Ya has realizado el examen del módulo. <br><br><a href="encuesta.php?modulo=<?php echo $modulo; ?>" class="btn btn-success" style="cursor: pointer;">Realizar encuesta para ver resultados.</a></b></p>-->
                            <p class="granate text-center"><b>Ya has realizado el examen del módulo. Debes contestar la encuesta para ver tus resultados<br><br><a href="encuesta.php?modulo=<?php echo $id; ?>&capitulo=<?php echo $capitulo; ?>&pagina=<?php echo $pagina; ?>" class="btn-siguiente" style="cursor: pointer;">HACER ENCUESTA</a></b></p>



                        </div>
                    <?php } else if ($estado_exam == 4) {  ?>
                        <div class="video">
                            <div class="clearfix"></div>
                            <br>
                            <p class="granate text-center"><b>Ya has realizado el examen del módulo.<br><br><a href="resultados.php?id=<?php echo $id; ?>&capitulo=<?php echo $capitulo; ?>&pagina=<?php echo $pagina; ?>" class="btn-siguiente" style="cursor: pointer;">VER RESULTADOS</a></b></p>
                        </div>
                    <?php } ?>
                </div>

                <!-- termina el examen -->

                <div class="row dentro">
                <?php  if ($examen_hecho != 1) { 
                                ?>
                    <div class="col-7 titulotema text-left">
                        <!--
                        <h2 class="color2"><?php echo $cap1->row[0]['caso'] ?> | <span class="color3"><small>Examen</small></span></h2>
                -->
                    </div>
                    <?php  }  ?>  
                    <!--
                    <div class="col-5 siguiente color2 text-right">
                        <?php if ($actual != 0) { 
                                    if ($arrayTip[$actual-1] != "exam") { ?>

                           
                            <a class="btn-siguiente" href="modulo.php?id=<?php echo $id?>&capitulo=<?php echo $arrayCap[$actual-1];?>&pagina=<?php echo $arrayPag[$actual-1]?>"><i class="fa fa-step-backward"></i></a>
                            <?php  
                                    }
                                }  ?>    
                                <?php 
                               
                                if ($actual <= ($contaArray)) { 

                                  
                                    if ($arrayTip[$actual+1] == "exam") {
                                         ?>

                            <a href="cexamen.php?id=<?php echo $id?>&capitulo=<?php echo $capitulo;?>&pagina=<?php echo $pagina?>" class="btn-siguiente<?php if ($cap->row[$contador_ult]['id'] == $cap1->row[0]['id'])   { ?> disabled<?php } ?>" ><i class="fa fa-step-forward"></i>&nbsp;&nbsp;&nbsp;SIGUIENTE</a>
                            
                            <?php  } else { ?>
                                <a href="modulo.php?id=<?php echo $id?>&capitulo=<?php echo $arrayCap[$actual+1];?>&pagina=<?php echo $arrayPag[$actual+1]?>" class="btn-siguiente<?php if ($cap->row[$contador_ult]['id'] == $cap1->row[0]['id'])   { ?> disabled<?php } ?>" ><i class="fa fa-step-forward"></i>&nbsp;&nbsp;&nbsp;SIGUIENTE</a>
                            
                            <?php  
                                  }
                                }  ?>
                        </div>
                        -->
                </div>

                <br>


            </div>




        </div>
        <div class="col-12 col-lg-3 text-center botons">

        <!--
                <p class="small">Descarga el contenido del curso</p>
                <a href="uploads/modulos/<?php echo $id?>.zip" class="btn-siguiente"><i class="fa fa-download"></i>&nbsp;&nbsp; DESCARGAR</a>
                <hr>
                <p class="small">Realiza la encuesta de satisfacción del Módulo</p>
                <a href="encuesta.php?modulo=<?php echo $id?>" class="btn-siguiente"><i class="fa fa-list"></i>&nbsp;&nbsp; ENCUESTA</a>
                <hr> -->
            </div>
        <!-- /#page-content-wrapper -->

    </div>

</div>
<div class="clearfix"></div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Examen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="alert alert-success">Preguntas guardadas correctamente</div>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php'); ?>