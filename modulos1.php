<?php 
error_reporting(E_ALL);
ini_set('display_errors', '0'); 

$page = 'index';
include('header.php'); 


foreach ($_GET as $key => $value) {
    //if ($key != 'filtro' && $key != 'adfil') {
    $listvarall .=  $key . "=" . $value . "&";
    //}


    if ($key != 'pagi') {
        $listvar .=  $key . "=" . $value . "&";
    }

    if ($key != 'orden' && $key != 'tiporden' && $key != 'pagi') {
        $listvaro .=  $key . "=" . $value . "&";
    }
}

$mod = new Modulo();

if (!empty($orden)) {
    $mod->orden = $orden;
}

if (!empty($tiporden)) {
    $mod->tiporden = $tiporden;
}


if (!empty($pagi)) {
    $mod->pag = $pagi;
}


$mod->getAll();
$exam = new Examen();

?>
    <div class="main">
        <div class="container">
            <p class="text-center">Tendrás 30 días desde el primer acceso al Módulo para visualizar todos los contenidos y realizar los exámenes.<br>Para superar la evaluación se deberá obtener al menos un <b>70% de respuestas correctas en total</b>.</p>
                <p class="text-center">En caso de no obtener la puntuación necesaria se podrá <b>repetir el examen</b> una segunda vez. <br>Está opción estará habilitada hasta la finalización de los 30 días.</p>
            <div class="modulos">
                
                <div class="row row-cols-1 row-cols-lg-2">
                    <?php
                foreach ($mod->row as $Elem) {
         
            $exam->modulo = $Elem['id'];
            $exam->alumno = $authj->rowff['id'];
            $estado_exam[$Elem['id']] = $exam->getEstado(1);
            if ($exam->checkPlazo() == 1) {
                $plazo_vencido = 1;
              } else {
                $plazo_vencido = 0;
              }

             } ?>
				
                    <div class="col">
                        <?php if ($plazo_vencido == 0 && $estado_exam[1]==0) { ?>
                            <a href="#" data-toggle="modal" data-target="#myModal_news">
                        <?php } else { ?>
                            <a href="modulo.php?id=1">
                        <?php }  ?>


                            <img src="img/mod1E.jpg?v=2"> </a> 
                </div>
					
					<div class="col"><a href="modulo.php?id=2"><img src="img/m2E.png?v=4"> </a> </div>
                    
                    <div class="col"><img src="img/mod3.jpg"> </div>
                    <div class="col"><img src="img/mod4.jpg">  </div>

                </div>
                <br>
                <p>* Módulo 1 acreditado por parte del Consell Català de Formació Continuada de les Professions Sanitàries – Comisión de Formación Continuada del Sistema Nacional de Salud. Período de acreditación de la actividad 09/029581-MD: del 29/03/2021 al 28/03/2022. Acreditada con 1,2 créditos..</p>
				<p>Módulo 2 acreditado por parte del Consell Català de Formació Continuada de les Professions Sanitàries – Comisión de Formación Continuada del Sistema Nacional de Salud. Período de acreditación de la actividad 09/029957-MD: del 07/06/2021 al 06/06/2022. Acreditada con 1,2 créditos.﻿.</p>
			</div>
        </div>
        
    </div>


<div class="modal fade" id="myModal_news" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
    
       	 <h4 class="modal-title" style="text-align: center;">AVISO</h4>
       	 <button type="button" class="close modalcerrar" data-dismiss="modal" aria-hidden="true" style="position: absolute; right:10px; top:10px;">&times;</button>
      
            
            
          </div>
      <div class="modal-body">
        <div class="elcont">
        
         <p class="texto">Si inicias la visualización del módulo se inicia el plazo de 30 dias para finalizar el examen. Deseas continuar al modulo</p>

         <a href="modulo.php?id=1">Entrar</a>

         <a href="#" data-dismiss="modal" aria-hidden="true">No entrar</a>

         


        </div>
      </div>
                                 
      </div>
      
  </div>
</div>
<?php include('footer.php'); ?>
    