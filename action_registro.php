<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once 'lib/autoloader.class.php';
require_once 'lib/init.class.php';
require_once("includes/recaptchalib.php");
require_once 'lib/auth_off.php';

// require_once 'lib/auth.php'; //florian
//include("cursoPDO.php");
/*
function datoApi($valor)
{
	return $valor;
}*/


# Our new data

# Create a connection

if ($action == 'cemail') {
} else if ($action == 'cpass') {
} else if ($action == 'forgot') {

	$db = Db::getInstance();
	$sql = "SELECT * FROM com_alumnos WHERE email = :email LIMIT 1";
	$bind = array(
		':email' => $usu_email
	);

	$cont = $db->run($sql, $bind);

	if ($cont > 0) {

		$db1 = Db::getInstance();
		$rowff1 = $db1->fetchAll($sql, $bind);
		$contador = 0;
		foreach ($rowff1 as $rowff) {
			$idm = $rowff['id'];
			$ape1 = $rowff['ape1'];
			$nombre = $rowff['nombre'];
			$email = $rowff['email'];
		}

		$clave = uniqid();

		$db3 = Null;
		$db3 = Db::getInstance();
		$data3 = array(
			'clave' => $clave,
			'usuario' => $idm,
			'fecha' => date('Y-m-d H:i:s')

		);
		$db3->insert('com_passrecover', $data3);
		$elid = $db3->lastInsertId();

		$nota = "<table width=\"580\" style=\"background-color: #ffffff; margin: 0px auto;\" cellpadding=\"0\" cellspacing=\"0\" border=\"1\" bordercolor=\"#19ABB9\">
        <tr>
         <td valign=\"top\" align=\"center\"><img src=\"" . $app_url . "img/logo.png\" alt=\"" . $apptitle . "\" width=\"166\" /></td>
        </tr>
        <tr>
         <td valign=\"top\" align=\"left\">
             <table width=\"580\" style=\"margin: 0px auto; border-collapse: collapse;\" cellpadding=\"0\" cellspacing=\"0\">
             <tr>
               <td width=\"15\" valign=\"top\" align=\"left\">&nbsp;</td>
               <td width=\"560\" align=\"left\" valign=\"top\"><font size=\"2\" color=\"#000000\" face=\"Arial, sans-serif\"><br /><br />
              Apreciado/a Dr/Dra. " . $nombre . " " . $ape1 . "<br><br>

Hemos recibido tu solicitud de recuperar tu contrase&ntilde;a en nuestros servicios.<br>
Para obtener una nueva contrase&ntilde;a debes hacer clic sobre el siguiente enlace.<br>
<a href=\"" . $app_url . "recover_pass.php?id=" . $idm . "&unique=" . $clave . "\">Modificar clave</a>
<br><br> </font>
                </td>
               <td width=\"15\" valign=\"top\" align=\"left\">&nbsp;</td>
             </tr>

             <tr>
               <td width=\"15\" valign=\"top\" align=\"left\">&nbsp;</td>
               <td width=\"560\" align=\"left\" valign=\"top\"><font size=\"2\" color=\"#000000\" face=\"Arial, sans-serif\"><br /><br />

Si no puedes acceder a trav&eacute;s del enlace de arriba, copia la siguiente direcci&oacute;n:<br><br>
" . $app_url . "recover_pass.php?id=" . $idm . "&unique=" . $clave . "<br><br>

Muchas gracias por tu confianza en " . $apptitle . ".<br><br>

Cordialmente,<br><br>
" . $apptitle . "
<br />&nbsp;<br />&nbsp;<br />&nbsp;<br />
                </font>




                </td>
               <td width=\"15\" valign=\"top\" align=\"left\">&nbsp;</td>
             </tr>

             </table>
         </td>
        </tr>

        </table>";
		require_once('includes/class.phpmailer.php');
		require_once('includes/class.smtp.php');

		$mail = new PHPMailer();

		$mail->IsSMTP();

		$mail->SMTPDebug = 0;
		// 0 = no output, 1 = errors and messages, 2 = messages only.


		/* Sustituye (ServidorDeCorreoSMTP)  por el host de tu servidor de correo SMTP*/
		$mail->Host = $mailhost;
		if (!empty($mailsecure)) {
			$mail->SMTPSecure = $mailsecure;
		}
		if (!empty($mailport)) {
			$mail->Port = $mailport;
		}




		$mail->From = $mailemail;
		$mail->FromName = $apptitle;



		$mail->addAddress($email);
		$mail->AddBCC('gianna@tba.es', 'test');
		//$mail->AddBCC('contacto@cursocen.com', 'contacto');
		$mail->addReplyTo('info@tbhealthcare.es', 'TBHealthcare.es');

		$mail->isHTML(true);
		$mail->SMTPAuth = true;


		$mail->Username = $maillogin;
		$mail->Password = $mailpass;
		$mail->CharSet = 'UTF-8';
		// Set email format to HTML

		$mail->Subject = 'Recuperacion de clave de acceso - '.$apptitle;
		$mail->Body    = $nota;

		if (!$mail->send()) {
			/*$app->flash("error", "We're having trouble with our mail servers at the moment.  Please try again later, or contact us directly by phone.");
        error_log('Mailer Error: ' . $mail->errorMessage());
        $app->halt(202);
		die();*/
		} else {
			header("Location: forgot.php?act=OK");
		}
	} else {
		header("Location: forgot.php?err=1");
	}
}

// MODIFICAR LO DATOS DE USUARIO
else if ($action == 'modificar') {
	// aqui empieza el registro en el servicio

	//$dev_OK = 'registroOK.php';
	$dev_OK = 'misdatos.php?status=OK&tipo=modif';
	$dev_KO = 'misdatos.php?status=KO';



	if ($mailing != 'N') {
		$mailing = "S";
	}
	//print_r($fields);



	$db = Null;

	$db = Db::getInstance();
	$data = array(
		'ape1' => $usu_ape1,
		'ape2' => $usu_ape2,
		'codusuario' => $usu_codusuario,
		'nombre' => $usu_nombre,
		'dni' => $usu_dni,
		'perfil' => $usu_codperfil,
		'especialidad' => $usu_codespecialidad,
		'numcolegiado' => $usu_numcolegiado,
		'pais' => $usu_codpais,
		'provincia' => $usu_codprovestado,
		'poblacion' => $usu_codpoblacion,
		'ciudad' => $usu_ciudad,
		'direccion' => $usu_direccion,
		'cp' => $usu_cp,
		'telefono' => $usu_telefono,
		'fax' => $usu_fax,
		'empresa' => $usu_empresa,
		'fecmod' => $fechoy,
		'mailing' => $mailing
	);
	// print_r($data);
	$db->update('com_alumnos', $data, 'id = :id', array(':id' => $id));
	/*$sqlup = "UPDATE com_alumnos SET ape1 = '". noFiltro($usu_ape1) ."', ape2 = '". noFiltro($usu_ape2) ."', email = '". noFiltro($usu_email) ."', nombre = '". noFiltro($usu_nombre) ."', dni = '". noFiltro($usu_dni) ."', perfil = '".noFiltro($usu_codperfil)."', especialidad = '".noFiltro($usu_codespecialidad)."', numcolegiado = '".noFiltro($usu_numcolegiado)."', pais = '".noFiltro($usu_codpais)."', provincia = '".noFiltro($usu_codprovestado)."', poblacion = '".noFiltro($usu_codpoblacion)."', ciudad = '".noFiltro($usu_ciudad)."', direccion = '".noFiltro($usu_direccion)."', cp = '".noFiltro($usu_cp)."', telefono = '".noFiltro($usu_telefono)."', fax = '".noFiltro($usu_fax)."', empresa = '".noFiltro($usu_empresa)."',  servicio= '1' WHERE codusuario = '".noFiltro($usu_codusuario)."'";
		//echo $sqlup;
				  $resultup = mysql_query ($sqlup,$link) or die ("hay un error en la consulta1 ".mysql_error());*/

	header("Location: " . $dev_OK);

	// aqui termina el registro en el servicio
}
// TERMINA MODIFICAR LOS DATOS DE USUARIO
else if ($action == 'registro') {

	/* //florian
	$response   = isset($_POST["g-recaptcha-response"]) ? $_POST['g-recaptcha-response'] : null;
	$privatekey = "6Ld_QHEaAAAAAIffhjlgpZaeHkQCD7Mk7OrJ3db-";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, array(
		'secret' => $privatekey,
		'response' => $response,
		'remoteip' => $_SERVER['REMOTE_ADDR']
	));

	$resp = json_decode(curl_exec($ch));
	curl_close($ch);

	if ($resp->success) {
	*/	
		// primero revisamos que el email NO exista
		$db = Db::getInstance();
		$sql = "SELECT * FROM com_alumnos WHERE email = :email LIMIT 1";
		$bind = array(
			':email' => $usu_email
		);

		$cont = $db->run($sql, $bind);

		if ($cont == 0) {
			// SI NO EXISTE REGISTRAMOS

			$dev_OK = 'login.php?act=rOK';
			$dev_KO = 'registro.php?err=1';
			// empieza el registro de usuario
			if (empty($mailing)) {
				$mailing = "N";
			}
			if (empty($tbh)) {
				$tbh = 0;
			}
			if (empty($patrocinador1)) {
				$patrocinador1 = 0;
			}
			$pass1 = sha1(md5(trim($usu_password)));

			if (empty($usu_codespecialidad)) {
				$usu_codespecialidad = 0;
			}

			if (empty($usu_ciudad)) {
				$usu_ciudad = 'valor 1';
			}



			$clave00 = uniqid();

			$db = Db::getInstance();
			$data = array(
				'ape1' => $usu_ape1,
				'ape2' => $usu_ape2,
				'codusuario' => $usu_codusuario,
				'email' => $usu_email,
				'nombre' => $usu_nombre,
				'dni' => $usu_dni,
				'perfil' => $usu_codperfil,
				'especialidad' => $usu_codespecialidad,
				'numcolegiado' => $usu_num,
				'pais' => $usu_codpais,
				'provincia' => $usu_codprovestado,
				'poblacion' => $usu_codpoblacion,
				'ciudad' => $usu_ciudad,
				'direccion' => $usu_direccion,
				'cp' => $usu_cp,
				'telefono' => $usu_telefono,
				'fax' => $usu_fax,
				'empresa' => $usu_centro,
				'usu_tipo' => $usu_tipo,
				'clave' => $clave00,
				'fecha' => $fechoy,
				'fecreg' => $fechoy,
				'fecmod' => $fechoy,
				'pass' => $pass1,
				'mailing' => $mailing,
				'mailing1' => $tbh,
				'datoscedidos' => $patrocinador1,
				'uniqueid' => $clave00
			);
			$db->insert('com_alumnos', $data);
			$ide = $db->lastInsertId();

			// se envia el email

			require('includes/class.phpmailer.php');
			require('includes/class.smtp.php');

			$nota = "<table width=\"580\" style=\"background-color: #ffffff; margin: 0px auto;\" cellpadding=\"0\" cellspacing=\"0\" border=\"1\" bordercolor=\"#19ABB9\">
        <tr>
         <td valign=\"top\" align=\"center\"><img src=\"" . $app_url . "img/logo.png\" alt=\"" . $apptitle . "\" width=\"166\" /></td>
        </tr>
        <tr>
         <td valign=\"top\" align=\"left\">
             <table width=\"580\" style=\"margin: 0px auto; border-collapse: collapse;\" cellpadding=\"0\" cellspacing=\"0\">
             <tr>
               <td width=\"15\" valign=\"top\" align=\"left\">&nbsp;</td>
               <td width=\"560\" align=\"left\" valign=\"top\"><font size=\"2\" color=\"#000000\" face=\"Arial, sans-serif\"><br /><br />
              Apreciado/a Dr/Dra. " . $usu_nombre . " " . $usu_ape1 . "<br><br>

Hemos recibido su solicitud de registro en el curso <a href=\"" . $app_url . "\">" . $app_url . "</a>.<br>
Para confirmar su registro definitivo debe hacer clic sobre el siguiente enlace.<br>
Acceder&aacute; a una p&aacute;gina de confirmaci&oacute;n en la web de " . $apptitle . ".<br><br> </font>
                </td>
               <td width=\"15\" valign=\"top\" align=\"left\">&nbsp;</td>
             </tr>
             <tr>
               <td width=\"15\" valign=\"top\" align=\"left\">&nbsp;</td>
               <td width=\"560\" align=\"center\" valign=\"top\"><font size=\"2\" color=\"#000000\" face=\"Arial, sans-serif\"><br /><br />
<a href=\"" . $app_url . "registro_confirma.php?id=" . $ide . "&unique=" . $clave00 . "\"><img src=\"" . $app_url . "mailing/confirmacion/boton.png\" alt=\"" . $apptitle . "\" width=\"281\" height=\"54\" /></a><br><br>
                </font>
                </td>
               <td width=\"15\" valign=\"top\" align=\"left\">&nbsp;</td>
             </tr>
             <tr>
               <td width=\"15\" valign=\"top\" align=\"left\">&nbsp;</td>
               <td width=\"560\" align=\"left\" valign=\"top\"><font size=\"2\" color=\"#000000\" face=\"Arial, sans-serif\"><br /><br />

Si no puede acceder a trav&eacute;s del enlace de arriba, copie la siguiente direcci&oacute;n:<br><br>
" . $app_url . "registro_confirma.php?id=" . $ide . "&unique=" . $clave00 . "<br><br>

Muchas gracias.<br><br>

Cordialmente,<br><br>
" . $apptitle . "
<br />&nbsp;<br />&nbsp;<br />&nbsp;<br />
                </font>




                </td>
               <td width=\"15\" valign=\"top\" align=\"left\">&nbsp;</td>
             </tr>

             </table>
         </td>
        </tr>

        </table>";



		/* //florian

			$mail = new PHPMailer();

			$mail->IsSMTP();

			$mail->SMTPDebug = 0;
			// 0 = no output, 1 = errors and messages, 2 = messages only.


			// Sustituye (ServidorDeCorreoSMTP)  por el host de tu servidor de correo SMTP
			$mail->Host = $mailhost;
			if (!empty($mailsecure)) {
				$mail->SMTPSecure = $mailsecure;
			}
			if (!empty($mailport)) {
				$mail->Port = $mailport;
			}




			$mail->From = $mailemail;


			$mail->FromName = $apptitle;

			$mail->Subject = "Confirmacion de registro " . $apptitle;

			$mail->AltBody = "Confirmacion de registro " . $apptitle;
			$mail->IsHTML(true);

			$mail->MsgHTML($nota);

			// Sustituye  (CuentaDestino )  por la cuenta a la que deseas enviar por ejem. admin@domitienda.com  


			$mail->AddAddress($usu_email, $usu_email);
			//$mail->AddBCC('gianna@tba.es', 'test');

			$mail->SMTPAuth = true;


			$mail->Username = $maillogin;
			$mail->Password = $mailpass;

			if (!$mail->Send()) {

				//header("Location: gracias.php?err=1");
			} else {
				// header("Location: gracias.php");
			}

			// se termina de enviar el email
			*/
			

			header("Location: " . $dev_OK);
		} else {
			// el email ya existe en la base de datos en la plantilla login debe mostrar error de email existente
			header("Location: login.php?err=6");
		}
	/*	
	} else {
		header("Location: registro.php?err=7");
	}
	*/
	// termina el registro de usuario
}
