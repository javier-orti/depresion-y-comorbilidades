<?php

require_once 'lib/autoloader.class.php';
require_once 'lib/init.class.php';
if ($tipologin == 'off') {
    require_once 'lib/auth_off.php';
} else {
    require_once 'lib/auth.php';
}

?>
<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/estilos.css?v=31">
    <?php if ($page == "contacto" || $page == "registro") {?> 
    <script src='https://www.google.com/recaptcha/api.js'></script>   
    <?php } ?>

 


    <title>Formación DE & CO</title>
  </head>
  <body>
    <header>
    <div class="topbar">

<div class="tbbtns">
  <?php if ($authj->logueado == 1) { ?>
  <div class="topbarbtn nombreb">
      <p>Dr(a). <?php echo $authj->rowff['nombre']." ".$authj->rowff['ape1']." ".$authj->rowff['ape2']; ?></p>
  </div>
  <div class="topbarbtn desconb">
    <a href="salir.php">Desconectar</a>
  </div>
<?php } else { ?>
  <!-- Si NO está logueado -->
  <div class="topbarbtn loginb">
    <a href="login.php" style="cursor:pointer;">Iniciar Sesión</a>
  </div>

  <div class="topbarbtn loginb">
    <a href="registro.php">Registrarse</a>
  </div>
<?php } ?>
</div>

</div> <!-- / topbar -->
        <div class="menuheader" >
          
          <div class="row row-cols-1 row-cols-lg-3">
            <div class="col-6 col-sm-6 col-lg-2 order-1 order-xs-1 order-sm-1 order-lg-1 div_logo" style="min-height: 100px;">
              <a href="index.php" class="navbar-brand"><img src="img/logo.png" alt=""></a>
            </div>
            <div class="col col-lg-8 order-3 order-xs-3 order-sm-3 order-lg-2">
            <?php if ($authj->logueado == 1) { ?>
      
              <nav class="navbar navbar-expand-lg navbar-light bg-light">
                
                <?php if ($page != 'login' && $page != 'registro'){ ?>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    
                    <ul class="navbar-nav ml-auto mr-auto">
                        <li class="nav-item <?php if ($page == 'intro') echo 'active';?>">
                            <a class="nav-link" href="index.php">INTRODUCCIÓN <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item <?php if ($page == 'comite') echo 'active';?>">
                            <a class="nav-link" href="comite.php">COMITÉ CIENTÍFICO</a>
                        </li>
                        <li class="nav-item <?php if ($page == 'modulo') echo 'active';?>">
                            <a class="nav-link" href="modulos.php">CURSO</a>
                        </li>
                        <li class="nav-item <?php if ($page == 'diplomas') echo 'active';?>">
                            <a class="nav-link" href="diplomas.php">DIPLOMAS</a>
                        </li>

                        <li class="nav-item <?php if ($page == 'contacto') echo 'active';?>">
                            <a class="nav-link" href="contacto.php">CONTACTO</a>
                        </li>
                        <!--
                        <li class="nav-item <?php if ($page == 'master') echo 'active';?>">
                            <a class="nav-link" href="masterclass.php">MASTERCLASS</a> -->
                        </li>
                        
                    </ul>
                    
                </div>
                <?php } ?>
              </nav>
        
            <?php  } ?>
            </div>
            <div class="col-6 col-sm-6 col-lg-2 order-2 order-xs-2 order-sm-2 order-lg-3">
               <img src="img/tbh.png" alt="the-butterflies-healthcare" style="width:100%;max-width:200px;margin-top:10px">
            </div>
          </div>
          
        </div>
    </header>