<footer>
        <div class="container">
            <div class="text-center">
                 
            <div class="row row-cols-1 row-cols-lg-3">
           
            <div class="col logo">
            <?php if ($page == 'login' || $page == 'intro'){ ?>
                        <p class="color1 mb-0"><small>Patrocinado por:</small></p>
                        <img src="img/esteve.png" alt="Esteve">
                        <br>
                        <?php } ?>
                    </div>
                    
               
                 
                    <div class="col logo">
                        <p class="color1 mb-0"><small>Con el aval científico de la Sociedad Española de Psiquiatría</small></p>
                        <img src="img/sep.png?v=1" alt="Esteve">
                        <br>
                    </div>
                  
                 
                    <div class="col logo">
                        <p class="color1 mb-0"><small>Con el aval científico de la Sociedad Española de Psiquiatría Biológica</small></p>
                        <img src="img/sepb.png?v=1" alt="Esteve">
                        <br>
                    </div>
                   
                
            </div>

           
            <p class="text-center foot-links"><a href="privacidad.php">Privacidad</a> | <a href="legal.php">Aviso legal</a> | <a href="cookies.php">Política de cookies</a> | <a href="contacto.php">Contacto</a> </p>
        </div>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="js/jquery.validate.js"></script>
<?php if ($page == "registro") { ?>
<script src="js/registro.js?v=1"></script>
<?php } ?>
<?php  
              if ($page == 'modulos') { ?>
              <script type="text/javascript">
          $(document).ready(function () {

              $('.btn-entrar').on('click', function () {
                 $('#btn-entrar1').attr('href', '/modulo.php?id='+ $(this).attr('rel'));
                 $('#myModal_news').modal('show');

           
             });
            });


</script>

              <?php } ?>
<?php if ($page == 'modulo' or $page='modulo-caso') { ?>
    <script type="text/javascript">
          $(document).ready(function () {

           

              

             $('#sidebarCollapse').on('click', function () {
                 $('#sidebar').toggleClass('active');
             });


          });


    </script>
    <script>
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
      function toggleResetPswd(e){
        e.preventDefault();
        $('#logreg-forms .form-signin').toggle() // display:block or none
        $('#logreg-forms .form-reset').toggle() // display:block or none
      }

      function toggleSignUp(e){
          e.preventDefault();
          $('#logreg-forms .form-signin').toggle(); // display:block or none
          $('#logreg-forms .form-signup').toggle(); // display:block or none
      }

      $(()=>{
          // Login Register Form
          $('#logreg-forms #forgot_pswd').click(toggleResetPswd);
          $('#logreg-forms #cancel_reset').click(toggleResetPswd);
          $('#logreg-forms #btn-signup').click(toggleSignUp);
          $('#logreg-forms #cancel_signup').click(toggleSignUp);
      });
    </script>
<?php } ?>
<?php if ($page='modulo-caso') { ?>
<script>
    $('.carousel').on('touchstart', function(event){
    const xClick = event.originalEvent.touches[0].pageX;
    $(this).one('touchmove', function(event){
        const xMove = event.originalEvent.touches[0].pageX;
        const sensitivityInPx = 5;

        if( Math.floor(xClick - xMove) > sensitivityInPx ){
            $(this).carousel('next');
        }
        else if( Math.floor(xClick - xMove) < -sensitivityInPx ){
            $(this).carousel('prev');
        }
    });
    $(this).on('touchend', function(){
        $(this).off('touchmove');
    });
});
</script>
<?php } ?>
</body>
</html>