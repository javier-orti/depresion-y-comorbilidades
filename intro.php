<?php 
$page = 'intro';
include('header.php'); ?>
    <div class="main">
        <div class="container">
            <div class="video embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item"  src="https://www.youtube.com/embed/ITTGVQqjqH8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <p class="small color1">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>
            <div class="acceder">
                <a href="login.php" class="btn-acceder">ACCEDER<br>AL CURSO</a>
            </div>
        </div>
        
    </div>
<?php include('footer.php'); ?>
    