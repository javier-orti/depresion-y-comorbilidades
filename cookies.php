<?php 
$page = 'legal';
$tipologin = 'off';
include('header.php'); ?>
    
    

    
    <div class="main">
        <div class="container">
            <h1 class="color2">Política de cookies</h1>
            
			  <br><br>
			  
			  <p class="subtit">Información general sobre las cookies</p>

		<p>Una “cookie” es un pequeño archivo de texto que se genera de forma imperceptible en el terminal del usuario (PC, smartphone,
tablet o cualquier otro dispositivo) al acceder al presente sitio web (el “Sitio Web”). Las cookies almacenan y recuperan
			la información sobre la navegación que se efectúa desde dicho equipo.</p>
			<p>Las cookies permiten conocer cierta información como, por ejemplo, la fecha y la hora de la última vez que el usuario visitó el
Sitio Web, el diseño de contenidos que el usuario escogió en su última visita, sus preferencias o los elementos de seguridad
				que intervienen en el control de acceso a las áreas restringidas.</p>
			<p>Dispone de más información sobre las cookies <a href="https://es.wikipedia.org/wiki/Cookie" target="_blank">aquí</a>.</p>
			<p>Aquellos usuarios que no deseen recibir cookies o quieran ser informados antes de que se almacenen en su ordenador pueden
configurar su navegador a tal efecto. La mayor parte de los navegadores permiten la gestión de las cookies de tres formas
diferentes: (i) las cookies no se aceptan nunca; (ii) para cada cookie, el navegador pregunta al usuario si ésta debe ser aceptada;
				o (iii) las cookies se aceptan siempre.</p>
				<p>El navegador también puede incluir la posibilidad de especificar qué cookies tienen que ser aceptadas y cuáles no. En concreto,
el usuario puede optar por alguna de las siguientes opciones: (i) rechazar las cookies de determinados dominios; (ii) rechazar
las cookies de terceros; aceptar cookies como no persistentes (se eliminan cuando el navegador se cierra); o (iii) permitir
al servidor crear cookies para un dominio diferente. Además, los navegadores también pueden permitir a los usuarios ver y
					borrar cookies individualmente.</p>
					<p>A continuación se detalla una relación de las principales cookies utilizadas en los Sitios Web del grupo al que pertenece el
						Administrador Web:</p>
						<p>(a) <span class="subraya">Cookies de sesión:</span> Son cookies temporales que permanecen en el archivo de cookies de su navegador hasta que aban
done el Sitio Web, por lo que ninguna queda registrada en el disco duro del usuario. La información obtenida por medio de
estas cookies sirven para analizar pautas de tráfico en el Sitio Web. A la larga, esto nos permite proporcionar una mejor experiencia
							para mejorar el contenido del Sitio Web y facilitando su uso.</p>
			<p>(b) <span class="subraya">Cookies permanentes:</span> Son cookies almacenadas en el disco duro y la web las detecta cada vez que el usuario realiza una
nueva visita. Una cookie permanente posee una fecha de expiración determinada. La cookie dejará de funcionar después de
esa fecha. Se utilizan generalmente para facilitar, por ejemplo, los servicios de registro.
			<p>(c) <span class="subraya">Cookies técnicas:</span> Son cookies estrictamente necesarias para el funcionamiento del Sitio Web como, por ejemplo, aquellas
que sirven para una correcta navegación o para asegurar que el contenido del Sitio Web se carga eficazmente.</p>
			<p>(d) <span class="subraya">Cookies de terceros:</span> Son cookies de terceros como, por ejemplo, las usadas por redes sociales o por complementos externos
				de contenido como “Google Maps”.</p>
			<p>(e) <span class="subraya">Cookies analíticas:</span> Son cookies para recopilar datos estadísticos y comportamentales de la actividad del usuario.</p>

			<p class="subtit">Tipos de cookies del Sitio Web</p>
			<p>En el presente Sitio Web se pueden llegar a utilizar las siguientes cookies propias y de terceros:</p>


			<table border="1" cellspacing="0" cellpadding="1" width="100%" class="cookietable">
 				 <tr>
    <td colspan="4" class="centrado bgazul">
      <strong>COOKIES PROPIAS</strong></td>
  </tr>
  <tr>
    <td width="20%" class="bgazul"><p class="centrado"><strong>Cookie</strong></p></td>
    <td width="20%" class="bgazul"><p class="centrado"><strong>Tipo de cookie</strong></p></td>
    <td width="30%" class="bgazul"><p class="centrado"><strong>Finalidad de la cookie</strong></p></td>
    <td width="30%" class="bgazul"><p class="centrado"><strong>Vencimiento de la cookie</strong></p></td>
  </tr>
  <tr>
    <td><p>codusuario_jko</p></td>
    <td><p>Técnica</p></td>
    <td><p>Código de Usuario Registrado</p></td>
    <td><p>Sesión</p></td>
  </tr>
  <tr>
    <td><p>id_jko</p></td>
    <td><p>Técnica</p></td>
    <td><p>Código del usuario en el sistema interno de la aplicación</p></td>
    <td><p>Sesión</p></td>
  </tr>
  <tr>
    <td><p>clave</p></td>
    <td><p>Técnica</p></td>
    <td><p>Identificador de la sesión del usuario</p></td>
    <td><p>Sesión</p></td>
  </tr>
  <tr>
    <td><p>cooka1</p></td>
    <td><p>Técnica</p></td>
    <td><p>Verifica si el usuario ha visto la política de cookies.</p></td>
    <td><p>Sesión</p></td>
  </tr>
  <tr>
    <td><p>altoDiapo</p></td>
    <td><p>Técnica</p></td>
    <td><p>Se utiliza para ajustar el contenido del curso a la pantalla.</p></td>
    <td><p>Sesión</p></td>
  </tr>
</table>
<p>&nbsp;</p>
<table border="1" cellspacing="0" cellpadding="1" width="100%" class="cookietable">
  <tr>
    <td colspan="4" class="centrado bgazul"><p class="centrado"><strong>COOKIES DE TERCEROS</strong></p></td>
  </tr>
  <tr>
    <td width="20%" class="bgazul"><p class="centrado"><strong>Cookie</strong></p></td>
    <td width="20%" class="bgazul"><p class="centrado"><strong>Tipo de cookie</strong></p></td>
    <td width="30%" class="bgazul"><p class="centrado"><strong>Finalidad de la cookie</strong></p></td>
    <td width="30%" class="bgazul"><p class="centrado"><strong>Vencimiento de la cookie</strong></p></td>
  </tr>
  <tr>
    <td><p>embed_preferences </p></td>
    <td><p>Técnica</p></td>
    <td><p>Cookies de Vimeo utilizadas en la reproducción de vídeos a través de la    web.</p></td>
    <td><p>Persistente</p></td>
  </tr>
  <tr>
    <td><p>pl_volume</p></td>
    <td><p>Técnica</p></td>
    <td><p>Cookies de Vimeo utilizadas en la reproducción de vídeos a través de la    web.</p></td>
    <td><p>240 días</p></td>
  </tr>
  <tr>
    <td><p>uid</p></td>
    <td><p>Técnica</p></td>
    <td><p>Cookies de Vimeo utilizadas en la reproducción de vídeos a través de la    web.</p></td>
    <td><p>240 días</p></td>
  </tr>
  <tr>
    <td><p>vuid</p></td>
    <td><p>Técnica</p></td>
    <td><p>Cookies de Vimeo utilizadas en la reproducción de vídeos a través de la    web.</p></td>
    <td><p>240 días</p></td>
  </tr>
</table>

 			<p>&nbsp;</p>
			<p>Es posible que al visitar un Sitio Web se publique un anuncio o una promoción sobre nuestros productos y servicios o se
instale en el navegador alguna cookie que nos sirve para desarrollar un control de nuestros anuncios en relación, por ejemplo,
			con el número de veces que son vistos, donde aparecen, a qué hora se ven, etc.</p>
			<p>Puede ocurrir que algunas cookies utilizadas en este Sitio Web no estén relacionadas con el Administrador Web. Ello se debe
a que algunos Sitios Web tienen insertado contenido procedente de Sitios Web de Terceros. Debido a que el referido contenido
procede de un Sitio Web de Terceros, el Administrador Web no controla la configuración de dichas cookies. Si el usuario
quiere cambiar sus preferencias de configuración de cookies, deberá consultar los Sitios Web de Terceros para obtener información.</p>
			<p class="subtit">Administración de las cookies. Revocación y eliminación</p>
			<p>El usuario, en cualquier momento, puede bloquear o eliminar las cookies de este Sitio Web instaladas en su equipo mediante la
				configuración de las opciones del navegador instalado en su ordenador:</p>
				<p>• Para más información sobre Internet Explorer pulse <a href="https://support.microsoft.com/es-es/help/17442/windows-internet-explorer-delete-manage-cookies">aquí</a>.<br>
				   • Para más información sobre Google Chrome pulse <a href="https://support.google.com/chrome/answer/95647">aquí</a>.<br>
				   • Para más información sobre Firefox pulse <a href="https://support.mozilla.org/es/kb/Borrar%20cookies">aquí</a>.<br>
				   • Para más información sobre Safari pulse <a href="https://support.apple.com/es-es/HT201265">aquí</a>.</p>
				  <p>En caso de que no se permita la instalación de cookies técnicas en su navegador, es posible que no pueda acceder a alguna de
					  las secciones de nuestro Sitio Web o que éste funcione incorrectamente.</p>
				  <p>Finalmente, si desea tener un mayor control sobre la instalación de cookies, puede instalar programas o complementos a su
navegador, conocidos como herramientas de “Do Not Track”, que le permitirán escoger.</p>

		</div>
        
    </div>
    
<?php include('footer.php'); ?>