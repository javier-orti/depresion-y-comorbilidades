<?php 
$page = 'contacto';
$tipologin = 'off';
include('header.php'); ?>
    
    

    
    <div class="main">
        <div class="container">
            <h1 class="color2">Contacto</h1>
			<br><br>
      <div class="subtit">Envíenos sus dudas y sugerencias</div>
      <?php if ($act == 2) { ?>
      <div class="alert alert-success">Gracias por contactarnos, en breve le responderemos.<br><br></div>
      <?php } else if ($act == 3) { ?>
        <div class="alert alert-danger">Demuestre que no es un robot.</div>
                            
      <?php } ?>
                      <br>
            <div class="col-xs-12">
              <form class="form-horizontal" role="form" action="contacto1.php" method="post" id="contacto_1">
                                <div class="form-group">
                  <label class="control-label col-sm-2" for="nombre">Nombre:</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" value="" required>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-2" for="email">E-mail:</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="" required>
                  </div>
                  <div class="clearfix"></div>
                </div>


                <div class="form-group">
                  <label class="control-label col-sm-2" for="mensaje">Mensaje:</label>
                  <div class="col-sm-12">
                    <textarea name="mensaje" class="form-control" placeholder="Mensaje" required></textarea>
                  </div>
                  <div class="clearfix"></div>
                </div>
              	<div class="form-group">
              		<div class="col-sm-2">
              		</div>
              		<div class="col-sm-12">
              			<div class="g-recaptcha" data-sitekey="6Ld_QHEaAAAAAFZvr42ova5CQXSVVV6viDiMikY-"></div>
              		</div>
                  <div class="clearfix"></div>
              	</div>
          
                <div class="form-group">
                  <label class="control-label col-sm-2" for="pwd">&nbsp;</label>
                  <div class="col-sm-12 notapie">
                   <label><input type="checkbox" value="1" name="acepto" id="acepto" required>
                            He leído y acepto la <a href="privacidad.php">Política de Privacidad</a> y el <a href="legal.php">Aviso legal</a></label>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-12 text-right">
                    <button type="submit" class="btn-reg btn-clr2">Enviar</button>
                  </div>
                  <div class="clearfix"></div>
                </div>
                            </form>

                <div class="clearfix"></div>
              </div>
          
     	</div>
        
    </div>
    
<?php include('footer.php'); ?>