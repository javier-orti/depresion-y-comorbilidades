<?php 
$page = 'legal';
$tipologin = 'off';
include('header.php'); ?>
    
    

    
    <div class="main">
        <div class="container">
            <h1 class="color2">Aviso Legal</h1>
            <div class="subtit">Condiciones generales de uso</div>
          <br><br>

     <div class="clearfix"></div>
        <div class="col-xs-12">
        <div class="cua_int home legalinfo">



         <p class="subtit">Administrador del Sitio Web</p>

		<p>The Butterflies Healthcare S.L<br>
            Pje. Carsi 13 bis 08025 Barcelona<br>
            Telf.: +34 93 253 11 36<br>
            NIF: B66600164</p>

		<p class="subtit">Contenido y acceso al Sitio Web</p>


		<p>Las presentes condiciones generales de uso (las "Condiciones Generales de Uso") regulan el acceso y utilización del presente
            sitio web (el "Sitio Web"), dirigido exclusivamente a mayores de edad. El acceso al Sitio Web y uso de sus contenidos implica
			la aceptación sin reservas de las presentes Condiciones Generales de Uso por parte del usuario.</p>
		<p><span class="negrita">The Butterflies Healthcare</span> S.L (la "<span class="negrita">Editorial</span>") es el administrador del presente Sitio Web.
            Los contenidos y servicios que la Editorial pone a disposición del usuario en el Sitio Web podrán, asimismo, quedar regulados
            por condiciones particulares que, en caso de conflicto, prevalecerán sobre las Condiciones Generales de Uso (las "Condiciones
            Particulares"). El usuario se somete asimismo, cada vez que utilice el correspondiente contenido o servicio, a sus correspondientes
            Condiciones Particulares aplicables.</p>
        <p>Las informaciones o servicios contenidos en el Sitio Web están destinados exclusivamente a profesionales sanitarios facultados
            para la prescripción o dispensación de medicamentos en el territorio español (los “Usuarios”). Dicha información se ofrece
            solamente con carácter formativo y educativo, y no pretende sustituir en ningún caso opiniones, consejos y recomendaciones
            de un profesional sanitario cualificado, y tampoco debe ser utilizada para diagnosticar un problema de salud o de condición
            física.</p>
        <p>Es intención de la Editorial ofrecer en todo momento una información cierta y actualizada. Sin embargo, esta información
            puede contener incorrecciones técnicas o errores tipográficos. La Editorial no se responsabiliza de la idoneidad de la información
            contenida en el Sitio Web. En ningún caso, la Editorial será responsable de cualquier daño por la pérdida de uso, de
            datos o de beneficios, como consecuencia de la visualización, utilización del software, documentos, información o servicios, en
            general, disponibles en este Sitio Web.</p>
        <p>La Editorial adopta medidas de seguridad razonablemente adecuadas para detectar la existencia de virus. No obstante, el
            usuario debe ser consciente de que las medidas de seguridad de los sistemas informáticos en Internet no son enteramente
            fiables y que, por tanto, la Editorial no puede garantizar la inexistencia de virus u otros elementos que puedan producir alteraciones
            en los sistemas informáticos (software y hardware) del usuario o en sus documentos electrónicos y ficheros contenidos
            en los mismos.</p>
							<p>Cada país puede tener requisitos legales o prácticas médicas propias. La información contenida en cada uno de los sitios web
está pensada para ser utilizada, en su caso, exclusivamente por profesionales de la salud de España.
En todo caso, para cualquier cuestión respecto del presente Sitio Web o contenidos relacionados, puede hacerlo a través del
								siguiente <a href="contacto.php">formulario de contacto</a>.</p>
			<p class="subtit">Uso correcto del Sitio Web</p>
			<p>El usuario se compromete a utilizar el Sitio Web, los contenidos y servicios de conformidad con la Ley, las presentes Condiciones
Generales de Uso, las Condiciones Particulares, si fueran de aplicación, las buenas costumbres y el orden público. Del
mismo modo, el usuario se obliga a no utilizar el Sitio Web, sus contenidos o los servicios que se presten a través de dicho
Sitio Web con fines o efectos ilícitos o contrarios al contenido de las presentes Condiciones Generales de Uso (y, en su caso,
de las Condiciones Particulares), lesivos de los intereses o derechos de terceros, o que de cualquier forma pueda dañar, inutilizar,
hacer inaccesibles o deteriorar el Sitio Web, sus contenidos o sus servicios o impedir un normal disfrute del mismo por
				otros usuarios.</p>
				<p>Asimismo, el usuario se compromete expresamente a no destruir, alterar, inutilizar o, de cualquier otra forma, dañar los datos,
programas o documentos electrónicos que se encuentren en el Sitio Web y a no obstaculizar el acceso de otros usuarios mediante
el consumo masivo de los recursos informáticos a través de los cuales la Editorial presta el servicio, así como realizar
					acciones que dañen, interrumpan o generen errores en dichos sistemas o servicios.</p>
					<p>Del mismo modo, el usuario se compromete a no introducir programas, virus, macros, applets, controles "ActiveX" o cualquier
otro dispositivo lógico o secuencia de caracteres que causen o sean susceptibles de causar cualquier tipo de alteración en los
						sistemas informáticos de la Editorial o de terceros.</p>
						<p>Los documentos y gráficos publicados en este Sitio Web podrían incluir incorrecciones técnicas y errores tipográficos. Los
cambios son periódicamente añadidos a la información disponible. La Editorial podrá modificar en cualquier momento las
presentes Condiciones Generales de Uso sin aviso previo y, por tanto, le recomendamos que revise periódicamente dichas
							Condiciones Generales de Uso.</p>
			<p class="subtit">Contenidos, servicios ajenos al Sitio Web y publicidad</p>
			<p>Las presentes Condiciones Generales de Uso se refieren únicamente al Sitio Web de La Editorial y no se aplican a sitios web
				de terceros.</p>
				<p>No obstante, el Sitio Web pone a disposición del usuario, únicamente para la búsqueda de, y acceso a, contenidos y servicios
disponibles en Internet, hiperenlaces o dispositivos técnicos de enlace, que permiten al usuario el acceso a sitios web o portales
de Internet pertenecientes a, o gestionados por, terceros (los "<span class="negrita">Sitios Web de Terceros</span>"). Los Sitios Web de Terceros
referidos anteriormente no están bajo el control de la Editorial y, en consecuencia, la Editorial no es responsable del contenido
					y servicios de ninguno de los Sitios Web de Terceros.</p>
					<p>Estos Sitios Web de Terceros se proporcionan únicamente para informarle sobre la existencia de otras fuentes de información
sobre un tema concreto y la inclusión de un enlace en el presente Sitio Web no implica la aprobación de los Sitios Web de
						Terceros por parte de la Editorial.</p>
					<p>Por otro lado, cualquier persona física o jurídica que se proponga establecer un hiperenlace o dispositivo técnico de enlace
desde un Sitio Web de Terceros al presente Sitio Web deberá obtener una autorización previa y por escrito de la Editorial. En
todo caso, el establecimiento de un enlace desde un Sitio Web de Terceros al presente Sitio Web no implica, en ningún, caso la
existencia de relaciones entre el propietario del Sitio Web de Terceros y la Editorial, ni la aceptación o aprobación por parte de
							la Editorial de sus contenidos o servicios.</p>
					<p>El Sitio Web puede albergar, asimismo, contenidos publicitarios o estar patrocinados. Los anunciantes y patrocinadores son
los únicos responsables de asegurarse de que el material remitido para su inclusión en el Sitio Web cumple las leyes que en
								cada caso puedan ser de aplicación.</p>
					<p>La Editorial no será responsable de cualquier error, inexactitud o irregularidad que puedan incluir los contenidos publicitarios
									o de los patrocinadores.</p>
			<p class="subtit">Derechos de propiedad intelectual e industrial</p>
			<p>El presente Sitio Web contiene o hacer referencia a derechos de propiedad intelectual e industrial, incluyendo, sin carácter
limitativo, código fuente, diseño, estructura y arquitectura de navegación, bases de datos, fotografías, fonografías, signos distintivos,
patentes, modelos de utilidad, información, tecnologías, productos, procesos u otros derechos de la propiedad
intelectual o industrial, que son titularidad de la Editorial (o, en su caso, de terceros que han autorizado y/o licenciado su uso a
				favor de la Editorial) o de terceros.</p>
				<p>En virtud de estas Condiciones Generales de Uso no se cede ningún derecho de propiedad intelectual o industrial sobre el
Sitio Web ni sobre ninguno de sus elementos integrantes, quedando expresamente prohibidos al usuario la reproducción,
transformación, distribución, comunicación pública, puesta a disposición, extracción, reutilización, reenvío o la utilización de
cualquier naturaleza, por cualquier medio o procedimiento, de cualquiera de ellos, salvo en los casos en que esté legalmente
					permitido o sea autorizado por el titular de los correspondientes derechos.</p>
			<p class="subtit">Ley aplicable y jurisdicción</p>
			<p>Este Sitio Web se rige por la legislación española.</p>
			<p>La Editorial y el usuario, con renuncia expresa a su propio fuero, se someten a la jurisdicción de los Juzgados y Tribunales de la
ciudad de Madrid (España), salvo en los casos en que, por imperativo legal de normas de derecho internacional privado,
				fueran competentes los Juzgados y Tribunales del domicilio del usuario.</p>
			<p class="subtit">LEYENDA EDITORIAL</p>
			<p>Este curso ha sido elaborado por una entidad editorial profesional, independiente y autónoma. La entidad editorial, como
organizadora y proveedora del curso, es responsable de la planificación y contenidos de la actividad y, en ningún modo, las
aportaciones que, en su caso, pudiesen realizarse por el patrocinador condicionan, entre otros, la independencia de los contenidos,
la independencia de los ponentes, el control de la publicidad o la presencia de logotipos comerciales. Si usted tuviera
algún problema o incidencia sobre el curso puede comunicarlo a través del <a href="contacto.php">formulario de contacto</a>.</p>
<p>
La entidad editorial tiene la obligación de mantener los diplomas sólo durante el tiempo establecido por la normativa de la
Comunidad Autónoma correspondiente, por ello le recomendamos que se descargue y guarde el diploma una vez haya finalizado
el curso.</p>
</div></div></div>
        
    </div>
    
<?php include('footer.php'); ?>