﻿CKEDITOR.addStylesSet( 'my_styles',
[
    // Block Styles
    { name : 'Azul Title', element : 'h2', styles : { 'color' : 'Blue' } },
    { name : 'Rojo Title' , element : 'h3', styles : { 'color' : 'Red' } },
 
    // Inline Styles
    { name : 'CSS Style', element : 'span', attributes : { 'class' : 'estiloww' } },
    { name : 'Marca: Yellow', element : 'span', styles : { 'background-color' : 'Yellow' } }
]);