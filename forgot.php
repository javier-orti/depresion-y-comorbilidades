<?php
$page = 'login';
$tipologin = 'off';
include('header.php'); ?>
<div class="main">
    <div class="container">
        <div class="login">
            <h2 class="color2">INICIAR SESIÓN</h2>
            <div class="card">

                <?php if ($act == 'OK') { ?>
                    <div class="alert alert-success">Se ha enviado un email a su correo electrónico, por favor siga las instrucciones para recuperar su clave.</div>
                <?php } else  if ($err == '1') { ?>
                    <div class="alert alert-danger">El email introducido no está registrado en nuestra base de datos.</div>
                <?php } ?>



                <p>Introduzca su correo electrónico para recuperar su clave de acceso.</p>
                <br>
                <form action="action_registro.php?action=forgot" method="post">
                    <p><b>Correo electrónico</b></p>
                    <input type="email" name="usu_email" class="form-control">
                    <br>

                    <div class="text-center">
                        <button type="submit" class="btn-login">ENVIAR</button>
                    </div>
                </form>

            </div>
            <br>
            <div class="text-center">
                <a href="login.php" class="btn-reg">LOGIN</a> - <a href="registro.php" class="btn-reg">CREAR UNA CUENTA</a>
            </div>
            <br><br>
        </div>
    </div>

</div>
<?php include('footer.php'); ?>