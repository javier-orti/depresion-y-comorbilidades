<?php 
$page = 'legal';
$tipologin = 'off';
include('header.php'); ?>
    
    

    
    <div class="main">
        <div class="container">
			<h1 class="color2">Política de privacidad</h1>
			<br><br>
			<div class="cua_int home legalinfo">

			<p>El Responsable del tratamiento de los datos recabados por medio de este Sitio Web es The Butterflies Healthcare S.L., con N.I.F. B-66600164 y domicilio en Pje. Carsí 13, bis 08025 de Barcelona (en adelante, la “TBH”), provisto del teléfono 932 531 136 y de la dirección de correo electrónico info@tbhealthcare.es.
			</p>
	<p>Le informamos que los datos personales que nos proporcione a través de este Sitio Web o que recopilemos serán tratados para las siguientes finalidades (o alguna de ellas, en función del formulario de recogida de datos que haya aceptado):
	<div style="padding: 0 40px">
	<p>
(i)	Atender las solicitudes de información y/o consultas efectuadas por el usuario a través del Sitio Web. Los datos tratados con esta finalidad se conservarán por parte de TBH hasta haber dado respuesta a la solicitud de información y/o consulta, tras lo cual serán conservados hasta haber transcurrido el máximo plazo de prescripción de las infracciones en materia de protección de datos personales. La base jurídica para llevar a cabo este tratamiento de datos es el consentimiento del usuario dado para que su solicitud y/o consulta sea atendida.
</p>
	<p>
(ii)	Tramitar su inscripción, posibilitar la realización de la formación a través del Sitio Web, así como mantener informado al usuario sobre el desarrollo de. Los datos tratados con esta finalidad se conservarán hasta la total finalización de la formación y, tras ello, durante los plazos de conservación y de prescripción de responsabilidades legalmente previstos. La base jurídica para llevar a cabo este tratamiento de datos es el consentimiento del usuario dado para la realización del curso.
</p>
	<p>
(iii)	Mantener informado al usuario, incluso por medios electrónicos, acerca de otros cursos y servicios de TBH, siempre y cuando el usuario haya consentido de forma expresa el tratamiento de sus datos con esta finalidad, marcando la casilla correspondiente en el formulario de recogida de datos. Los datos tratados con esta finalidad se conservarán hasta el momento en que el usuario retire su consentimiento dado para la recepción de dichas comunicaciones. La base jurídica para llevar a cabo este tratamiento de datos es el consentimiento del usuario dado para la recepción de dichas comunicaciones comerciales.
</p>
	
</div>
<p>
Sus datos personales no serán transmitidos para su uso por terceros, a menos que hayamos obtenido su consentimiento o cuando así lo exija la ley. Los datos podrán comunicarse a proveedores que nos ayudan a proporcionar nuestros servicios y que tienen la consideración de encargados del tratamiento. Algunos de estos encargados pueden estar ubicados fuera del Espacio Económico Europeo (EEE), con los que adoptaremos garantías adecuadas para la seguridad en la transferencia internacional de datos personales.
</p>
	<p>No obstante lo anterior, TBH informa a los usuarios que realicen el curso, que sus datos personales serán comunicados al patrocinador del curso, la entidad Esteve Pharmaceuticals, S.A., con N.I.F. A-08037236 y domicilio en Passeig de la Zona Franca, nº 109, 4ª Planta, 08038, Barcelona, Sociedad Unipersonal (en adelante, “Esteve”), provisto del teléfono 93.446.60.00 y de la dirección de correo electrónico contactcenter@esteve.com, para las siguientes finalidades:
		</p>
		<div style="padding: 0 40px">
	<p>-	Posibilitar y velar por el correcto desarrollo de la formación. Los datos tratados con esta finalidad se conservarán hasta la total finalización de la formación y, tras ello, durante los plazos de conservación y de prescripción de responsabilidades legalmente previstos. La base jurídica para llevar a cabo este tratamiento de datos es el consentimiento del usuario dado para la realización del curso.
	</p>
	<p>-	Mantener informado al usuario, incluso por medios electrónicos, acerca de los productos, servicios y novedades de Esteve, temas de ámbito científico, profesional, sanitario o farmacéutico y próximos eventos, siempre y cuando usted haya consentido de forma expresa el tratamiento de sus datos con dicha finalidad, marcando la casilla correspondiente en el formulario de recogida de datos. Los datos tratados con esta finalidad se conservarán hasta el momento en que el usuario retire su consentimiento dado para la recepción de dichas comunicaciones. La base jurídica para llevar a cabo este tratamiento de datos es el consentimiento del usuario dado para la recepción de dichas comunicaciones comerciales.
</p>
	</div>
	<p>
Los datos serán tratados con el grado de protección adecuado, adoptándose medidas técnicas y organizativas apropiadas para evitar su alteración, pérdida accidental, divulgación o acceso no autorizados y cualquier otra forma indebida de tratamiento. Para la aplicación de dichas medidas se tienen en cuenta, entre otros aspectos, la naturaleza de los datos tratados, los riesgos, así como los avances de la tecnología y los costes de su implementación.
</p>
	<p>Esteve no transmitirá sus datos personales para su uso por terceros, a menos que hayamos obtenido su consentimiento o cuando así se exija legalmente. Los datos podrán comunicarse a proveedores que nos ayudan a proporcionar nuestros servicios y que tienen la consideración de encargados del tratamiento. Algunos de estos encargados pueden estar ubicados fuera del Espacio Económico Europeo (EEE), con los que adoptaremos garantías adecuadas para la seguridad en la transferencia internacional de datos personales.

	</p>
	<p>
			<strong>Derechos que asisten al usuario:</strong>
			</p>
	<p>
El usuario puede ejercer sus derechos de acceso, rectificación, supresión, limitación del tratamiento, portabilidad de los datos, oposición y a no ser objeto de decisiones individuales automatizadas, incluida la elaboración de perfiles. De igual modo, en los tratamientos de los datos del usuario cuya legitimación se base en el consentimiento dado por el usuario, éste tiene el derecho a retirar dicho consentimiento en cualquier momento, sin que ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada. 
</p>
	<p>Para el ejercicio de tales derechos, el usuario puede enviar su solicitud por escrito a:
	</p>
	<div style="padding: 0 40px">
	<p>-	The Butterflies Healthcare S.L., Pje Carsi. 13 bis, 08025 Barcelona o a través del correo electrónico info@tbhealthcare.es;
	</p>
	<p>-	Esteve Pharmaceuticals, S.A. (A/A Delegado de Protección de Datos), Passeig de la Zona Franca, nº 109, 4ª Planta, 08038, Barcelona; o al correo electrónico privacy@esteve.com
	</p>
	</div>
	<p>
En todo caso, el usuario tiene derecho a presentar una reclamación ante la Agencia Española de Protección de Datos, si lo estima oportuno. 

</p>





					<div class="clearfix"></div>
				</div>
		</div>
        
    </div>
    
<?php include('footer.php'); ?>