<?php

error_reporting(E_ALL);
ini_set('display_errors', '0'); 
$page = 'registro';
$tipologin = 'off';
include('header.php'); ?>
<div class="main">
    <div class="container">
        <div class="registro">
            <a href="login.php">
                < Volver</a>
                    <br><br>
                    <h2 class="color2">CREAR CUENTA</h2>
                    <div>

                        <br>
                        

                    <p><b>Área de acceso restringido</b>: esta área contiene información dirigida exclusivamente a profesionales sanitarios facultados para prescribir o dispensar medicamentos en España (requiere una formación especializada para su correcta interpretación).</p>

                    <p class="color1"><b>CLAVES DE ACCESO A LOS SERVICIOS RESTRINGIDOS</b></p>

                     <?php   if ($err=='6') { ?>
          <div class="alert alert-danger">El email que está intentado registrar ya está registrado en la base de datos, use su correo y contraseña para acceder o recupere su contraseña si la ha olvidado.</div>
          <?php } ?>

                    <form action="action_registro.php?action=registro" method="post" id="registro" >
                      <input type="hidden" name="con" value="<?php echo $con_ppal?>">
                      <input type="hidden" name="zon" value="<?php echo $zon_ppal?>">

                    <div class="row row-cols-1">

                      <div class="col-12">

                        <input type="email" class="form-control" name="usu_email" placeholder="Correo electrónico*" autocomplete="new-email" required/>

                      </div>

                      <div class="col-lg-6">

                        <input type="password" class="form-control" name="usu_password" id="usu_password" placeholder="Contraseña*" autocomplete="new-password" required/> 

                      </div>

                      <div class="col-lg-6">

                        <input type="password" class="form-control" name="usu_password2" id="usu_password2" placeholder="Repetir contraseña*" autocomplete="new-password" required/> 

                      </div>

                      <div class="col">

                      <p>Por motivos de seguridad, la contraseña deberá modificarse periódicamente y se establece que deberá ser cambiada por el usuario con una periodicidad mínima anual.</p>

                      </div>

                    </div>

                    <p class="color1"><b>DATOS PERSONALES</b></p>

                    <div class="row row-cols-1">

                      <div class="col-12">

                        <input type="text" class="form-control" name="usu_nombre" placeholder="Nombre*" required/>

                      </div>

                      <div class="col-lg-6">

                        <input type="text" class="form-control" name="usu_ape1" placeholder="Primer apellido*" required/> 

                      </div>

                      <div class="col-lg-6">

                        <input type="text" class="form-control" name="usu_ape2" placeholder="Segundo apellido*" required/> 

                      </div>

                      <div class="col-lg-6">

                        <select name="usu_codperfil" id="usu_codperfil" class="form-control">
                                <option value="0">Eres*</option>
                                <option value="ME">Médico</option>
                                <option value="FA">Farmacéutico</option>
                                <option value="AX">Auxiliar de Farmacia</option>
                                <option value="OT">Otros</option>
                                <option value="RS">Residente</option>
                                <option value="EN">Enfermero</option>
                        </select>

                      </div>

                      <div class="col-lg-6">

                      <select class="form-control" name="usu_codespecialidad" id="usu_codespecialidad" data-validation="required" disabled="disabled" autocomplete="off">
                <option value="">Especialidad*</option>
<option value="4">Alergia</option><option value="5">Analistas</option><option value="6">Anatomia patológica</option><option value="3">Andrólogo</option><option value="8">Anestesistas</option><option value="46">Angiología</option><option value="1">Aparato circulatorio</option><option value="2">Aparato digestivo/proctología</option><option value="7">Aparato respiratorio</option><option value="9">Bacteriología</option><option value="10">Cardiología</option><option value="11">Cirugía</option><option value="45">Cirugía vascular</option><option value="12">Citología</option><option value="13">Dermatología</option><option value="14">Diabetología</option><option value="47">Drogodependencia</option><option value="16">Endocrinología</option><option value="15">Enfermedades infecciosas</option><option value="56">Enfermería</option><option value="49">Epidemiología</option><option value="18">Farmacéutico</option><option value="17">Farmacología</option><option value="19">Geriatría</option><option value="20">Hematología / hemoterapia</option><option value="21">Medicina de empresa</option><option value="22">Medicina familiar</option><option value="23">Medicina general</option><option value="42">Medicina hospitalaria</option><option value="24">Medicina interna</option><option value="50">Medicina Preventiva</option><option value="57">Medicina Tropical</option><option value="52">Micología</option><option value="25">Nefrología</option><option value="41">Neumología</option><option value="26">Neuro-psiquiatría</option><option value="44">Neurocirugía</option><option value="27">Neurología</option><option value="28">Odontología/estomatología</option><option value="29">Oftalmología </option><option value="30">Oncología</option><option value="31">Otorrinolaringología</option><option value="58">Otra</option><option value="32">Pediatría</option><option value="55">Psicología</option><option value="33">Psiquiatría</option><option value="34">Radiología</option><option value="51">Radioterapia</option><option value="36">Rehabilitación</option><option value="35">Reumatología</option><option value="43">Sin especialidad</option><option value="37">Tocología ginecología</option><option value="38">Traumatología</option><option value="53">UCI - UVI</option><option value="48">Unidades de tabaquismo</option><option value="39">Urgencias</option><option value="40">Urología</option><option value="54">Vacunas</option>              </select>

                      </div>

                      <div class="col-lg-6">

                        <select class="form-control" name="usu_codpais" id="usu_codpais" autocomplete="off">
                          <option value="">Pais*</option>
                          <?php /*$sql_pais = "SELECT * FROM com_paises ORDER BY pais";
                       $result_pais = mysql_query($sql_pais,$link) or die ("Error en el examen");
                       while($row_pais = mysql_fetch_array($result_pais)) {
                      $db_pais = Db::getInstance();
                    $sql_pais = "SELECT * FROM com_paises ORDER BY pais";
                      $bind_pais = array(
                        ':codusuario' => ''
                      );
                      $row_pais1 = $db_pais->fetchAll($sql_pais);*/

                      $paises = New Pais();
                      $paises->getAll('todos');
                      foreach($paises->row as $row_pais) {
                      ?>
                          <option value="<?php echo $row_pais['codigo'];?>"><?php echo $row_pais['pais'];?></option>
                      <?php } ?>
                        </select>

                      </div>

                      <div class="col-lg-6" id="dv_provincia">

                      <select class="form-control" name="usu_codprovestado" value="" id="usu_codprovestado" autocomplete="off" disabled >
              <option value="">Provincia*</option> </select>

                      </div>
                      <div class="col-lg-6">

                        <input type="text" class="form-control" name="usu_numcolegiado" placeholder="Numero de Colegiado*" required/> 

                      </div>

                      <div class="col">

                        <br>
                        <div class="check">
                            

                          <input type="checkbox" name="pfofsan" id="pfofsan" class="css-checkbox" required>

                          <label for="pfofsan" class="css-label">Declaro que soy profesional sanitario facultado para prescribir o dispensar medicamentos en España.</label>

                        </div>
                        <br>
                        <div class="check">
                            

                          <input type="checkbox" name="acepto" id="acepto" class="css-checkbox" required>

                          <label for="acepto" class="css-label">He leído y acepto el <a href="https://www.esteve.com/es/avisolegal" target="_blank" title="Aviso Legal">aviso legal</a> y la <a href="privacidad.php" target="_blank" title="Política de privacidad" class="info-privacy">política de privacidad</a>.</label>

                        </div>

                        <div class="clearfix"></div></br>

                        <div class="check">

                          <input type="checkbox" name="mailing" id="mailing" class="css-checkbox" value="S">

                          <label for="mailing" class="css-label">Deseo recibir <a href="privacidad.php" target="_blank" title="">comunicaciones comerciales del organizador

del curso THE BUTTERFLIES HEALTHCARE</a>.</label>

                        </div>

                        <div class="clearfix"></div></br>

                        <div class="check">

                          <input type="checkbox" name="mailing" id="mailing" class="css-checkbox" value="S">

                          <label for="mailing1" class="css-label">Acepto recibir <a href="privacidad.php" target="_blank" title="">comunicaciones comerciales del patrocinador del curso ESTEVE</a>.</label>

                        </div>

                      </div>

                      <div class="clearfix"></div></br>

                      <div class="col text-right">

                        <button type="submit" class="btn-login">CREAR CUENTA</button>




                      </div>

                      

                    </div>

                    



                    </form>

                    </div>
                    <br>

                    <br><br>
        </div>
    </div>

</div>
<?php include('footer.php'); ?>